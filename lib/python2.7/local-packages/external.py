# Classes and functions facilitating access to applications and systems
# external to the application. These may generally be referred to as adapters.

from boto.dynamodb2.layer1 import DynamoDBConnection as NoSqlDB
from boto.dynamodb2.table import Table
from boto.dynamodb2.items import Item
import sqlite3 as SqlDB
import logging
import sys
import os
import time
from datetime import datetime
from smtplib import SMTP_SSL  # this invokes the secure SMTP protocol (port 465, uses SSL)
# from smtplib import SMTP                  # use this for standard SMTP protocol   (port 25, no encryption)
from email.mime.text import MIMEText
import httplib  # still needed?
#from httplib2 import Http
import shutil
#from twython import Twython
from collections import OrderedDict
import json
import math
from pprint import pprint
#from instagram import client, subscriptions
import codecs  # encode output files (utf-8-sig)
import requests
#from oauth2client.client import OAuth2WebServerFlow  # oauth2 flow
from oauth2client.service_account import ServiceAccountCredentials  # server-server
#from googleapiclient.discovery import build

import util
import parse
import internal
import enrich

module_nm = 'main.external'
module_logger = logging.getLogger(module_nm)
api_pg_delay = 5  # delay (sec) between API pagination calls
api_delay = 1  # delay where subsequent API calls must be made

class Ext():
    def __init__(self, ext_id='', ext_cfg={}):
        self.cfg = {}  # adapter configuration elements
        if ext_cfg:  # if populated
            self.cfg = { key: ext_cfg[key] for key in ext_cfg }  # config into aggregation object
        if ext_id:  # if populated
            self.cfg['id'] = ext_id
        self.rcds = []  # records represented as stored in application data store
        self.id = {}  # identity elements, e.g., external ID/name
        self.payload = {}  # records/data as represented within/by remote application/system
        self.metrics = {}  # capture metrics associated with processing
        self.metrics['rcd_cnt'] = -1  # number of records
        self.metrics['io_time'] = -1.0  # i/o (e.g., file read, WS call) execution time of batch in seconds
        self.metrics['size_bytes'] = -1  # size of batch payload in bytes
        self.metrics['proc_strt_time'] = time.time()  # time that batch processing began
        self.metrics['proc_time'] = -1.0
        self.metrics['proc_end_ts'] = ''
        self.metrics['proc_end_epch_ms'] = -1
        self.metrics['avg_rcd_per_sec'] = -1.0
        self.metrics['ds_rcd_cnt'] = -1  # number of records to/from data store
        self.metrics['ds_size_bytes'] = -1
        self.metrics['ds_avg_rcd_size_bytes'] = -1.0
        self.metrics['ds_xfer_units'] = -1
        self.metrics['ds_avg_thrput_units'] = -1.0
        self.logger = logging.getLogger(module_nm + '.' + 'Ext')
        
    def put_ext_metrics(self):
        
        log = logging.getLogger(module_nm + '.Ext.put_ext_metrics')
        
        log.debug("Computing closing metrics.")
        no_sql = internal.NoSql()
        params =    {   'limit': 1,  # just want most recent (i.e., active)
                        'ext_id__eq': self.cfg['id'],
                        'proc_end_epch_ms__lte': util.get_epch_ms(),  # get now (epoch ms)
                        'reverse': True
                    }
    
        no_sql.get_items('metrics_ext', params)
        
        self.metrics['proc_time'] = time.time() - self.metrics['proc_strt_time']
        self.metrics['avg_rcd_per_sec'] = self.metrics['rcd_cnt'] / self.metrics['proc_time']
        if self.metrics['rcd_cnt'] > 0:
            self.metrics['avg_rcd_size_bytes'] = int(round(self.metrics['size_bytes'] / self.metrics['rcd_cnt']))
        else:
            self.metrics['avg_rcd_size_bytes'] = 0
        dttm = util.get_now_dttm()
        self.metrics['proc_end_ts'] = util.get_sql_ts(dttm)
        self.metrics['proc_end_epch_ms'] = util.get_epch_ms(dttm)
        self.metrics['ds_rcd_cnt'] = len(self.rcds)
        self.metrics['ds_size_bytes'] = sys.getsizeof(str(self.rcds))
        if self.metrics['ds_rcd_cnt'] > 0:
            self.metrics['ds_avg_rcd_size_bytes'] = self.metrics['ds_size_bytes'] / self.metrics['ds_rcd_cnt']
        else:
            self.metrics['ds_avg_rcd_size_bytes'] = 0

        # below, determine computationally how many capacity units were consumed, assuming
        # strongly consistent reads (1kb per unit; conservative); writes are 1kb per unit...
        # would need to add check here for dest versus source if used for both outgoing/incoming
        xfer_unit_sz = 1000.0
        xfer_units = 0
        for i in xrange(len(self.rcds)):
            xfer_units += math.ceil(sys.getsizeof(str(self.rcds[i])) / xfer_unit_sz)
        self.metrics['ds_xfer_units'] = int(xfer_units)
        if len(no_sql.rcds) > 0:  # only if previous metrics were returned
            diff = self.metrics['proc_end_epch_ms'] - int(no_sql.rcds[0]['proc_end_epch_ms'])  # time (ms) since last run
            self.metrics['ds_avg_thrput_units'] = self.metrics['ds_xfer_units'] / (diff / 1000.0)  # diff from ms to seconds
            
        log.debug("The following metrics were gathered for external ID %s: %s" % (self.cfg['id'], str(pprint(self.metrics))))
        
        no_sql = internal.NoSql()
        self.metrics['ext_id'] = self.cfg['id']  # required NoSql key!
        no_sql.put_rcds('metrics_ext', [self.metrics])
        
        return
        
    def log_ext_cfg(self):
        
        log = logging.getLogger(module_nm + '.Ext.log_ext_cfg')

        log.debug("External configuration loaded, active parameters are:")
        for key in self.cfg:
            if 'secret' not in key and 'pw' not in key:  # crude, hard-coded check to not print out sensitive cfg
                log.info("    %s : %s" % (key, self.cfg[key]))
            else:
                log.info("    %s : **********" % key)
                
        return
    
    def chk_for_bkmrk_multi(self, rcds, bkmrk_fld, bkmrk_val, bkmrk_delim='-'):
        # Handles multi-part bookmark search
        # Expects:      rcds (list) - records to search/return/reject
        #               bkmrk_fld (str) - field in each rcd to check for bkmrk_val
        #               bkmrk_val (str) - bookmark value to search for
        #               bkmrk_delim (str) - character delim'ing multiple parts
        # Returns:      bkmrk_fnd (bool) - whether bookmark was found, signaling we're done
        
        log = logging.getLogger(module_nm + '.chk_for_bkmrk_multi')
        
        t_append = self.rcds.append
        if bkmrk_delim not in bkmrk_val:  # treat as single part
            while rcds:
                if rcds[0][bkmrk_fld] == bkmrk_val:  # multi-part first part
                    log.debug("Single-part bookmark %s found. That means we're done." % bkmrk_val)
                    return True
                t_append(rcds.pop(0))
        else:  # multi-part detected
            prev_id = ''  # track previous component of multi-part
            while rcds:
                if prev_id == bkmrk_val.split('-')[1]:  # second part found
                    if rcds[0][bkmrk_fld] == bkmrk_val.split('-')[0]:  # multi-part first part
                        log.debug("Multi-part bookmark %s found. That means we're done." % bkmrk_val)
                        bkmrk_fnd = True
                        self.rcds.pop()  # previously-appended like is redundant
                        return True
                prev_id = rcds[0][bkmrk_fld]
                t_append(rcds.pop(0))
                
        return False

    def chk_for_bkmrk_eq(self, rcds, bkmrk_fld, bkmrk_val):
        # Handles 'equal to' string comparison against bookmark
        # Expects:      rcds (list) - records to search/return/reject
        #               bkmrk_fld (str) - field in each rcd to check for bkmrk_val
        #               bkmrk_val (str) - bookmark value to search for
        # Returns:      bkmrk_fnd (bool) - whether bookmark was found, signaling we're done
        
        log = logging.getLogger(module_nm + '.chk_for_bkmrk_eq')
        
        t_append = self.rcds.append
        while rcds:
            if rcds[0][bkmrk_fld] == bkmrk_val:  # multi-part first part
                log.debug("Bookmark %s found. That means we're done." % bkmrk_val)
                return True
            t_append(rcds.pop(0))
            
        return False
    
    def chk_for_bkmrk_gt(self, rcds, bkmrk_fld, bkmrk_val):
        # Handles 'greater than' string comparison condition for records as they relate
        # to bookmark, one use when searching for chronologically descending timestamps...
        # Expects:      rcds (list) - records to search/return/reject
        #               bkmrk_fld (str) - field in each rcd to check for bkmrk_val
        #               bkmrk_val (str) - bookmark value to search for
        # Returns:      bkmrk_fnd (bool) - whether bookmark was found, signaling we're done
        
        log = logging.getLogger(module_nm + '.chk_for_bkmrk_eq')
        
        t_append = self.rcds.append
        while rcds:
            if rcds[0][bkmrk_fld] > bkmrk_val:  # string comparison
                t_append(rcds.pop(0))
                continue
            log.debug("In append mode, record with created_time of %s is less than or equal to bookmark %s. That means we're done." % (rcds[0][bkmrk_fld], bkmrk_val))
            return True
        
        return False

    def get_bkmrk(self):
        # returns current value of bookmark for external, if any
        # otherwise, returns None
    
        log = logging.getLogger(module_nm + '.get_bkmrk')
        
        # not sure how i even feel about the line below this, not used, atm
        if 'bkmrk_val' not in self.cfg:  # allow bookmark override via config (not used at time of this writing)
            bkmrk_key = self.gen_bkmrk_key()
            bkmrk_val = internal.read_cache([bkmrk_key])[bkmrk_key]
            if bkmrk_val is None:
                log.debug("No bookmark found in cache for this source, returning None.")
                return None
            else:
                log.debug("Bookmarked value of %s pulled from cache." % str(bkmrk_val))
                return str(bkmrk_val)
        else:
            return (self.cfg['bkmrk_val'])

    def put_bkmrk(self, bkmrk_val):
    
        log = logging.getLogger(module_nm + '.Ext.put_bkmrk')
        
        log.debug("Storing to cache new bookmark value of %s." % str(bkmrk_val))
        
        internal.write_cache({self.gen_bkmrk_key() : bkmrk_val})
        
    def gen_bkmrk_key(self):
        # generate bookmark key in a consistent way
        
        log = logging.getLogger(module_nm + '.Ext.gen_bkmrk_key')
        
        return str(self.cfg['id']) + '_ext_bkmrk'
    
    def do_ws_get(self, func):
        # common function for WS queries
        # Expects:      func (function) - function to use for API call
        
        log = logging.getLogger(module_nm + '.Ext.do_ws_get')
        
        io_start = time.time()
        print str(self.params)
        self.params = { key: self.params[key] for key in self.params.keys() if self.params[key] is not None }  # remove Nones
        log.debug("Submitting parameters: %s" % str(self.params))
        
        retry_intvl = 30  # 10 seconds between retries
        retry_max = 3  # allow three retries
        retry_cnt = 0
        while retry_cnt < retry_max:
            try:  # hit remote system
                rsp = func(**self.params)
                self.metrics['size_bytes'] += sys.getsizeof(str(rsp))
                break  # no errors, we're done
            except Exception as e:  # problem
                retry_cnt += 1
                if retry_cnt == retry_max:  # last try, we're done
                    log.error("All %i retries exhausted, giving up on this remote web service call." % retry_max)
                    rsp = []  # empty list appropriate to return here? None? other?
                else:
                    log.warning("Remote exception encountered: %s" % str(e))
                    log.warning("External service unavailable during attempt %i of %i, will retry in %i seconds." % (retry_cnt, retry_max, retry_intvl))
                    time.sleep(retry_intvl)
            
        if self.metrics['io_time'] == -1.0:  # first (and only?) call
            self.metrics['io_time'] = time.time() - io_start
        else:  # sum multiple calls during same run (e.g., pagination)
            self.metrics['io_time'] += time.time() - io_start

        return rsp
    
    def post_get_chld(self, chld_nm, append_prnt_rcds, rfrsh_prnt_rcds):
        # Specifically handles follow-up (to 'get') work for child adapter.
        # Expects:      chld_nm (str) is contextual name of child adapter (e.g., 'comments')
        #               append_prnt_rcds (list) are parents with child records in append mode
        #               rfrsh_prnt_rcds (list) are parents with child records in refresh mode
        # The/A major difference in this function (compared to post_get()) is that each child
        # record potentially has an updated parent record with a new or removed bookmark. This function
        # can be invoked using children from multiple parent objects with those parents represented in
        # append/rfrsh_prnts.
        
        log = logging.getLogger(module_nm + '.Ext.post_get_chld')
        
        self.metrics['rcd_cnt'] = len(self.rcds)
        
        log.info("Child adapter %s executed in %s seconds and resulted in %i records." % ((self.cfg['nm'] + ' : ' + chld_nm), str(self.metrics['io_time']), self.metrics['rcd_cnt']))
        
        # store records to NoSql store using e_rcd_id as key
        ds = internal.NoSql()
        chld_rcds = []
        for prnt in append_prnt_rcds:  # child records (per parent) in append mode
            this_prnt_id = prnt['e_rcd_id']
            chld_rcds.extend(list(reversed( [ self.rcds.pop(i) for i in reversed(xrange(len(self.rcds))) if self.rcds[i]['e_prnt_rcd_id'] == this_prnt_id ] )))
        ds.put_rcds('ext', chld_rcds)  # regular put mode
        log.info("%i child records written (append mode) for %i parent objects." % (len(chld_rcds), len(append_prnt_rcds)))
        chld_cnt_all = 0
        for prnt in rfrsh_prnt_rcds:  # child records (per parent) in refresh mode
            this_prnt_id = prnt['e_rcd_id']
            chld_rcds = [ self.rcds.pop(i) for i in reversed(xrange(len(self.rcds))) if self.rcds[i]['e_prnt_rcd_id'] == this_prnt_id ]
            chld_cnt_all += len(chld_rcds)
            chld_rcds = list(reversed(chld_rcds))  # back into correct order
            log.debug("Performing refresh of %i child records associated with parent ID %s." % (len(chld_rcds), this_prnt_id))
            ds.rfrsh_rcds('ext', chld_rcds, chk_del_rcds=True)  # assumes e_deleted checks are desired...
        log.info("%i child records checked (refresh mode) for %i parent objects." % (chld_cnt_all, len(rfrsh_prnt_rcds)))
            
        # if updated parent records were submitted (as part of child adadpter work), only now
        # once all records are written, do we update the parent record(s) in data store
        updtd_prnt_rcds = append_prnt_rcds + rfrsh_prnt_rcds  # all parent records
        log.info("Updating %i parent records belonging to external ID %i." % (len(updtd_prnt_rcds), self.cfg['id']))
        no_sql = internal.NoSql()
        params = {}
        chld_bkmrk_nm = 'e_bkmrk_chld_' + chld_nm  # accommodate multiple child adapters, hard-coded child bookmark prefix
        for rcd in updtd_prnt_rcds:
            if chld_bkmrk_nm not in rcd:  # no bookmark for child adapter
                # multiple sections separated by space (REMOVE/SET)
                params['update_expression'] = 'REMOVE ' + chld_bkmrk_nm + ' SET ' + 'e_updtd_epch_ms = :epch_ms'
                params['expression_attribute_values'] = { ':epch_ms': { 'N': str(util.get_epch_ms()) }
                                                    }
                log.debug("Removing any value for bookmark %s from parent record ID %s." % (chld_bkmrk_nm, rcd['e_rcd_id']))
            else:  # set bookmark accordingly for this parent
                # multiple attributes separated by comma in SET statement
                params['update_expression'] = 'SET ' + chld_bkmrk_nm + ' = :chld_bkmrk_val, e_updtd_epch_ms = :epch_ms'
                params['expression_attribute_values'] = { ':chld_bkmrk_val': { 'S': rcd[chld_bkmrk_nm] },
                                                      ':epch_ms': { 'N': str(util.get_epch_ms()) }
                                                    }
                log.debug("Setting value of %s for bookmark %s for parent record ID %s." % (rcd[chld_bkmrk_nm], chld_bkmrk_nm, rcd['e_rcd_id']))

            # below, lower-level API requires that item/key fields include data types
            no_sql.updt_item(   'ext',
                                {   'e_rcd_id' : { 'S' : rcd['e_rcd_id'] },  # keeping same rcd_id
                                    'ext_id' : { 'N' : str(self.cfg['id']) }
                                },
                                params
                            )
        self.cfg['id'] = self.cfg['children'][chld_nm]  # so metrics go toward child adapter

        Ext.put_ext_metrics(self)
        
    
    def post_get(self, rfrsh=False):
        # things that need to be done following any get to an external
        # 'rfrsh' triggers special handling of updating data store, default is just
        # regular put
        
        log = logging.getLogger(module_nm + '.Ext.post_get')

        self.metrics['rcd_cnt'] = len(self.rcds)

        log.info("Source %s executed in %s seconds and resulted in %i new records." % (self.cfg['nm'], str(self.metrics['io_time']), self.metrics['rcd_cnt']))
        
        # store records to NoSql store using e_rcd_id ID as key
        ds = internal.NoSql()
        if not rfrsh:
            ds.put_rcds('ext', list(self.rcds))  # regular put mode, creating copy of records
        else:
            ds.rfrsh_rcds('ext', list(self.rcds), chk_del_rcds=True)  # creating copy of records, assumes e_deleted checks are desired...

        Ext.put_ext_metrics(self)
        
    def updt_chld_adptr(self, chld_get_func, chld_nm, prnt_id_nm, enrich_func, prnt_max_age=0, get_all=False):
        # Records in self.rcds are treated as belonging to a child adapter associated
        # with respective parent records for configured parent external, and are updated
        # accordingly in the data store. Emphasis: the configuration context is actually
        # that of the parent adapter.
        # expects:      self - context of parent adapter
        #               chld_get_func (func) - which app function to invoke to retrieve records
        #               chld_nm (str) - name of child adapter (must match config)
        #               prnt_id_nm (str) - name of attribute in which parent ID resides
        #               enrich_func (func) - which app function to invoke for enrichment of records
        #               prnt_max_age (milliseconds) maximum age of parent objects that should be updated
        #               get_all - controls whether all child records of *qualifying parent records* is retrieved, checking for e_deletions
        # Neglecting to provide prnt_max_age will update child records for all parent objects associated with external.
        # Any function designated by chld_get_func should expect:
        #               prnt_id (str) - identifier of parent object
        #               bkmrk_val (str) - value of child bookmark that should be used
        # Any function designated by chld_get func should return:
        #               chld_rcds (list) - child records associated with parent object
        #               bkmrk_fnd (bool) - whether bookmark was found (controls append versus refresh)
        #               bkmrk_val (str) - new value for child bookmark
        # Right now, this function will invoke refresh mode if *any* bookmark is not found; i.e., a bookmark not being found
        # for one or more parent records will trigger refresh mode for the full set of child adapters (across parent objects).
        # This could be done on a one-to-one basis, if desired...again, consisent ordering of (child) records from external
        # is extremely important...
        
        log = logging.getLogger(module_nm + '.Ext.updt_chld_adptr')
        
        log.debug("Retrieving parent records for child adapter %i (entity: %s), whose parent adapter is external %i." % ( self.cfg['children'][chld_nm], chld_nm, self.cfg['id'] ))
        
        chld_bkmrk_nm = 'e_bkmrk_chld_' + chld_nm  # hard-coded child bookmark prefix
        no_sql = internal.NoSql()
        if prnt_max_age:
            min_epch_ms = util.get_epch_ms() - prnt_max_age
        else:
            min_epch_ms = 0  # check all parents
        # below, just need e_rcd_id of parent record and associated bookmark for child adapter, if any,
        # for all parent records represented
        no_sql.get_rcds_btw(self.cfg['id'], attrs=['e_rcd_id', 'e_eff_epch_ms', chld_bkmrk_nm, prnt_id_nm], snc_epch_ms=min_epch_ms)  # age based on e_proc_epch_ms (loose)
        init_len = len(no_sql.rcds)
        no_sql.rcds = [ rcd for rcd in no_sql.rcds if min_epch_ms <= int(rcd['e_eff_epch_ms']) ]  # so now filter based on e_eff_epch_ms
        log.debug("%i parent records retrieved, %i of which have qualifying e_eff_epch_ms." % (init_len, len(no_sql.rcds)))
        # wonder how Dynamo Streams fits into this...
        
        rfrsh = False  # instantiate as default
        append_prnts = []  # parents with child records in append mode
        rfrsh_prnts = []  # parents with child records in refresh mode
        all_chld_rcds = []  # store child records across parent records
        # below, child entities will be appended to self.rcds
        for prnt_dict in no_sql.rcds:  # loop through our parent records
            chld_rcds = []  # reset
            log.debug("TRACE: parent record is %s" % str(prnt_dict))
            time.sleep(api_delay)  # temporary(?) to avoid hammering server...
            if get_all or chld_bkmrk_nm not in prnt_dict:  # get 'em all
                if chld_bkmrk_nm not in prnt_dict:
                    log.debug("No %s bookmark exists in parent record." % chld_nm)
                if get_all:
                    log.debug("get_all flag set to True.")
                log.debug("Retrieving all available %s for this parent." % chld_nm)
                chld_rcds, bkmrk_fnd, new_bkmrk = chld_get_func(prnt_dict[prnt_id_nm])
                rfrsh = True
            else:
                log.debug("Incremental retrieval (append mode) of %s based on stored bookmark of %s." % (chld_nm, prnt_dict[chld_bkmrk_nm]))
                chld_rcds, bkmrk_fnd, new_bkmrk = chld_get_func(prnt_dict[prnt_id_nm], prnt_dict[chld_bkmrk_nm])
            log.debug("%i child records (%s) retrieved for parent ID %s." % (len(chld_rcds), chld_nm, prnt_dict[prnt_id_nm]))
            if new_bkmrk:
                prnt_dict[chld_bkmrk_nm] = new_bkmrk  # update child bookmark in parent object
            else:
                prnt_dict.pop(chld_bkmrk_nm, None)  # no value for bookmark attribute (None returned if key not found in dict, avoids exception)
            if not bkmrk_fnd:  # triggers refresh for child records associated w/ this parent
                rfrsh = True
            if rfrsh:
                rfrsh_prnts.append(prnt_dict)
            else:
                append_prnts.append(prnt_dict)
            all_chld_rcds.extend(chld_rcds)  # one big list of child records associated with all concerned parent records

        self.rcds = all_chld_rcds  # little slight-of-hand here to accommodate subsequent common functions
        self = enrich_func(self)  # enrich all child records

        # below, chld_nm value must match that in configuration
        self.post_get_chld(chld_nm, append_prnts, rfrsh_prnts)

        log.debug("Child (%s) adapter work completed." % chld_nm)
        
class Google(Ext):
    def __init__(self, ext_id, ext_cfg):
        Ext.__init__(self, ext_id, ext_cfg)
        self.ext_id = ext_id
        self.ext_cfg = ext_cfg
        self.unauth_cfg = {}
        self.unauth_cfg['client_email'] = 'crav-beta@chrome-epigram-133019.iam.gserviceaccount.com'  # hard-coded app client email
        self.unauth_cfg['key_path'] = './private/MyProject-acfa4fc018ed.p12'  # hard-coded path to p12 key
        
class GoogleAxRprt(Google):
    # google analytics reporting
    def __init__(self, ext_id, ext_cfg):
    
        log = logging.getLogger(module_nm + '.GoogleAxRprt')
        
        Google.__init__(self, ext_id, ext_cfg)
        self.unauth_cfg['scopes'] = ['https://www.googleapis.com/auth/analytics']  # hard-coded scope
        self.unauth_cfg['api_nm'] = 'analytics'  # hard-coded API name
        self.unauth_cfg['api_ver'] = 'v4'  # hard-coded API version
        creds = ServiceAccountCredentials.from_p12_keyfile(
            self.unauth_cfg['client_email'], self.unauth_cfg['key_path'], self.unauth_cfg['scopes'])
        self.auth = creds.authorize(Http())  # used to make api calls
        self.svc = build(self.unauth_cfg['api_nm'], self.unauth_cfg['api_ver'], http=self.auth)

    def get_service(api_name, api_version, scope, key_file_location,
                    service_account_email):
      """Get a service that communicates to a Google API.

      Args:
        api_name: The name of the api to connect to.
        api_version: The api version to connect to.
        scope: A list auth scopes to authorize for the application.
        key_file_location: The path to a valid service account p12 key file.
        service_account_email: The service account email address.

      Returns:
        A service that is connected to the specified API.
      """

      credentials = ServiceAccountCredentials.from_p12_keyfile(
        service_account_email, key_file_location, scopes=scope)

      http = credentials.authorize(httplib2.Http())

      # Build the service object.
      service = build(api_name, api_version, http=http)

      return service
    
#    {
#  "access_token":"1/fFAGRNJru1FTz70BzhT3Zg",
#  "expires_in":3920,
#  "token_type":"Bearer"
#}
    
    

class GoogleUsr(Google):
    def __init__(self, ext_id, ext_cfg):
        
        log = logging.getLogger(module_nm + '.GoogleUsr')
        
        Google.__init__(self, ext_id, ext_cfg)
        if 'access_token' in util.cfg_app['auth']['google']['user'][self.cfg['auth']]:
            self.access_token = util.cfg_app['auth']['google']['user'][self.cfg['auth']]['access_token']
        self.id = self.cfg['auth']  # user ID configured in external
        self.params = {}
        self.get_usr_auth()

    def get_usr_auth(self):
        # retrieve/refresh any user auth info already in the application
        
        log = logging.getLogger(module_nm + '.GoogleUsr.get_usr_auth')
        
#        self.auth_cfg = {}
        
#        usr = GoogleUsr(self.ext_id, self.ext_cfg)
        cfg_auth = util.get_cfg_auth()
        
        try:
            usr_auth = cfg_auth['google']['user'][self.id]
        except:
            log.warning("No existing auth credentials found for user %s, returning None." % self.id)
            return None

        if 'refresh_token' not in usr_auth:
            log.warning("No refresh token in existing auth credentials for user %s. This implies the " +
                "user was never registered with the application, returning None.")
            return None
        
        log.debug("Existing user auth credentials retrieved: %s" % usr_auth)

        # check expiration and refresh if necessary
        exp_epch_ms = int(usr_auth['snc_epch_ms']) + int(usr_auth['expires']) * 1000
        if exp_epch_ms > util.get_epch_ms():  # compare expiration time versus now
            self.rfrsh_usr_access_token()
#        self.auth_cfg['access_token'
#                      util.cfg_app['auth']['google']['app']['client_id']

    def rfrsh_usr_access_token(self):
        # utilizes pre-stored refresh token to retrieve new access token
        # returns access_token and expires
        
        log = logging.getLogger(module_nm + '.Google.rfrsh_usr_access_token')
        
        params = {}
        params['refresh_token'] = self.auth_cfg['refresh_token']
        params['client_id'] = self.unauth_cfg['client_id']
        params['client_secret'] = self.unauth_cfg['client_secret']
        params['grant_type'] = 'refresh_token'
        
        rsp = requests.post(url=self.unauth_cfg['refresh_url'], params=params).json()
        
        print str(rsp)
        raise
        
class Fb(Ext):
    def __init__(self, ext_id, ext_cfg):
        Ext.__init__(self, ext_id, ext_cfg)
        self.api_host = 'https://graph.facebook.com'
        self.api_ver = 'v2.4'
        self.api_base = self.api_host + '/' + self.api_ver + '/'
        self.ext_id = ext_id
        self.ext_cfg = ext_cfg
        self.unauth_cfg = {}
        # below, hard-coded for dev purposes....will be same for entire app, though (?)
        self.unauth_cfg['client_id'] = util.cfg_app['auth']['fb']['app']['client_id']
        self.unauth_cfg['client_secret'] = util.cfg_app['auth']['fb']['app']['client_secret']
        self.unauth_cfg['redirect_uri'] = 'http://localhost:' + util.cfg_app['env']['ui_port'] + '/auth/fb_callback'  # hard-coded hostname
        
    def req_access_code(self):
        # first step in auth process
        
        url = 'https://www.facebook.com/dialog/oauth'
        url += '?client_id=' + self.unauth_cfg['client_id']
        url += '&redirect_uri=' + self.unauth_cfg['redirect_uri']
        url += '&response_type=' + 'code'  # two-step code/token auth
        url += '&scope=manage_pages,publish_pages'  # hard-coded list of permissions

        return url
    
    def reg_usr(self, code):
        # manages (re)registration of user with application
        
        log = logging.getLogger(module_nm + '.Fb.reg_usr')
        
        usr_auth = self.get_usr_access_token(code)

        # Let's get User ID/information
        usr = FbUsr(self.ext_id, self.ext_cfg)
        usr.access_token = usr_auth['access_token']  # override any existing token with new one
        rsp = usr.get_me_info()
        
        # right now, only saving off ID and name for use in registering user with Enigma
        usr_id = rsp['id']
        usr_auth['nm'] = rsp['name']

        # update configuration with user-level tokens
        cfg_auth = util.get_cfg_auth()
        
        if 'fb' not in cfg_auth['auth']:
            cfg_auth['auth']['fb'] = {}
        if 'user' not in cfg_auth['auth']['fb']:
            cfg_auth['auth']['fb']['user'] = {}
        # below will overwrite any existing for this user ID
        cfg_auth['auth']['fb']['user'][str(usr_id)] = usr_auth

        util.updt_cfg_aspect(cfg_auth)  # update config

        log.info("Configuration updated with new access token and user information for user %s." % usr_auth['nm'])
        
        return "Configuration updated with new access token and user information for user %s." % usr_auth['nm']
    
    def get_usr_access_token(self, code):
        # returns dictionary of fresh user auth info:
        #       access_token, snc_epch_ms, expires
        
        log = logging.getLogger(module_nm + '.Fb.get_usr_access_token')
        
        self.api_url = 'https://graph.facebook.com/v2.3/oauth/access_token'  # reference self.api_base?
        params = {}
        params['client_id'] = self.unauth_cfg['client_id']
        params['redirect_uri'] = self.unauth_cfg['redirect_uri']
        params['client_secret'] = self.unauth_cfg['client_secret']
        params['code'] = code

        # all necessary information returned in response below
        # (redirect param is not actually followed)
        rsp = requests.get(url=self.api_url, params=params)
        
        # below, can't believe i have to(?) resort to manually parsing this...
        # seems like a string is returned when documentation online suggests rsp.text
        # would be a dict...and no, json.loads() did not seem to work for me...
        try:
            rsp_params = json.loads(rsp.text)
#            rsp_params = { nm_val.split('=')[0] : nm_val.split('=')[1] for nm_val in rsp.text.split('&') }  # dict from string
        except Exception as e:
            log.warning("Problem retrieving access_token and expires_in. Error: %s." % str(e))
            return "Could get get access token."

        # start user-level entry to authentication
        usr_auth = {}
        usr_auth['access_token'] = rsp_params['access_token']
        usr_auth['snc_epch_ms'] = util.get_epch_ms()  # *now* in epoch ms
        # 2016-02-17
        # FB updated 'expires' key to 'expires_in', although for some reason
        # I am not seeing it on the response back...sooo...assume 30 days
        # if not observed
        if 'expired_in' in rsp_params:
            usr_auth['expires'] = rsp_params['expires_in']  # seconds 'til expiry
        else:
            log.info('expires_in not seen in response, so setting to default of 30 days (in seconds)')
            usr_auth['expires'] = 3600 * 24 * 30  # 30 days in seconds

        log.debug('usr_auth information retrieved is %s.' % str(usr_auth))
        return usr_auth

    def get_next_data_pg(self, rsp=[]):
        # Handles paging of data array, intended to support, e.g., Fb.do_fb_data_get()
        # Expects:      rsp (list/array) - current response from FB
        # Returns:      True/False - whether there is a new page; if True, updates self.params['api-url']

        log = logging.getLogger(module_nm + '.Fb.get_next_data_pg')
        
        if 'paging' in rsp:  # supposedly 'paging' will always be here, just may not have 'next' in it
            log.debug("Paging object returned in response.")
            if 'next' in rsp['paging']:
                log.debug("'next' object returned in paging object, setting URL for next call. %i records so far." % len(self.rcds))
                if 'created_time' in self.rcds[len(self.rcds) - 1]:  # some entities have this field
                    log.debug("Last object in that last set has timestamp of %s, so will start after there on this next call." % self.rcds[len(self.rcds) - 1]['created_time'])
                self.params['api_url'] = rsp['paging']['next']  # all set for starting over in while loop
                time.sleep(api_pg_delay)  # be nice to FB, we like FB
                return True
            else:
                log.debug("No 'next' object in paging object, we're done.")
                self.params['api_url'] = None  # done
                return False
        else:
            log.debug("No paging object in response, we're done.")
            self.params['api_url'] = None  # done
            return False
    
    def do_fb_data_get(self, parse_func, bkmrk={}, bkmrk_func=Ext.chk_for_bkmrk_eq):
        # bkmrk_type (enum): 'str' (string comparison), 'eq' (equal to), 'multi' (multipart)
        
        log = logging.getLogger(module_nm + '.Fb.do_fb_data_get')
        
        if 'limit' not in self.params:
            self.params['limit'] = 100  # default max number ofjects in data array (per call)
            
        if not bkmrk:
            log.debug("No bookmark information provided, will return all records retrieved.")
        elif len(bkmrk) > 1:
            log.warning("Bookmark dict is greater than length 1. This is invalid, aborting this request.")
            return
        else:
            bkmrk_fld = bkmrk.keys()[0]
            bkmrk_val = bkmrk[bkmrk_fld]
            if not bkmrk_val:  # empty string or None: app-managed bookmark
                log.warning("Bookmark field provided, but no associated value. Defaulting to returning all records.")
                bkmrk = {}
            else:
                log.debug("Using bookmark field %s with value of %s with bookmark function %s." % (bkmrk_fld, bkmrk_val, str(bkmrk_func)))

        bkmrk_fnd = False  # track whether bookmark interrupted processing
        rsp = self.do_ws_get(do_fb_get)
        call_cnt = 0
        while 'data' in rsp:
            call_cnt += 1
            if not bkmrk:  # get 'em all
                self.rcds.extend( [ rcd for rcd in rsp['data'] ] )
            else:  # bookmark work to be done
                if bkmrk_func(self, rsp['data'], bkmrk_fld, bkmrk_val):  # appends to self.rcds, regardless
                    bkmrk_fnd = True  # bookmark found
                    break
            if not self.get_next_data_pg(rsp):  # check for another page (updates 'api_url', if there)
                break
            rsp = self.do_ws_get(do_fb_get)  # another page found, let's get it...
            call_cnt += 1
        
        log.info("%i records being kept from sequence of %i API calls." % (len(self.rcds), call_cnt))
        
        self.rcds = [ parse_func(rcd) for rcd in self.rcds ]  # run 'em through parser
        
        return bkmrk_fnd  # optionally serve as basis for downstream rfrsh value (bkmrk_fnd==False means rfrsh==True)


#    def get_fb_post(self, post_ids):
#        # gets post objects
#        # Expects:      post_ids (list)
        
#        log = logging.getLogger(module_nm + 'Fb.get_fb_post')
        
#        # can accomplish retrieval of multiple posts at once via
#        # http://graph.facebook.com/posts?ids={post-id-a},{post-id-b}
#        self.params['api_url'] = self.api_base + post_ids[0]
#        rsp = self.do_ws_get(do_fb_get)
        
#        print rsp
#        raise

#        self.rcds.extend(parse.fb_obj(rsp, self.params['fields']))  # single pg_info object
#        self = enrich.fb_pg(self)
#        self.post_get()

#        log.debug("Facebook page info work completed.")



#        # might want to roll below into common FB function (e.g., do_fb_data_get())
#        rsp = self.do_ws_get(self.do_fb_get)
#        self = enrich.fb_post(self)
#        self.post_get()

#        log.debug("Facebook page feed work completed.")

#        ----------

#        Some random sample code:
        
#        https://graph.facebook.com/PAGE_ID/feed?fields=comments.limit(1).summary(true),likes.limit(1).summary(true)
#        Returns a response like:
#        {
#          "data": [
#            {
#              ....
#              "summary": {
#                "total_count": 56
#              }
#              ...
#            }, 
#            {
#              ....
#              "summary": {
#                "total_count": 88
#              }
#              ...
#            }
#          ]
#        }
        
    def get_fb_likes(self, obj_id, bkmrk_val=''):
        # Likes always run as a child adapter.
        # Gets Likes information for FB object, consisting of array of FB user objects.
        # Expects:      obj_id (str)
        #               bkmrk_val (str) - value at which to stop fetching; if not provided, returns all
        #
        # Right now, since I'm inept and cannot immediately determine how to return an entire
        # user object (or selected fields) in the 'data' array, this function performs two steps:
        #       - query for user IDs of those who liked the object
        #       - query again for user information associated with each ID
        # These two calls should be rolled into one, when we get a chance...
        
        log = logging.getLogger(module_nm + '.Fb.get_fb_likes')
        
        log.debug("Begin retrieval of Facebook likes.")

        if bkmrk_val:
            bkmrk = { 'id' : bkmrk_val }
        else:  # retrieve all
            log.debug("No bookmark provided, will retrieve all.")
            bkmrk = {}

        # could set this up such that likes are retrieved for multiple objects in single
        # API call, but for now...single object...
        # setup params
        self.params = {}
        self.params['api_url'] = self.api_base + obj_id + '/likes'
        self.params['access_token'] = self.access_token
        self.params['limit'] = 100  # only returning ID for each like/user, at the moment...
        # grrrr....surely there is a way to return all desired fields in each user object returned
        # in array, but i'm having trouble getting it to work...
#        if 'fields' not in self.params:
#            self.params['fields'] = 'users{id,email,first_name,gender,last_name,link,location,updated_time,name}'
#            self.params['fields'] = 'id,email,first_name,gender,last_name,link,location,updated_time,name'
    
        bkmrk_fnd = self.do_fb_data_get(parse.fb_obj, bkmrk=bkmrk, bkmrk_func=Ext.chk_for_bkmrk_multi)
        
        log.debug('Records returned:')
        log.debug(str(self.rcds))

        # below, if bookmark wasn't found, it could because entity that served as bookmark
        # was deleted from remote system...switch rfrsh to True in order to perform
        # reconciliation against data store...
            
#        e.g.: fields=photos.fields(id,images{source}).limit(1)
        
        # this extra step because I did not determine yet how to get all/more user fields
        # back from call to likes endpoint.../sigh...    
        usr_ids = [ rcd['id'] for rcd in self.rcds ]  # save off user IDs
        self.rcds = []  # reset
        if len(usr_ids) > 0:
            self.get_fb_usr_info(usr_ids)
        
        log.debug("Facebook likes work completed.")

        return bkmrk_fnd  # can serve as indicator whether data store should be refreshed
    
    def get_fb_cmnts(self, obj_id, bkmrk_val=''):
        # Comments always run as a child adapter.
        # Gets comments information for FB object, consisting of array of FB comment objects.
        # In 'stream' mode, comments returned in chronological order, OLDEST FIRST, so there is
        # no way to incrementally check for new comments ('toplevel' mode?). So all comments come
        # back every time. No bookmark, no appending.
        # Expects:      obj_id (str)
        #               bkmrk_val (str) - value at which to stop fetching; if not provided, returns all
        #                       (CURRENTLY NOT USED)
        
        log = logging.getLogger(module_nm + '.Fb.get_fb_cmnts')
        
        log.debug("Begin retrieval of Facebook comments.")

        # could set this up such that likes are retrieved for multiple objects in single
        # API call, but for now...single object...
        # setup params
        self.params = {}
        self.params['api_url'] = self.api_base + obj_id + '/comments'
        self.params['access_token'] = self.access_token
        self.params['limit'] = 100  # only returning ID for each like/user, at the moment...
        # below, two options: stream or toplevel, latter does not include replies to comments
        self.params['filter'] = 'stream'
        if 'fields' not in self.params:  # set default fields to return (why does this not work for likes?)
            self.params['fields'] = [ 'created_time', 'message', 'from', 'id', 'comment_count', 'like_count', 'parent', 'attachment' ]
    
        # Bookmarking not supported, will never be found (always False)
        bkmrk_fnd = self.do_fb_data_get(parse.fb_cmnt)

        # below, if bookmark wasn't found, it could because entity that served as bookmark
        # was deleted from remote system...switch rfrsh to True in order to perform
        # reconciliation against data store...
        
        log.debug("Facebook comments work completed.")

        return bkmrk_fnd  # will always be False right now
        
    def get_fb_usr_info(self, ids=[]):
        # Right now, this function is NOT to utilize bookmarking; i.e., other functions
        # perform entity management in the data store. This function serves purely to
        # return user information for all user IDs requested and performs no writes to
        # the data store on its own.
        # Expects: ids (list) - user IDs to retrieve info for
        #
        
        log = logging.getLogger(module_nm + '.Fb.get_fb_user_info')
        
        log.debug("Begin retrieval of Facebook user info for %i users." % len(ids))
        
        # setup params
        self.params = {}
        self.params['access_token'] = self.access_token
        self.params['api_url'] = self.api_base
        self.params['fields'] = [ 'id', 'email', 'first_name', 'gender', 'last_name', 'link', 'location', 'updated_time', 'name' ]

        # below should be pulled out in common FB function (e.g., get_fb_ids), as this model is used
        # for various objects, including:
        #       users, posts, pages, [other?]
        batch_lmt = 25  # max number of IDs to submit in a single query; current FB max: 50
        cnt = 1  # number of calls
        while True:
            self.params['ids'] = []  # reset IDs to look-up in this batch
            while ids and len(self.params['ids']) < batch_lmt:  # still something in list
                self.params['ids'].append(ids.pop(0))  # build our batch
            if self.params['ids']:  # do we have IDs to look up?
                rsp = self.do_ws_get(do_fb_get)  # make the API call
                if 'error' in rsp:
                    log.warning("Facebook returned an error message: %s" % str(rsp['error']))
                    log.warning("Will proceed with processing, but previous call should be reviewed for point of this error.")
                else:               
                    self.rcds.extend( [ parse.fb_obj(rsp[id]) for id in self.params['ids'] ] )  # maintain sequence
            if ids:  # throttle if there's going to be another call
                time.sleep(api_pg_delay)
                cnt += 1
            else:  # we're done
                break
                
        log.debug("%i user information objects returned in %i API calls." % (len(self.rcds), cnt))

#        e.g.,: batch=[{"method":"GET", "relative_url":"me"},{"method":"GET", "relative_url":"me/friends?limit=50"}]

        log.debug("Done retrieving user information for %i users." % len(self.rcds))

        return self

class FbUsr(Fb):
    def __init__(self, ext_id, ext_cfg):
        
        log = logging.getLogger(module_nm + '.FbUsr')
        
        Fb.__init__(self, ext_id, ext_cfg)
#        try:  # get required
        self.access_token = util.cfg_app['auth']['fb']['user'][self.cfg['auth']]['access_token']
        self.id = self.cfg['auth']  # user ID configured in external
        self.nm = util.cfg_app['auth']['fb']['user'][self.cfg['auth']]['nm']
#        except KeyError:  # config does not exist or is incomplete
#            log.warning("While instantiating FbUsr object, required configuratoin found missing...")
        self.params = {}
        
    def reg_pgs(self):
        # # manages (re)registration of pages associated with user
        
        log = logging.getLogger(module_nm + '.FbUsr.reg_pgs')
        
        pg_auths = self.get_pg_access_tokens()
        
        # below, should also include paging logic; there's a 'paging' object returned
        # alongside the 'data' object, presumably to accommodate large number of pages...
        pg_cfgs = {}  # will hold our new page configurations
        for auth in pg_auths['data']:  # each set of page credentials
            # right now, only saving off ID and name for use in registering page with Enigma
            pg_cfgs[auth['id']] = {}
            pg_cfgs[auth['id']]['nm'] = auth['name']
            pg_cfgs[auth['id']]['access_token'] = auth['access_token']
            pg_cfgs[auth['id']]['usr_id'] = self.id  # user ID associated with this auth
            # nothing with auth['perms'] right now but could make a good check before loading cfg

        # update configuration with user-level tokens
        cfg_auth = util.get_cfg_auth()

        if 'page' not in cfg_auth['auth']['fb']:
            cfg_auth['auth']['fb']['page'] = {}
        
        # below will overwrite any conflicting page (ID) configs
        # note: currently no support for maintaining multiple tokens for same page; i.e.,
        # tokens for multiple admins/users for a single page
        cfg_auth['auth']['fb']['page'].update(pg_cfgs)

        util.updt_cfg_aspect(cfg_auth)  # update config
        
        print cfg_auth['auth']['fb']['page']

        log.info("Configuration updated with new access token and page information for pages %s." % str([ cfg_auth['auth']['fb']['page'][key]['nm'] for key in cfg_auth['auth']['fb']['page'] ]))
        
        return "Configuration updated with new access token and page information for pages %s." % str([ cfg_auth['auth']['fb']['page'][key]['nm'] for key in cfg_auth['auth']['fb']['page'] ])
        
        
    def get_pg_access_tokens(self):
        # using user access token, retrieve page access tokens to allow admin of pages,
        # list of page auth objects returned from WS
        
        self.params = {}
        self.params['api_url'] = self.api_base + self.id + '/accounts'
        self.params['access_token'] = self.access_token
        rsp = self.do_ws_get(do_fb_get)

        return rsp

    def get_me_info(self):
        # returns, e.g.:
        # {"id":"10153093547883597","first_name":"Jeff","gender":"male","last_name":"Leake"
        #"link":"https:\/\/www.facebook.com\/app_scoped_user_id\/10153093547883597\/","locale":"en_US"
        # "name":"Jeff Leake","timezone":-4,"updated_time":"2014-11-08T06:37:08+0000","verified":true}
        
        log = logging.getLogger(module_nm + '.FbUsr.get_me_info')
        
        self.params['api_url'] = self.api_base + 'me'
        self.params['access_token'] = self.access_token
        rsp = self.do_ws_get(do_fb_get)
        
        log.info("User information retrieved for %s." % rsp['name'])
        
        return rsp
    
class FbPg(Fb):
    def __init__(self, ext_id, ext_cfg):
        Fb.__init__(self, ext_id, ext_cfg)
        
        log = logging.getLogger(module_nm + 'FbPg')
#        try:  # get required
        self.access_token = util.cfg_app['auth']['fb']['page'][self.cfg['auth']]['access_token']
        self.id = self.cfg['auth']  # page ID configured in external
        self.nm = util.cfg_app['auth']['fb']['page'][self.cfg['auth']]['nm']
#        except KeyError:  # config does not exist or is incomplete
#            log.warning("While instantiating FbPg object, required configuratoin found missing...")
        self.params = {}
        
    def get_pg_info(self):
        # gets snapshot of current state of page (various fields)
        
        log = logging.getLogger(module_nm + 'FbPg.get_pg_info')
        
        log.debug("Begin Facebook page info.")
        
        self.params = {}
        self.params['api_url'] = self.api_base + self.id 
        self.params['fields'] = ['likes']  # fields to retrieve/parse
        self.params['access_token'] = self.access_token
        
        rsp = self.do_ws_get(do_fb_get)

        self.rcds.append(parse.fb_obj(rsp, self.params['fields']))  # single pg_info object
        
        self = enrich.fb_pg(self)
        self.post_get()

        log.debug("Facebook page info work completed.")
        
    def get_pg_posts(self, bkmrk_val='', flds=[], get_all=False):
        # Retrieves post objects in page's feed, those made by the page and to the page,
        # manages detection of new/deleted posts.
        # Expects:      bkmrk_val (str) - provide for manual submission of bookmark (created_time)
        #               flds (list) - fields to retrieve for each post
        #               get_all (bool) - overrides bookmark, gets all (new bookmark committed to data store)
        
        log = logging.getLogger(module_nm + '.FbPg.get_pg_posts')
        
        log.debug("Begin Facebook page posts.")
        
        dflt_flds = [ 'id', 'message', 'created_time', 'caption', 'description', 'from', 'icon', 'link', 'message_tags', 'name', 'object_id', 'picture', 'place', 'shares', 'source', 'status_type', 'story', 'to', 'type', 'updated_time', 'with_tags' ]

        self.params = {}
        self.params['api_url'] = self.api_base + self.id + '/feed'
        self.params['access_token'] = self.access_token

        if not flds:
            flds = dflt_flds
        self.params['fields'] = flds

        bkmrk_fld = 'created_time'
        # might want to roll all below into common FB function (e.g., do_fb_data_get())
        bkmrk_fnd = self.do_fb_data_get(parse.fb_post, bkmrk={ bkmrk_fld : bkmrk_val }, bmkrk_func=Ext.chk_for_bkmrk_gt)
        if len(self.rcds) > 0:
            self.put_bkmrk(self.rcds[0][bkmrk_fld])  # update w/ newest record
        
        self = enrich.fb_post(self)
        if get_all or not bkmrk_fnd:  # need to check/reconcile
            rfrsh = True
        else:  # normal append operation
            rfrsh = False
        self.post_get(rfrsh=rfrsh)
        
        log.debug("Facebook page posts completed.")
        
    def get_pg_feed(self, bkmrk_val='', flds=[], get_all=False):
        # Retrieves all posts in page feed;
        # manages detection of new/deleted posts.
        # Expects:      bkmrk_val (str) - provide for manual submission of bookmark (created_time)
        #               flds (list) - fields to retrieve for each post
        #               get_all (bool) - overrides bookmark, gets all (new bookmark committed to data store)
        
        log = logging.getLogger(module_nm + '.FbPg.get_pg_feed')
        
        log.debug("Begin Facebook page feed work.")
        
        dflt_flds = [ 'id', 'message', 'created_time', 'caption', 'description', 'from', 'icon', 'link', 'message_tags', 'name', 'object_id', 'picture', 'place', 'shares', 'source', 'status_type', 'story', 'to', 'type', 'updated_time', 'with_tags' ]

        self.params = {}
        self.params['api_url'] = self.api_base + self.id + '/feed'
        self.params['access_token'] = self.access_token

        if not flds:
            flds = dflt_flds
        self.params['fields'] = flds

        bkmrk_fld = 'created_time'
        if get_all:
            bkmrk_val = ''
        else:
            bkmrk_val = self.get_bkmrk()
        # might want to roll all below into common FB function (e.g., do_fb_data_get())
        bkmrk_fnd = self.do_fb_data_get(parse.fb_post, bkmrk={ bkmrk_fld : bkmrk_val }, bkmrk_func=Ext.chk_for_bkmrk_gt)
        
        self = enrich.fb_post(self)
        if get_all or not bkmrk_fnd:  # need to check/reconcile
            rfrsh = True
        else:  # normal append operation
            rfrsh = False
        self.post_get(rfrsh=rfrsh)
        
        if len(self.rcds) > 0:
            self.put_bkmrk(self.rcds[0][bkmrk_fld])  # update bookmark w/ newest record
        
        log.debug("Facebook page feed work completed.")
        
    def get_pg_likes(self, post_id, bkmrk_val=''):
        # Get likes associated with specific post, helper function invoked by upper (updt)
        # function which performs post WS and data store work.
        # Expects:      post_id
        #               bkmrk_val (str) - first user ID (new->) old that should _not_ be retrieved
        # Returns:      self.rcds (list) - qualifying likes
        #               bkmrk_fnd (bool)
        #               new_bkmrk (str)
        # Returns discrete list of like (i.e., user) objects, per child adapter spec
        
        log = logging.getLogger(module_nm + '.FbPg.get_pg_likes')
        
        log.debug("Retrieving likes for Facebook post ID %s." % post_id)
        
        self.rcds = []  # reset
        bkmrk_fnd = self.get_fb_likes(post_id, bkmrk_val)  # common FB likes function

        # need parent ID in each child record
        if len(self.rcds) > 0:
            log.debug("%i child records returned for parent ID %s." % (len(self.rcds), post_id))
            for i in xrange(len(self.rcds)):  # since this is child
                self.rcds[i]['prnt_id'] = post_id  # insert parent ID
        
        # handle new bookmark work
        if len(self.rcds) > 1:
            new_bkmrk = self.rcds[0]['id'] + '-' + self.rcds[1]['id']
        elif len(self.rcds) == 1:
            if bkmrk_val:  # if pre-existing bookmark
                new_bkmrk = self.rcds[0]['id'] + '-' + bkmrk_val.split('-')[0]
            else:
                new_bkmrk = self.rcds[0]['id']
        else:
            new_bkmrk = None
            
        return self.rcds, bkmrk_fnd, new_bkmrk
        
    def updt_pg_likes(self, prnt_max_age=None, get_all=False):
        # Parent adapter stores likes as child adapter, this function updates likes
        # associated with FB post objects for configured parent adapter.
        # Expects:      self - context of parent adapter
        #               prnt_max_age (int: milliseconds) - maximum age of post objects that should be updated
        
        log = logging.getLogger(module_nm + '.FbPg.updt_pg_likes')
        
        log.debug("Begin Facebook page likes.")
        
        self.updt_chld_adptr(self.get_pg_likes, 'likes', 'id', enrich.fb_like, prnt_max_age=prnt_max_age, get_all=get_all)

        log.debug("Facebook page likes work completed.")
        
    def get_pg_cmnts(self, post_id, bkmrk_val=''):
        # Get comments associated with specific post, helper function invoked by upper (updt)
        # function which performs post WS and data store work.
        # Expects:      post_id
        #               bkmrk_val (str) - first comment ID (new->) old that should _not_ be retrieved
        #                       (NOT CURRENTLY USED)
        # Returns:      comments (list) - qualifying comments
        #               bkmrk_fnd (bool)
        #               new_bkmrk (str)
        # Returns discrete list of comment objects, per child adapter spec
        
        log = logging.getLogger(module_nm + '.FbPg.get_pg_cmnts')
        
        log.debug("Retrieving comments for Facebook post ID %s." % post_id)
        
        self.rcds = []  # reset
        bkmrk_val = ''  # hard-coded: no bookmarks for comments
        bkmrk_fnd = self.get_fb_cmnts(post_id, bkmrk_val)  # common FB comments function

        # need parent ID in each child record
        if len(self.rcds) > 0:
            log.debug("%i child records returned for parent ID %s." % (len(self.rcds), post_id))
            for i in xrange(len(self.rcds)):  # since this is child
                self.rcds[i]['prnt_id'] = post_id  # insert parent ID (should prolly be 'e_prnt_id'
        
        new_bkmrk = None  # bookmark not used for comments right now due to API
            
        return self.rcds, bkmrk_fnd, new_bkmrk  # bkmrk_fnd always False right now
    
    def updt_pg_cmnts(self, prnt_max_age=None, get_all=False):
        # Parent adapter stores comments as child adapter, this function updates comments
        # associated with FB post objects for configured parent adapter.
        # Expects:      self - context of parent adapter
        #               prnt_max_age (int: milliseconds) - maximum age of post objects that should be updated
        #               get_all (bool) - get all comments associated with qualifying parent records, triggers check for e_deletions
        
        log = logging.getLogger(module_nm + '.FbPg.updt_pg_cmnts')
        
        log.debug("Begin Facebook page comments.")
        
        self.updt_chld_adptr(self.get_pg_cmnts, 'comments', 'id', enrich.fb_cmnt, prnt_max_age=prnt_max_age, get_all=get_all)

        log.debug("Facebook page comments work completed.")
        
        
class Inst(Ext):
    def __init__(self, ext_id, ext_cfg):
        Ext.__init__(self, ext_id, ext_cfg)
        self.unauth_cfg = {}
        # below, hard-coded for dev purposes....will be same for entire app, though (?)
        self.unauth_cfg['client_id'] = util.cfg_app['auth']['inst']['user'][self.cfg['auth']]['client_id']
        self.unauth_cfg['client_secret'] = util.cfg_app['auth']['inst']['user'][self.cfg['auth']]['client_secret']
        self.unauth_cfg['redirect_uri'] = 'http://localhost:' + util.cfg_app['env']['ui_port'] + '/auth/inst_callback'  # hard-coded hostname
        self.unauth_api = client.InstagramAPI(**self.unauth_cfg)

    def req_access_token(self):

        try:
            url = self.unauth_api.get_authorize_url(scope=['likes', 'comments'])  # hard-coded, what we want access to
            return url
        except Exception as e:
            print(e)
            
    def get_access_token(self, code):
        # expects code returned in token request response URL
        
        log = logging.getLogger(module_nm + '.Inst.get_access_token')
            
        try:
            access_token, user_info = self.unauth_api.exchange_code_for_access_token(code)
            if not access_token:
                log.warning("Unable to retrieve access token.")
                return 'Could not get access token'
        except Exception as e:
            print(e)

        # update configuration with user-level tokens
        cfg_auth = util.get_cfg_auth()
        
        # below will overwrite any existing
        cfg_auth['auth']['inst']['user'][self.cfg['auth']]['access_token'] = access_token
        util.updt_cfg_aspect(cfg_auth)  # update config
        
        log.info("Configuration updated with new access token.")
        
        return access_token, user_info

class InstUsr(Inst):
    def __init__(self, ext_id, ext_cfg):
        Inst.__init__(self, ext_id, ext_cfg)
        self.api = client.InstagramAPI(access_token=str(util.cfg_app['auth']['inst']['user'][self.cfg['auth']]['access_token']), client_secret=str(util.cfg_app['auth']['inst']['user'][self.cfg['auth']]['client_secret']))  # these cannot be unicode (see Python HMAC 'feature')
        self.params = {}
        
    def updt_media_likes(self, prnt_max_age=None, get_all=False):
        # parent adapter stores likes as child adapter
        # update likes associated with media records for configured external
        # expects:      self - context of parent adapter
        #               prnt_max_age (milliseconds) - maximum age of media objects that should be updated
        #               get_all - get all likes associated with qualifying parent records, triggers check for e_deletions
        # Question: likes aren't in order from newest to oldest??
        # Answer: Yes, they should be, but since there is no pagination, and all likes are
        # returned in a single call, anyway...guess you could still track e_deletes...
        
        log = logging.getLogger(module_nm + '.InstUsr.updt_media_likes')
        
        log.debug("Begin Instagram media likes work.")
        
        self.updt_chld_adptr(self.get_media_likes, 'likes', 'id', enrich.inst_like, prnt_max_age=prnt_max_age, get_all=get_all)

        log.debug("Instagram media likes work completed.")
        
    def updt_media_cmnts(self, prnt_max_age=None, get_all=False):
        # parent adapter stores comments as child adapter
        # update comments associated with media records for configured external
        # expects:      self - context of parent adapter
        #               prnt_max_age (milliseconds) maximum age of media objects that should be updated
        #               get_all - get all comments associated with qualifying parent records, triggers check for e_deletions
        # prnt_max_age of None will update comments for all media associated with external
        
        log = logging.getLogger(module_nm + '.InstUsr.updt_media_cmnts')
        
        log.debug("Begin Instagram media comments work.")
        
        self.updt_chld_adptr(self.get_media_cmnts, 'comments', 'id', enrich.inst_cmnt, prnt_max_age=prnt_max_age, get_all=get_all)

        log.debug("Instagram media comments work completed.")
        
        
    def get_media_cmnts(self, media_id, min_cmnt_id=None):
        # Get comments posted to a specific media, helper function invoked by upper function which
        # performs post WS and data store work.
        # expects media_id (str)
        #         min_cmnt_id (str), most recent/maximum comment ID; if not passed, will get all
        # no min_cmnt_id will retrieve all available
        # returns discrete list of comment objects, makes no changes to Ext object
        
        log = logging.getLogger(module_nm + '.InstUsr.get_media_cmnts')
        
        log.debug("Retrieving comments for Instagram media ID %s." % media_id)
        
        if min_cmnt_id:
            log.debug("Retrieving only comments with ID newer than %s." % min_cmnt_id)
        else:
            log.debug("Retrieving all comments for this media.")
        
#        self.params['pagination_format'] = 'dict'
        self.params['media_id'] = media_id

        cmnts = []  # list of comments that will be returned
        media_cmnts = self.do_ws_get(self.api.media_comments)  # needs to be unicode...?

        bkmrk_fnd = False
        t_append = cmnts.append  # make local
        ex_cmnts = 0  # track how many existing comments encountered
        for cmnt in media_cmnts:  # comments in order from oldest to newest
            if min_cmnt_id:
                if cmnt.id <= min_cmnt_id:  # check if this is a new comment (string comparison)
                    ex_cmnts += 1  # existing comment
                    continue  # do not append, already have this comment
                elif cmnt.id == min_cmnt_id:
                    bkmrk_fnd = True
            t_append(parse.inst_cmnt(cmnt, media_id))  # media_id signifies parent object

        log.debug("Initial (and only) call to Instagram returned %i comments, %i of which are new and being stored." % (ex_cmnts + len(cmnts), len(cmnts)))
        
        # okay, so would appear no pagination is supported for comments;
        # from what i've read online, it could be that only (the last?) 150
        # comments are returned, whether you query for a piece of media and pull
        # them out of the 'comments' attribute or query the comments endpoint itself;
        # for other endpoints, pagination logic goes here...
        
        if len(cmnts) > 0:
            min_cmnt_id = cmnts[len(cmnts) - 1]['id']  # new bookmark
        
        return cmnts, bkmrk_fnd, min_cmnt_id  # three vars returned for child adapter work

    def get_media_likes(self, media_id, bkmrk_val=None):
        # Get likes posted to a specific media, helper function invoked by upper function which
        # performs post WS and data store work.
        # Expects:      media_id
        #               bkmrk_val (string) - concatenated ('-' delim) consecutive user IDs; if None, will get_all (max. 150 via API)
        # Returns:      likes (list) - qualifying likes
        #               bkmrk_fnd (bool)
        #               new_bkmrk (str)
        # returns discrete list of like objects, makes no changes to Ext object
        
        log = logging.getLogger(module_nm + '.InstUsr.get_media_likes')
        
        log.debug("Retrieving likes for Instagram media ID %s." % media_id)

        self.params['media_id'] = media_id
        media_likes = self.do_ws_get(self.api.media_likes)  # needs to be unicode...?

        likes = []
        t_append = likes.append  # make local
        init_len = len(self.rcds)
        bkmrk_fnd = False
        # getting creative with bookmarks: using IDs from two consecutive records as a
        # bookmark (fend against like/unlike/like scenario)....??
        prev_id = ''
        for like in (media_likes):
            if bkmrk_val:
                if '-' in bkmrk_val:  # multi-part
                    if prev_id == bkmrk_val.split('-')[1]:  # second part found....
                        if like.id == bkmrk_val.split('-')[0]:  # multi-part first part
                            log.debug("Multi-part bookmark found. That means we're done.")
                            bkmrk_fnd = True
                            self.rcds.pop()  # the previously appended like is redundant
                            break
                    prev_id = like.id  # for next round through loop
                else:  # single-part
                    if like.id == bkmrk_val:
                        log.debug("Single-part bookmark mode, bookmark found. That means we're done.")
                        bkmrk_fnd = True
                        break
            t_append(parse.inst_like(like, media_id))  # media_id signifies parent object

        log.debug("Initial (and only) call to Instagram returned %i likes." % (len(likes) - init_len))
        log.debug("First ten likes are (curious if always in same order): %s." % str(likes[:10]))
        
        # okay, so would appear no pagination is supported for likes;
        # from what i've read online, it could be that only (the last?) 150
        # likes are returned; for other endpoints, pagination logic goes here...
        
        if len(likes) > 1:
            new_bkmrk = likes[0]['id'] + '-' + likes[1]['id']
        elif len(likes) == 1:
            if bkmrk_val:  # if pre-existing bookmark
                new_bkmrk = likes[0]['id'] + '-' + bkmrk_val.split('-')[0]
            else:
                new_bkmrk = likes[0]['id']
        else:
            new_bkmrk = None
        
        return likes, bkmrk_fnd, new_bkmrk  # likes associated with this media
        
    def get_usr_media(self, usr_id='self', get_all=False):
        # Get media posted for a user, defaults to authenticated user
        # get_all gets all results (default: False), otherwise
        # only new media are retrieved
        # the first run of this function could be long for a given user,
        # especially given time.sleep(5)
        
        log = logging.getLogger(module_nm + '.InstUsr.get_usr_media')
        
        log.debug("Retrieving Instagram media for user ID %s." % usr_id)

        self.params['pagination_format'] = 'dict'
        self.params['user_id'] = usr_id
        
        if not get_all:  # continue where last left off
            self.params['min_id'] = self.get_bkmrk()  # media ID as bookmark
        
        medias = []  # each media will be dict in this list
        usr_media, next = self.do_ws_get(self.api.user_recent_media)  # needs to be unicode...?

        t_append = self.rcds.append  # make local
        log.debug("Pagination dictionary returned: %s" % str(next))
        first_media = True
        for media in usr_media:
            # below, the first media returned will BE our bookmarked media, which we already have,
            # so we want to ignore it; may be entirely possible to increment the trailing part of
            # the media ID by 1 (str-->int-->str) but for now doing this... 
            if first_media:
                first_media = False
                continue  # next (second) media
            t_append(parse.inst_media(media))
            log.debug(media.id)
        try:
            max_media_id = self.rcds[0]['id']  # first one *should* be newest
        except:
            max_media_id = None  # nothing was returned...?
        log.debug("Initial call to Instagram returned %i media." % len(self.rcds))

        while next:  # parses backwards in time, toward older media
            num_rcds = len(self.rcds)
            try:  # looks like next is returned during last iteration, just without most values
                # looks like could also use self.params['with_next_url'] = next['next-url']
                self.params['max_id'] = next['next_max_id']
            except KeyError:  # indicates we're done
                break

            usr_media, next = self.do_ws_get(self.api.user_recent_media)
            
            log.debug("Pagination dictionary returned: %s" % str(next))
            for media in usr_media:
                t_append(parse.inst_media(media))
                log.debug(media.id)
            log.debug("Subsequent page of results returned an additional %i media." % (len(self.rcds) - num_rcds))
            time.sleep(api_pg_delay)  # hopefully conservative wrt API limit...

        log.debug("A total of %i media returned." % len(self.rcds))

        # below function need to be changed to not accept Ext object but instead one record at a time (or does it?)
        self = enrich.inst_media(self)  # extract additional info for each record
        
        self.post_get(rfrsh=get_all)
        
        # below, tag ID is the part of the media ID prior to the underscore;
        # max_tag_id + 1 becomes min tag ID for next query...
        if max_media_id:  # only exists if results returned
            self.put_bkmrk(max_media_id)  # does this need to be incremented? may just overwrite...
            
        log.debug("Instagram user media work completed.")
                
        return self

    def get_usr_feed(self, get_all=False):
        # Get media for the user who is currently authenticated
        # get_all returns all results (default: False), otherwise
        # only new media are retrieved
        # the first run of this function could be long for a given user,
        # especially given time.sleep(5)
        
        log = logging.getLogger(module_nm + '.InstUsr.get_usr_feed')
        
        log.debug("Retrieving Instagram feed for authenticated user %s." % self.cfg['auth'])

        self.params['pagination_format'] = 'dict'
        
        if not get_all:  # continue where last left off
            self.params['min_id'] = self.get_bkmrk()  # media ID acts as bookmark

        try:
            feed_media, next = self.do_ws_get(self.api.user_media_feed)  # needs to be unicode...?
            log.debug("Pagination dictionary returned: %s" % str(next))
        except ValueError as e:  # common WS func returns empty list
            log.warn("Problem with Instagram query. Nothing to be done this execution. Error message: %s" % e)
            log.warn("Default empty list may have been returned from common WS function.")
            feed_media = []  # facilitate continuation
            next = None

        t_append = self.rcds.append  # make local
        log.debug("Pagination dictionary returned: %s" % str(next))
        for media in feed_media:
            t_append(parse.inst_media(media))
            log.debug(media.id)
        try:
            max_media_id = self.rcds[0]['id']  # first one *should* be newest
        except:
            max_media_id = None  # nothing was returned...?
        log.debug("Initial call to Instagram returned %i media." % len(self.rcds))

        while next:  # parses backwards in time, toward older media
            num_rcds = len(self.rcds)
            try:  # looks like next is returned during last iteration, just without most values
                # looks like could also use self.params['with_next_url'] = next['next-url']
                self.params['max_id'] = next['next_max_id']
            except KeyError:  # indicates we're done
                break

            feed_media, next = self.do_ws_get(self.api.user_media_feed)
            
            log.debug("Pagination dictionary returned: %s" % str(next))
            for media in feed_media:
                t_append(parse.inst_media(media))
                log.debug(media.id)
            log.debug("Subsequent page of results returned an additional %i media." % (len(self.rcds) - num_rcds))
            time.sleep(api_pg_delay)  # hopefully conservative wrt API limit...

        log.debug("A total of %i media returned." % len(self.rcds))

        # below function need to be changed to not accept Ext object but instead one record at a time
        self = enrich.inst_media(self)  # extract additional info for each record
        
        self.post_get(rfrsh=get_all)
        
        # below, tag ID is the part of the media ID prior to the underscore;
        # max_tag_id + 1 becomes min tag ID for next query...
        if max_media_id:  # only exists if results returned
            self.put_bkmrk(max_media_id)  # does this need to be incremented? may just overwrite...
            
        log.debug("Instagram user feed work completed.")
                
        return self
    
    def qry_tag_media(self, tag_nm, get_all=False):
        # get recent media for a tag
        # get_all gets all results (default: False), otherwise
        # only new media are retrieved
        # the first run of this function could be long for a given tag,
        # especially given time.sleep(5)
        
        log = logging.getLogger(module_nm + '.InstUsr.qry_tag_media')
        
        log.debug("Querying Instagram for media IDs returned for tag %s." % tag_nm)

        self.params['tag_name'] = tag_nm.decode('utf-8')
        self.params['pagination_format'] = 'dict'
        
        if not get_all:  # continue where last left off
            self.params['min_tag_id'] = self.get_bkmrk()  # tag ID acts as bookmark

        try:
            tag_recent_media, next = self.do_ws_get(self.api.tag_recent_media)  # needs to be unicode...?
            log.debug("Pagination dictionary returned: %s" % str(next))
        except ValueError as e:  # common WS func returns empty list
            log.warn("Problem with Instagram query. Nothing to be done this execution. Error message: %s" % e)
            log.warn("Default empty list may have been returned from common WS function.")
            tag_recent_media = []  # facilitate continuation
            next = None

        t_append = self.rcds.append  # make local
        
        for media in tag_recent_media:
            t_append(parse.inst_media(media))
            log.debug(media.id)
        try:
            # kind of wonky how Instagram uses these pagination params,
            # see CORE-79 for details
            min_tag_id = next['min_tag_id']  # will serve as bookmark
        except:
            min_tag_id = None  # nothing was returned...?
        log.debug("Initial call to Instagram returned %i media." % len(self.rcds))
        while next:  # parses backwards in time, toward older media
            num_rcds = len(self.rcds)
            try:  # looks like next is returned during last iteration, just without most values
                self.params['min_tag_id'] = next['min_tag_id']
                min_tag_id = next['min_tag_id']  # update new bookmark
            except KeyError:  # indicates we're done (no min_tag_id in pagination dict)
                break

            # will continue to retrieve/overwite same < 20 recent media until pagination dict
            # is returned with new min_tag_id (pagination dict comes when 20+ media are found
            # since bookmark)
            tag_recent_media, next = self.do_ws_get(self.api.tag_recent_media)
            
            log.debug("Pagination dictionary returned: %s" % str(next))
            for media in tag_recent_media:
                t_append(parse.inst_media(media))
                log.debug(media.id)
            log.debug("Subsequent page of results returned an additional %i media." % (len(self.rcds) - num_rcds))
            time.sleep(api_pg_delay)  # hopefully conservative wrt API limit...

        log.debug("A total of %i media returned." % len(self.rcds))

        # below function need to be changed to not accept Ext object but instead one record at a time
        self = enrich.inst_media(self)  # extract additional info for each record
        
        self.post_get(rfrsh=get_all)

        # bookmark
        if min_tag_id:  # only exists if results returned
            self.put_bkmrk(min_tag_id)  # no need to increment, per the API!
            
        log.debug("Instagram tag media adapter work completed.")
                
        return self
            
#        reactor = subscriptions.SubscriptionsReactor()
#        reactor.register_callback(subscriptions.SubscriptionType.TAG, process_tag_update)

#        api = client.InstagramAPI(access_token=access_token, client_secret=self.client_secret)

    def get_usr_info(self, usr_id='self'):
        # get user info for [authenticated, for now?] user
                
        log = logging.getLogger(module_nm + '.InstUsr.get_usr_info')
        
        log.debug("Querying Instagram for info for user %s." % usr_id)
        
        self.params['user_id'] = usr_id.decode('utf-8')
        
        usr_info = self.do_ws_get(self.api.user)

        self.rcds.append(parse.inst_usr(usr_info))  # a single object returned
        # below function need to be changed to not accept Ext object but instead one record at a time
        self = enrich.inst_usr(self)  # extract additional info for each record
        
        try:  # may not have returned anything
            log.debug("Information retrieved for user: %s" % str(self.rcds[0]))
        except:
            pass

        self.post_get()
            
        log.debug("Instagram user info adapter work completed.")
                
        return self

    def get_usr_fllwd_by(self, usr_id, get_all=False):
        # get followers for given user ID
        
        log = logging.getLogger(module_nm + '.InstUsr.get_usr_fllwd_by')
        
        log.debug("Querying Instagram for followers for user ID %s." % usr_id)
        
        self.params['user_id'] = usr_id.decode('utf-8')

        last_usr = self.get_bkmrk()  # most recent user to follow us
        if not last_usr:  # could be None
            last_usr = ''  # nothing will match, all will be retrieved/appended

        usrs = []  # each media will be dict in this list
        usrs_info, next = self.do_ws_get(self.api.user_followed_by)  # needs to be unicode...?

        bkmrk_fnd = False  # track, may impact whether this is append/update
        t_append = self.rcds.append  # make local
        log.debug("Pagination dictionary returned: %s" % str(next))  # None if nothing else to get
        # order of users is newest to oldest...
        for usr in usrs_info:
            if not get_all:  # only if full refresh not called for
                if usr.id == last_usr:  # found our last user to follow us
                    log.debug("Bookmarked user ID of %s encountered, no more users being retrieved." % last_usr)
                    next = None  # we're done
                    bkmrk_fnd = True
                    break
            t_append(parse.inst_usr(usr))  # otherwise, keep appending
        log.debug("Initial call to Instagram returned %i users." % len(self.rcds))

        while next:
            num_rcds = len(self.rcds)
            self.params['with_next_url'] = next
            usrs_info, next = self.do_ws_get(self.api.user_followed_by)
            # should there be a check here to stop retrieving once existing user is
            # detected? difficult without timestamps or incrementing ID in the source
            # data...
            log.debug("Pagination dictionary returned: %s" % str(next))
            for usr in usrs_info:
                if not get_all:
                    if usr.id == last_usr:
                        log.debug("Bookmarked user ID of %s encountered, no more users being retrieved." % last_usr)
                        next = None
                        bkmrk_fnd = True
                        break
                t_append(parse.inst_usr(usr))
            log.debug("Subsequent page of results returned an additional %i users." % (len(self.rcds) - num_rcds))
            time.sleep(api_pg_delay)

        log.debug("A total of %i users returned." % len(self.rcds))

        # below function needs to be changed, maybe, to not accept Ext object but just the records (am having second thoughts about this)
        self = enrich.inst_usr(self)  # extract additional info for each record

        rfrsh = False
        if not get_all:
            if not bkmrk_fnd and last_usr != '':  # had a bookmark but it wasn't found
                log.info("Bookmarked value of %s was not found, forcing this run into full rfrsh (rfrsh=True) mode." % last_usr)
                rfrsh = True
        else:
            rfrsh = True

        self.post_get(rfrsh=rfrsh)
        
        if len(self.rcds) > 0:
            self.put_bkmrk(self.rcds[0]['id'])  # our new newest user
            
        log.debug("Instagram user followed by work completed.")
                
        return self


    # Below not considered pure "adapters," at this time, as they do not rely and
    # utilize the NoSql data store. For now, considered helper functions until
    # role is articulated, but use InstUsr object for interacting with Instagram
    def get_tag_info(self, tag_nm):
        
        log = logging.getLogger(module_nm + '.InstUsr.get_tag_info')
        
        log.debug("Retrieving tag info for tag %s." % tag_nm)
        
        self.params['tag_name'] = tag_nm.decode('utf-8')
        
        tag_info = self.do_ws_get(self.api.tag)  # needs to be unicode...?
        
        return tag_info  # dict which indludes tag_info['data']['media_count']

class Twtr(Ext):
    def __init__(self, ext_id, ext_cfg):
        Ext.__init__(self, ext_id, ext_cfg)
        self.unauth_cfg = {}
        # below, hard-coded for dev purposes....will be same for entire app, though (?)
        self.unauth_cfg['cust_key'] = util.cfg_app['auth']['twtr']['app']['cust_key']
        self.unauth_cfg['cust_secret'] = util.cfg_app['auth']['twtr']['app']['cust_secret']
        self.unauth_cfg['redirect_uri'] = 'http://127.0.0.1:' + util.cfg_app['env']['ui_port'] + '/auth/twtr_callback'  # hard-coded hostname
        self.unauth_api = Twython(self.unauth_cfg['cust_key'], self.unauth_cfg['cust_secret'])
        self.params = {}  # parameters for submission with API call
        self.response = {}  # hold response data

    def req_access_token(self):

        try:
            auth = self.unauth_api.get_authentication_tokens(callback_url=self.unauth_cfg['redirect_uri'])  # hard-coded hostname
            return auth['oauth_token'], auth['oauth_token_secret'], auth['auth_url']
        except Exception as e:
            print(e)
            
    def get_auth_tokens(self, mid_oauth_token, mid_oauth_token_secret, oauth_verifier):
        # expects oauth token/secret/verifier returned in URL of previous step(s)
        
        log = logging.getLogger(module_nm + '.Twtr.get_auth_tokens')
            
        try:
            mid_auth_api = Twython( self.unauth_cfg['cust_key'], self.unauth_cfg['cust_secret'], mid_oauth_token, mid_oauth_token_secret )
            final_auth = mid_auth_api.get_authorized_tokens( oauth_verifier )
            if not final_auth['oauth_token']:
                log.warning("Unable to retrieve oauth token.")
                return 'Could not get oauth token.'
            if not final_auth['oauth_token_secret']:
                log.warning("Unable to retrieve oauth token secret.")
                return 'Could not get oauth token secret.'
        except Exception as e:
            print(e)
        
        # update configuration with user-level tokens
        cfg_auth = util.get_cfg_auth()
        
        # below will overwrite any existing
        try:
            cfg_auth['auth']['twtr']['user'][self.cfg['auth']]['oauth_token'] = final_auth['oauth_token']
            cfg_auth['auth']['twtr']['user'][self.cfg['auth']]['oauth_token_secret'] = final_auth['oauth_token_secret']
        except KeyError:  # may be a new user-level auth entry
            log.info("Adding new user-level authentication entry for user %s." % self.cfg['auth'])
            cfg_auth['auth']['twtr']['user'][self.cfg['auth']] = {}  # new dict
            cfg_auth['auth']['twtr']['user'][self.cfg['auth']]['oauth_token'] = final_auth['oauth_token']
            cfg_auth['auth']['twtr']['user'][self.cfg['auth']]['oauth_token_secret'] = final_auth['oauth_token_secret']                     
        util.updt_cfg_aspect(cfg_auth)  # update config
        
        log.info("Configuration updated with new oauth token and secret.")
        
        return final_auth['oauth_token'], final_auth['oauth_token_secret']
        
    def verify_auth():
        # looks like this can be called from app or user level API
        
        self.api.verify_credentials()  # need to probably include somewhere as a check (in case of failure?)
        
#    def get_rate_lmt():
#        lmt_info = self.api.get_application_rate_limit_status( **{})  # hard-coded for now
#        self.metrics['twtr_srch_lmt'] = lmt_info['search']['/search/tweets']['remaining']
#        self.metrics['twtr_app_lmt'] = lmt_info['application']['/application/rate_limit_status']['remaining']
    
class TwtrSrch(Twtr):
    
    def __init__(self, ext_id, ext_cfg):
        Twtr.__init__(self, ext_id, ext_cfg)
        # setup defaults
        # API: https://dev.twitter.com/docs/api/1.1/get/search/tweets
        self.params['count'] = 100  # max supported
        self.params['since_id'] = None
        self.params['max_id'] = None
        self.params['lang'] = 'en'  # by default, english only
        self.params['include_entities'] = True  # return entities node along with each status?
        self.params['until'] = None
        self.params['geocode'] = None
        self.params['locale'] = None
        self.params['callback'] = None
        
        # query operators: https://dev.twitter.com/rest/public/search
        # Example URL-encoded query for love OR #love OR @love: love%20OR%20%23love%20OR%20%40love
        self.params['q'] = ''  # our query string
        
        self.logger = logging.getLogger(module_nm + '.' + 'TwtrSrch')
        
        # for purposes of authentication, assume (hard-coded) that any one client,
        # that has one set of configuration, will have only one set of application-level
        # tokens for use with twitter; in fact, all of HAL may have a single application-level
        # set of credentials
        self.api = Twython(util.cfg_app['auth']['twtr']['app']['cust_key'], util.cfg_app['auth']['twtr']['app']['cust_secret'], util.cfg_app['auth']['twtr']['app']['access_token'], util.cfg_app['auth']['twtr']['app']['access_token_secret'])  # query object
        
    def srch_twtr(self):
        
        log = logging.getLogger(module_nm + '.TwtrSrch.srch_twtr')
        
        self.params['since_id'] = self.get_bkmrk()  # Twitter ID (since_id) acts as bookmark
        self.params['q'] = bld_qry_str(self.cfg['q'])  # bit of wonkiness here, need to decide how to manage 'q' w/ respect to optional params

        response = self.do_ws_get(self.api.search)
        
#        log.debug("Response from Twitter search is: %s" % str(response))  # leads to long entries in log

        # A quirk I observed only when the application started again after bringing
        # my computer out of sleep had an empty response from Twitter make it this far,
        # which causes key errors. Downstream logic *should* handle an empty dictionary,
        # so let's allow it to get passed the block below.
        if 'search_metadata' in response:  # in case empty Twitter response
            self.payload['meta'] = response['search_metadata']  # actually not even sure it's appropriate to store search_metadata in 'meta'
        else:
            self.payload['meta'] = {}
        if 'statuses' in response:  # in case empty Twitter response
            self.payload['data'] = response['statuses']
        else:
            self.payload['data'] = {}
        
        # below function need to be changed to not accept Ext object but instead one record at a time
        self = parse.twtr_statuses(self)
        
        # below function need to be changed to not accept Ext object but instead one record at a time
        self = enrich.enrich_twtr_statuses(self)  # extract additional info for each record
        self = enrich.enrich_twtr_statuses_srch(self)  # specific to tweets coming from search
        
        self.post_get()
        
        self.put_bkmrk(response['search_metadata']['max_id'])  # the last thing we do
            
        log.debug("TwtrSrch adapter work completed.")
                
        return self
    
class TwtrUser(Twtr):
    # containing functions at the user level
    
    def __init__(self, ext_id, ext_cfg):
        Twtr.__init__(self, ext_id, ext_cfg)
        self.logger = logging.getLogger(module_nm + '.' + 'TwtrUser')
        
        # for purposes of authentication, assume (hard-coded) that any one client,
        # that has one set of configuration, will have only one set of application-level
        # tokens for use with twitter; in fact, all of HAL may have a single application-level
        # set of credentials
        self.api = Twython(util.cfg_app['auth']['twtr']['app']['cust_key'], util.cfg_app['auth']['twtr']['app']['cust_secret'], util.cfg_app['auth']['twtr']['user'][self.cfg['auth']]['oauth_token'], util.cfg_app['auth']['twtr']['user'][self.cfg['auth']]['oauth_token_secret'])  # query object
        
        # setup defaults
        self.params['count'] = 200  # max supported(?)
        self.params['since_id'] = None
        self.params['max_id'] = None
        self.params['trim_user'] = False  # receive only user ID associated with status? (or else complete user object)
        self.params['contributer_details'] = True  # in contributors element for each status, include screen name in addition to user ID?
        self.params['include_entities'] = True  # return entities node along with each status?
        
    def get_home_timeline(self):
        
        log = logging.getLogger(module_nm + '.TwtrUser.get_home_timeline')
        
        # for these user-level functions, there are a couple parameters that
        # vary between them; not sure if each should have its own class for
        # that reason...but for now...
        if 'exclude_replies' not in self.params:
            self.params['exclude_replies'] = False  # do not include replies in timeline returned?
        self.params['since_id'] = self.get_bkmrk()  # Twitter ID (since_id) acts as bookmark
    
        response = self.do_ws_get(self.api.get_home_timeline)

        # A quirk I observed only when the application started again after bringing
        # my computer out of sleep had an empty response from Twitter make it this far,
        # which causes key errors. Downstream logic *should* handle an empty dictionary,
        # so let's allow it to get passed the block below.
        self.payload['data'] = response
        
        self = parse.twtr_statuses(self)

        # below function need to be changed to not accept Ext object but instead one record at a time
        self = enrich.enrich_twtr_statuses(self)  # extract additional info for each record
        
        self.post_get()

        ids = [ self.rcds[i]['id'] for i in range(len(self.rcds)) ]  # build list
        try:  # will error if no statuses were returned
            self.put_bkmrk(max(ids))  # the last thing we do
        except:
            pass
            
        log.debug("Adapter work completed.")
        
    def get_mentions_timeline(self):
        
        log = logging.getLogger(module_nm + '.TwtrUser.get_mentions_timeline')        
        
        self.params['since_id'] = self.get_bkmrk()  # Twitter ID (since_id) acts as bookmark
    
        response = self.do_ws_get(self.api.get_home_timeline)

        # A quirk I observed only when the application started again after bringing
        # my computer out of sleep had an empty response from Twitter make it this far,
        # which causes key errors. Downstream logic *should* handle an empty dictionary,
        # so let's allow it to get passed the block below.
        self.payload['data'] = response
        
        self = parse.twtr_statuses(self)

        # below function need to be changed to not accept Ext object but instead one record at a time
        self = enrich.enrich_twtr_statuses(self)  # extract additional info for each record
        
        self.post_get()

        ids = [ self.rcds[i]['id'] for i in range(len(self.rcds)) ]  # build list
        try:  # will error if no statuses were returned
            self.put_bkmrk(max(ids))  # the last thing we do
        except:
            pass
            
        log.debug("Adapter work completed.")
        
#    def updt_status(self, status):
        # pull out into own class? supported args:
        # status, in_reply_to_status_id, possibly_sensitive, lat, long, place_id, display_coordinates, trim_user, media_ids

#        self.do_ws_put(self.api.update_status(status))

def bld_qry_str(q_dict):
    # Expects configuration dictionary for text/hashtag/mention, builds query string
    # from it
    
    log = logging.getLogger(module_nm + '.' + 'bld_qry_str')

    q_str = ''
    for key in q_dict.keys():
        if key == 'text':
            for k, v in q_dict[key].iteritems():  # where k is conjunction string
                for param in v:  # list of param strings
                    q_str += k + ' ' + '"' + param + '"' + ' '  # double-quote each
        elif key == 'hashtag':
            for k, v in q_dict[key].iteritems():  # where k is conjunction string
                for param in v:  # list of param strings
                    q_str += k + ' ' + '#' + param + ' '
        elif key == 'mention':
            for k, v in q_dict[key].iteritems():  # where k is conjunction string
                for param in v:  # list of param strings
                    q_str += k + ' ' + '@' + param + ' '

    # clean it up
    q_str = q_str.split(' ', 1)[1]  # there's a leading conjunction we don't want
    q_str.strip()  # remove whitespace
    
    log.debug("Query string generated from configuration: %s" % q_str)
    
    return util.enc_url_str(q_str)  # URL-safe

def do_fb_get(**kwargs):
    # base function for handling calls to FB
    
    log = logging.getLogger(module_nm + '.do_fb_get')

    params = kwargs
    
    if 'api_url' not in params:
        log.warning("No URL submitted, nothing to do...returning empty string.")
        return ''
    # get common params in order
    # fields that should be retrieved
    if 'fields' in params:  # were specific fields requested?
        if isinstance(params['fields'], list):  # only if list
            params['fields'] = ','.join(params['fields'])
        log.debug("Retrieving the following fields for each entity: %s." % params['fields'])

    # IDs of objects to retrieve, if present
    if 'ids' in params:
        log.debug("Retrieving information for %i entities." % len(params['ids']))
        params['ids'] = ','.join(params['ids'])

    api_url = params.pop('api_url')

    log.debug("Submitting request to %s." % api_url)
    if 'post' in params:  # really, any value can be attributed to 'post' (True?)...not currently used(??)
        params.pop('post')
        rsp = requests.post(url=api_url, params=params).json()
    else:
        rsp = requests.get(url=api_url, params=params).json()
    
    log.debug("TRACE: response received: %s" % str(rsp))
    
    return rsp
    
    
###
### ABOVE THIS LINE HAS BEEN VETTED ###
###

class AdhocFile():
    
    def __init__(self):
        self.filename = ''  # just file name (no path)
        self.filetype = ''  # 'config', 'schema', or 'data', currently
        self.path = ''  # path to sub-folder (within which resides file)
        self.sub = ''  # name of sub-folder (associated with data type, e.g. 'csv')
        self.datatype = ''  # csv, txt, mp3, etc.
        self.logger = logging.getLogger(module_nm + '.' + 'File')
        self.lines = []  # list of lines of content
        self.eol = False  # flag for whether EOL chars are prsent in 'lines'
        self.srcId = -1  # source ID associated with file/path/sub
        self.strmId = -1  # stream ID associated with file (support just one for now)
        self.parser = ''  # to be used after switch over to db-driven config (e.g., 'csv', etc.)
        
    def get_config_file(self, file='', delim=''):  # Read a configuration file
        
        log = self.logger
        
        if file == '':
            file = os.path.join(self.path, self.sub, self.filename)
            
        log.info("Reading configuration file %s." % file)
        
        self.filetype = 'config'
        
        # need a conditional to accommodate overriding of fSchema/self attributes based on path passed to this function
        
        self.f = open(file, 'r')  # Config files opened as read-only text
        
        return self
    
    def get_schema_file(self, file='', delim=','):  # Read a data schema file (describes a data file)
        
        log = self.logger
        
        file = os.path.join(self.path, self.sub, self.filename)
        log.info("Reading schema file %s." % file)
        
        # eventually may want/need a conditional here to accommodate overriding of fSchema/self attributes based on path passed to this function
        # if file != '':
#            self.filename =
#            self.path =
#            self.sub =
#            self.datatype =  # would be based on sub dir(?)
        
        self.f = open(file, 'r')  # Schema files opened as read-only text
        i = 0
        schemaList = []  # will store our schema values
        for line in self.f:
            log.debug("Processing schema line %s." % str(line))
            line = line.strip()
            if line == '':  # no valid config
                continue
            elif line[0] == '#':  # no valid config
                continue
            values = line.split(delim)  # hard-coded delimiter to comma, make this configurable? why?
            log.debug("Length of list for this schema entry is %s" % str(len(values)))
            valueList = []  # store elements for this schema entry
            for value in values:
                valueList.append(value.strip())
            log.debug("Appending following entry to schema: %s" % str(valueList))
            schemaList.append(valueList)
        self.f.close()
            
        self.schema = []
        self.schema = schemaList
        
        return self
        
    def get_data_file_lines(self, name):  # Read a data file
        
        log = self.logger
        
        log.debug("Reading what may be a data file %s." % str(name))
        
        self.filename = name
        self.filetype = 'data'
        self.datatype = ''  # don't know yet

        with open(name) as fp:
            for line in fp:
                return line
            
    def write_output_file(self, file=''):
    
        log = self.logger
        
        if file == '':
            file = os.path.join(self.path, self.sub, self.filename)
        
        log.info("Going to output file %s." % file)
        
        if self.eol == True:
            pass  # stubbed out for future logic...?
        
        with codecs.open(file, 'w', 'utf-8-sig') as self.f:
            for i in xrange(len(self.lines)):
                self.f.write(self.lines[i] + '\n')  # python will convert \n to os.linesep
        
        self.f.close()
        
        return self
        
        
    def write_sample_data(self, fSchema, file='', delim=','):
        
                # eventually may want/need a conditional here to accommodate overriding of fSchema/self attributes based on path passed to this function
        # if file != '':
#            self.filename =
#            self.path =
#            self.sub =
#            self.datatype =  # would be based on sub dir(?)
        
        log = self.logger
        
        self.filename = fSchema.filename
        self.path = fSchema.path
        self.sub = fSchema.sub
        
        if file == '':
            file = os.path.join(self.path, self.sub, self.filename)
            
        log.info("Going to generate sample data based on schema file %s." % file)
        
        ts = util.get_fs_safe_ts(time.time())
        fileTen = self.filename.split('.')[0] + '-' + ts + '-10'
        fileThous = self.filename.split('.')[0] + '-' + ts + '-1k'
        fileTenThous = self.filename.split('.')[0] + '-' + ts + '-10k'
        
        headerRow = ''
        for i in range(len(fSchema.schema)):
            headerRow += fSchema.schema[i][0] + delim  # grabbing field label
        headerRow = headerRow[:-1]  # remove extra delim
        
        writePathTen = os.path.join(self.path, self.sub, fileTen)
        writePathThous = os.path.join(self.path, self.sub, fileThous)
        writePathTenThous = os.path.join(self.path, self.sub, fileTenThous)
        
        self.fTen = open(writePathTen, 'w')
        self.fThous = open(writePathThous, 'w')
        self.fTenThous = open(writePathTenThous, 'w')
        
        self.fTen.write(headerRow + '\n')  # python will convert \n to os.linesep
        self.fThous.write(headerRow + '\n')
        self.fTenThous.write(headerRow + '\n')

        i = 0
        while i < 10000:
            newLine=''
            for schema in fSchema.schema:
                if schema[1] == 'int':  # hard-coded list address
                    try:  # in case range was specified
                        min = int(schema[5][0])
                        max = int(schema[5][1])
                        newLine += (str(util.get_rand_int(min, max)) + delim)
                    except:
                        newLine += (str(util.get_rand_int()) + delim)  # default range
                elif schema[1] == 'long':  # hard-coded list address
                    try:  # in case range was specified
                        min = int(schema[5][0])
                        max = int(schema[5][1])
                        newLine += (str(util.get_rand_long(min, max)) + delim)
                    except:
                        newLine += (str(util.get_rand_long()) + delim)  # default range
                elif schema[1] == 'float':  # hard-coded list address
                    newLine += (str(util.get_rand_float()) + delim)
                else:  # no support now for lists, dicts, etc.
                    try:
                        strRange = schema[5]  # hard-coded for now
                    except:
                        strRange = []
                    newLine += (util.get_rand_string(strRange=strRange) + delim)
            newLine = newLine[:-1]  # get rid of trailing delim
            self.fTenThous.write(newLine + '\n')  # python will convert \n to os.linesep
            if i < 1000:
                self.fThous.write(newLine + '\n')
                if i < 100:
                    self.fTen.write(newLine + '\n')
            i += 1
                    
        self.fTen.close()
        self.fThous.close()
        self.fTenThous.close()
        
        return self
    
class Email(Ext):
    
    def __init__(self):
        Ext.__init__(self)
        self.sender = ''
        self.destination = ''
        self.subject = ''
        self.body = ''
        self.server = ''
        self.logger = logging.getLogger(module_nm + '.' + 'Email')
        
class EmailWriter(Email):

    def __init__(self):
        Email.__init__(self)
        # self.f = open()
        self.username = ''
        self.password = ''
        self.logger = logging.getLogger(module_nm + '.' + 'Email' +'.' + 'EmailWriter')
        
    def send_txt_ssl_mail(self, configParams):
        
        log = self.logger
        
        text_subtype = 'plain'  # typical values for text_subtype are plain, html, xml
        
        self.server = configParams.configDict['emailSendServer']
        self.username = configParams.configDict['emailSendUsername']
        self.password = configParams.configDict['emailSendPassword']
        
        if self.subject is '' or self.content is '':
            log.warning("Subject and/or content of email is blank, not sending.")
            return 0
        
        if self.sender is '' or self.destination is '':
            log.warning("Sender or destination of email is blank, not sending.")
            return 0

        try:
            msg = MIMEText(self.content, text_subtype)
            msg['Subject'] = self.subject
            msg['From'] = self.sender # some SMTP servers will do this automatically, not all

            conn = SMTP_SSL(self.server)
            conn.set_debuglevel(False)
            conn.login(self.username, self.password)
            try:
                conn.sendmail(self.sender, self.destination, msg.as_string())
                log.info("Email notification of subject '%s' sent through server %s." % (self.subject, self.server))
            finally:
                conn.close()

        except Exception, exc:
            log.error("Email of subject '%s' failed to send through server %s. Check configuration, credentials, and availability of server. Considering this a fatal error for now." % (self.subject, self.server))
            sys.exit( "Mail failed; %s" % str(exc) ) # give a error message

    
class RotatingFile(Ext):
    
    def __init__(self, path='', sub='', filename='', maxFiles=sys.maxint, maxFileSize=500000):
        Ext.__init__(self)
        self.init = 1  # initial value of incrementer
        self.ii = 1
        self.filename = filename  # just file name (no path)
        self.path = path  # path to sub-folder (within which resides file)
        self.sub = sub  # name of sub-folder, if applicable
        self.maxFiles = maxFiles
        self.maxFileSize = maxFileSize
        self.finished = False
        self.fh = None
        self.logger = logging.getLogger(module_nm + '.' + 'RotatingFile')
        
    def get_filename_rot(self, ii=-1):
        if ii == -1:  # not specified
            return self.filename_rot
        else:
            return os.path.join(self.path, self.sub, self.filename + "_%0.2d" % ii)
        
    def rotate(self):  # rotate file, if necessary
        if (os.stat(RotatingFile.get_filename_rot(self)).st_size > self.maxFileSize):
            self.close()
            if self.ii + 1 <= self.maxFiles:
                self.ii += 1
                self.open()
#            self.ii += 1
#            if (self.ii <= self.max_files):
#                self.open()
            else:
                try:
                    os.remove(RotatingFile.get_filename_rot(self, self.init))
                except:
                    log.warning("File %s not found, so can not delete as part of file rotation. Was it manually deleted? Is write-access allowed? Continuing..." % RotatingFile.get_filename_rot(self, self.init))
                    pass
                for i in range(self.init + 1, self.ii):  # start at file immediately following initial (which should no longer exist), rename each file
                    try:
                        shutil.move(RotatingFile.get_filename_rot(self, i), RotatingFile.get_filename_rot(self, i-1))
                    except:
                        log.warning("Problem when trying to move file %s to %s. Was it manually deleted? Is write-access allowed? Continuing..." % (RotatingFile.get_filename_rot(self, i), RotatingFile.get_filename_rot(self, i-1)))
                        pass
                self.open()
#                self.close()  # This went along with the line immediately below...
#                self.finished = True  # not using this flag, at the moment...but the idea of having files not written at a certain point...possibility for the future...

    def open(self):
        log = self.logger
        self.fh = open(RotatingFile.get_filename_rot(self), 'a')
        log.debug("File %s opened for writing." % RotatingFile.get_filename_rot(self))
        
    def write(self, text=''):
        self.fh.write(text)
        self.fh.flush()
        self.rotate()
        
    def close(self):
        log = self.logger
        self.fh.close()
        log.debug("File %s closed for writing." % RotatingFile.get_filename_rot(self))
                
    @property
    def filename_rot(self):
        return os.path.join(self.path, self.sub, self.filename + "_%0.2d" % self.ii)

class Web(Ext):
    
    def __init__(self):
        Ext.__init__(self)
        self.params = {}
        self.params['url'] = ''  # domain and path (full URL)
        self.params['req_type'] = 'GET'  # 'GET' or 'POST'
        self.payload['status'] = ''
        self.payload['reason'] = ''
        self.payload['data'] = ''
        self.auth = ()
        self.logger = logging.getLogger(module_nm + '.' + 'Web')
        
    def do_http_request(self, auth=()):
        
        log = self.logger
        
        print "Making HTTP request to %s." % self.params['url']
        
        self.payload['data'] = requests.get(self.params['url'], auth=(cfg['api_key'], ''))
        
        print "Getting campaigns for client ID " + cfg['client_id']

        conn = httplib.HTTPConnection(self.domain)
        conn.request(self.reqType, self.reqPath)
        
        resp = conn.getresponse()
        self.payload['status'] = resp.status
        self.payload['reason'] = resp.reason
        log.info("Web request of type %s made to %s, returned status of %s with reason of %s." % (self.cfg['reqType'], self.cfg['domain'], self.payload['status'], self.payload['reason']))
        
        if self.payload['status'] != 200:
            log.warning("Web request of type %s made to %s failed, returning a status code of %s with a reason of %s" % (self.cfg['reqType'], self.cfg['domain'], self.payload['status'], self.payload['reason']))
        
        self.payload['data'] = resp.read()
        
        conn.close()
        
        return self
    
    def do_json_request(self):
        
        log = logging.getLogger(module_nm + '.do_json_request')
        
        print "Retrieving JSON data from %s." % self.params['url']
        r = requests.get(self.params['url'], auth=self.auth)
        
        self.payload['data'] = r.text
        
        return self
        
def move_file(fullCurPath, fullDestPath):
    
    log = logging.getLogger(module_nm + '.move_file')
    
    try:
        shutil.move(fullCurPath, fullDestPath)
        log.debug("Moved file %s to %s." % (fullCurPath, fullDestPath))
    except IOError:
        raise IOError("Problem moving %s to %s.  Does destination directory exist?" % (fullCurPath, fullDestPath))

class dirListing():
    
    # An object to hold/represent a 'snapshot' of a directory structure, the folders and files within.

    def __init__(self):
        self.dirs = {}  # {'':{'':[]}} where {path:{sub:[files]}}
        self.logger = logging.getLogger(module_nm + '.dirListing')
        
    def get_dirs(self, path):
        
        log = self.logger
        
        log.debug("Retrieving directory listing for %s." % str(path))
        
        self.dirs[path] = {}  # {'':[]} where {sub:[files]}
        subs = os.listdir(path)
        for s in subs:
            if s[0] != '.':  # ignore directories with a period as first char
                self.dirs[path][s] = []  # empty list (for now) of files in sub-directory

        log.debug("The following directories found: %s" % (str(self.dirs[path])))
        
        return self
    
    def get_files(self):
        
        log = self.logger
        
        log.debug("Retrieving files from dropzone folders.")        
        
        for path in self.dirs:
            for sub in self.dirs[path]:
                tpath = os.path.join(path, sub)
                log.debug("Retrieving files from dropzone folder %s" % str(tpath))
                files = []
                for (dirpath, dirnames, filenames) in os.walk(tpath):  # let's try walk
                    files.extend(filenames)
                    break  # only want top-level, got what we needed
                if len(files) == 0:
                    self.dirs[path][sub] = []
                    continue
                
                okFiles = []
                for file in files:  # check for forbidden files
                    if file[0] not in ['.']:
                        okFiles.append(file)
                    
                self.dirs[path][sub] = okFiles
                
                log.debug("Found files %s" % str(self.dirs[path][sub]))
        
        return self
