import logging
import os
import sys

moduleName = 'main.validate'
moduleLogger = logging.getLogger(moduleName)

# The beginnings of data type check functions, to be built-out
# Also start thinking about sub-classes for each type

## Below, had been considering creating a new class....

#class Datum:
#    def __init__(self, myString, **kwargs):
#        self.value = myString
#        self.logger = logging.getLogger(moduleName + '.Datum')
        
#    def check_int(self, myString, **kwargs):
#        self.min = kwargs.get('min', -sys.maxint - 1)
#        self.max = kwargs.get('max', sys.maxint)
        
#        myInt = int(myString)
#        if myInt < self.min or myInt > self.max:
#            raise ValueError("Integer (%i) is beyond range defined by minimum of %i and maximum of %i." % (myInt, self.min, self.max))
#        self.value = myInt
        
#        return self
    
#    def check_long(self, myString, **kwargs):
#        self.min = kwargs.get('min', -9223372036854775807)
#        self.max = kwargs.get('max', 9223372036854775807)
        
#        myLongInt = long(myString)
#        if myLongInt < self.min or myLongInt > self.max:
#            raise ValueError("Long integer (%i) is beyond range defined by minimum of %i and maximum of %i." % (myLongInt, self.min, self.max))
#        self.value = myLongInt
        
#        return self
    
#    def check_string(self, myString, **kwargs):
#        self.minLen = kwargs.get('minLen', 0)
#        self.maxLen = kwargs.get('maxLen', 999)
        
#        if len(myString) < minLen or len(myString) > maxLen:
#            raise ValueError("String (%s) is beyond allowable range of length defined as minimum of %i and maximum of %i characters." % (myString, minLen, maxLen))
            
#        self.value = myString
        
#        return self
    
#    def check_float(self, myString, **kwargs):
#        self.min = kwargs.get('min', sys.float_info.min)
#        self.max = kwargs.get('max', sys.float_info.max)
        
#        myFloat = float(myString)
#        if myFloat < self.min or myFloat > self.max:
#            raise ValueError("Float value (%d) is beyond range defined by minium of %d and maximum of %d." % (myFloat, self.min, self.max))

# For whatever reason, I have very mixed feelings about using **kwargs, but seeing as I want to consolidate the calling of
# the following functions when validating records, I'm using them.

def check_int(myString, **kwargs):
    min = kwargs.get('min', -sys.maxint - 1)
    max = kwargs.get('max', sys.maxint)
    
    myInt = int(myString)
    if myInt < min or myInt > max:
        raise ValueError("Integer (%i) is beyond range defined by minimum of %i and maximum of %i." % (myInt, min, max))
        
    return myInt

def check_long(myString, **kwargs):
    min = kwargs.get('min', -9223372036854775807)
    max = kwargs.get('max', 9223372036854775807)
    
    myLongInt = long(myString)
    if myLongInt < min or myLongInt > max:
        raise ValueError("Long integer (%i) is beyond range defined by minimum of %i and maximum of %i." % (myLongInt, min, max))
        
    return myLongInt

def check_string(myString, **kwargs):
    minLen = kwargs.get('minLen', 0)
    maxLen = kwargs.get('maxLen', 999)
    trunc = kwargs.get('trunc', False)
    valRange = kwargs.get('valRange', [])
    
    if len(valRange) == 0:  # stubbed out for now for range-constrained validation
        pass
    
    if len(myString) < minLen or len(myString) > maxLen:  # no truncate option right now, just raise
        raise ValueError("String (%s) is beyond allowable range of length defined as minimum of %i and maximum of %i characters." % (myString, minLen, maxLen))
        
    return myString

def check_float(myString, **kwargs):
    min = kwargs.get('min', sys.float_info.min)
    max = kwargs.get('max', sys.float_info.max)
    
    myFloat = float(myString)
    if myFloat < min or myFloat > max:
        raise ValueError("Float value (%d) is beyond range defined by minimum of %d and maximum of %d." % (myFloat, min, max))
        
    return myFloat

def check_path(path, cfg_param):
    path = path.strip()  # updating
    if not os.path.abspath(path):
        raise ValueError("Invalid path (%s) specified for parameter %s, can not continue. The path specified can be absolute or relative." % (path, cfg_param))
    if os.path.isdir(path) is not True:
        print "Invalid path (" + path + ") specified for parameter " + cfg_param + ", will try creating it."
        try:
            os.makedirs(path)
            print "Directory successfully created, continuing on..."
        except:
            raise ValueError("Invalid path (%s) specified for parameter %s, can not continue. The path must exist on the file system.\n Tried creating it, to no avail." % (path, cfg_param))
    path = os.path.abspath(path)  # updating
    if os.access(path, os.W_OK) is not True:
        raise RuntimeError("User running this application does not have write access to the path (%s) specified, can not continue." % (path))
        
    return path

def check_log_params(params):
    
    log = logging.getLogger(moduleName + '.check_log_params')

    log.info("Validating configuration parameters.")
    
    vld_log_lvls = ['INFO', 'DEBUG', 'WARNING', 'ERROR']  # hard-coded

    rem_keys = []  # track keys that will be removed, that do not survive validation
    
    for key in params:
        log.debug("Parameter %s found with value of %s." % (key, params[key]))
        if key[-6:] == 'log_fn':  # check name of log file specified, hard-coded
            log.debug("Found log_fn parameter.")
            if params[key] == '':
                raise ValueError("Invalid filename (%s) specified for parameter %s, can not continue.  The filename can not be blank." % (params[key], key))
                
        if key[-4:] == 'path':  # check specified path, hard-coded
            log.debug("Found path.")
            t_path = ''
            if params[key].find(',') != -1:  # comma found, suggests multiple entries..
                log.debug("Multiple entries (%s) detected for parameter %s." % (str(params[key]), key))
                path_lst = params[key].split(',')
                path_lst_new = []
                for t_path in path_lst:
                    t_path = check_path(t_path, key)
                    path_lst_new.append(t_path)
                params[key] = path_lst_new  # updating
            elif key == 'dz_alt_path':  # dropzone treated as list of paths...in this case list of length 1, hard-coded
                path = check_path(params[key], key)
                params[key] = []
                params[key].append(path)
            else:  # any other path parameters treated as string indicating single path
                path = check_path(params[key], key)
                params[key] = path  # updating
                
        if key[-7:] == 'log_lvl':  # check log level, hard-coded
            log.debug("Found log_level parameter.")
            params[key] = params[key].upper()  # updating
            if params[key] not in vld_log_lvls:
                raise ValueError("Invalid log level (%s) specified for parameter %s, can not continue.  The log level must be set to one of: %s." % (params[key], key, str(vld_log_lvls)))
         
        if key[-13:] == 'log_file_size':  # check log file size, hard-coded
            log.debug("Found log_file_size parameter.")
            try:  # check log file size
                params[key] = int(params[key])
            except:
                raise TypeError("Invalid file size (%s) specified for parameter %s, can not continue. Must be an integer representing number of bytes per file." % (params[key], key))

        if key[-13:] == 'log_max_files':  # check maximum number of log files, hard-coded
            log.debug("Found log_max_files parameter.")
            try:
                params[key] = int(params[key])
            except:
                raise TypeError("Invalid maximum number of files (%s) specified for parameter %s, can not continue. This value must be an integer representing total number of log files permitted." % (params[key], key))
            
        if key == 'rpt_intvl':  # hard-coded
            try:
                params[key] = int(params[key])
            except:
                log.warning("Invalid number (%s) of seconds specified for paramter %s, will continue using default value. This parameter must be an integer representing interval at which report metrics should be generated." % (params[key], key))
                rem_keys.append(key)
    
    log.debug("Parameters (%s) failed validation, removing from initialization. These may be set to defaults, as necessary." % str(rem_keys))
    
    for key in rem_keys:
        del params[key]
        
    # Some setting of defaults...can be moved to more specific modules, if desired...probably best handled here, though...
    
    if 'failed_tx_path' not in params:  # hard-coded
        params['failed_tx_path'] = params['log_path']  # main log directory
    if 'failed_tx_fn' not in params:  # hard-coded
        params['failed_tx_fn'] = 'failedTx.log'  # default name
    
    # prolly need to add config params for default number of files, file size, etc.
                
    log.info("Configuration parameters validated.")
    
    return params

def test_txt_bin(self):  # Expects object of type 'FileReader'
    
    log = logging.getLogger(moduleName + '.test_txt_bin')

    log.info("Testing data to determine binary or text content.")
    
    txtCharList = range(32, 127)  # hard-coded list of characters we consider valid text
    txtThreshold = 0.30  # ratio above which the number of non-text characters indicates binary file
    blocksize = 4096  # size, in bytes, to use from the beginning of the file to test binary/text
    
    log.debug("Qualifying list of text characters (ASCII ordinates) is set to %s." % txtCharList)
    
    testString = 'init'
    with open(self.f.name, 'r') as file:
        testString = file.read(blocksize)

    i = 0
    for c in testString:
        if ord(c) in txtCharList:
            continue
        else:
            i += 1
    
    try:  # may be empty (div by 0)
        txtRatio = float(i) / float(len(testString))
    except:
        log.debug("Whoops, length of the test string appears to be zero: %s" % testString)
        txtRatio = 0  # consider empty file to be text file

    if txtRatio > 0.30:
        self.datatype = 'bin'
        log.debug("%i characters found *not* to be text characters, out of %i, for a ratio of %s%%.  This looks to be binary." % (i, len(testString), str(round(txtRatio*100, 2))))
    else:
        self.datatype = 'txt'
        log.debug("%i characters found *not* to be text characters, out of %i, for a ratio of %s%%.  This looks to be a text file." % (i, len(testString), str(round(txtRatio*100, 2))))
        
    return self

def check_schema(fSchema, defList=[]):
    
    log = logging.getLogger(moduleName + '.check_schema')
    
    defList=['label', 'type', 'subtype', 'maxSize', 'notNull', 'valRange']  # parameterize this as built out for other types (e.g., non-csv)..some of this function is specific to CSV, can later break out...
    
    log.debug("Schema found to have definition for %i fields/values." % len(fSchema.schema))
    
    # Some assumptions are made below as to what the schema should look like (based on CSV)
    # Below there are elements of logic specific to CSV as well as elements that could be made
    # into more general validation logic. To-do...
    
    chekdSch = []  # hold new schema as we validate/fix
    i = 0  # going to use as entry index
    for entry in fSchema.schema:
        
        # Check overall apperance of schema entries...
        
        i += 1
        p = 0  # going to use as parameter index within each entry
        if len(entry) == 0:  # would mean something wonky
            log.warning("Somehow a schema entry (list) with length zero made it here. Going to continue with next entry, but list of schema entries is %s." % str(fSchema.schema))
            continue
        chekdEntry = []  # hold new list of values for this particular entry as they are validated/fixed
        entryLen = len(entry)  # number of parameters detected
        defListLen = len(defList)  # number of defined parameters
        if entryLen > defListLen:
            log.warning("Number of parameters in schema entry %i exceeds those accommodated (%i), will use only the first %i." % (i, defListLen, defListLen))
                        
        # Parse and validate individual parameters
        
        # Column/field label
        if entry[p] == '':  # this should never happen
            chekdEntry.append("Column_" + i)
        else:
            chekdEntry.append(entry[p])  # column label
        
        p += 1  # Column/field data type
        try:
            pyType = entry[p]
        except:
            pyType = 'string'  # default
        if pyType.lower() in ['int', 'long', 'float', 'string', 'list', 'dict']:  # hard-coded supported types
            chekdEntry.append(entry[p].lower())
        else:
            log.warning("No %s defined for schema entry %i. Setting to default. This may or not be okay, depending on specific usage intended." % (defList[p], i))
            chekdEntry.append('string')
            
        p += 1  # Column/field sub-type
        try:
            subType = entry[p]
        except:
            log.info("No valid value specified for schema entry %i, parameter %s. Using default." % (i, defList[p]))
            subType = ''  # default
        chekdEntry.append(subType)
        # Add more content here.....?
        
        p += 1  # Column/field max [number/length]
        try:
            maxSize = int(entry[p])
        except:
            log.info("No valid value specified for schema entry %i, parameter %s. Using default." % (i, defList[p]))
            maxSize = ''  # Should something more substantive be used to indicate no value defined?
        chekdEntry.append(maxSize)
        
        p += 1  # Column/field not null indicator
        try:
            notNull = entry[p]
        except:
            notNull = False
        if notNull.upper() in ['TRUE', '1', 'YES']:  # couple hard-coded variations on whether field should be True
            chekdEntry.append(True)
        else:
            chekdEntry.append(False)
            
        p += 1  # Column/field range of valid values
        try:
            valRange = entry[p]  # is a string, for now
            if valRange == '':
                valRange = []
        except:
            log.info("No valid value specified for schema entry %i, parameter %s. Using default." % (i, defList[p]))
            valRange = []  # Should something more substantive be used to indicate no value defined?
            
        if valRange:
            if valRange[0] != '[' and valRange[:-1] != ']':
                log.warning("It appears that an attempt was made to configure parameter %s in entry %i, but it could not be properly parsed. Check for bracket bookends." % (defList[p], i))
                valRange = []  # Should something more substantive be used to indicate no value defined?
            else:
                valRangeStr = valRange[1:-1]  # don't want brackets
                tmpValRange = []
                for val in valRangeStr.split(';'):  # hard-coded semi-colon delimiter for values in the list of valid values; probably need to accommodate hyphens, too...
                    tmpValRange.append(val.strip())
                valRange = tmpValRange  # type-switch, valRange just went from string to list type (good ol' Python?)
        
        chekdEntry.append(valRange)
        # Add more content here....?  Accommodate integers, actual ranges (defined by hyphens, etc.)...
        
        chekdSch.append(chekdEntry)
    
    log.info("The following schema was loaded, validated, and will be used to process data of type '%s':\n" % fSchema.datatype)
    for entry in chekdSch:
        log.info("     %s" % str(entry))
    
    fSchema.schema = chekdSch
    
    return fSchema