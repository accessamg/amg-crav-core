import logging
import os
import sys
import time
import csv

import validate
#import external
import util

moduleName = 'main.parse'
moduleLogger = logging.getLogger(moduleName)

def twtr_statuses(ext):
    # Expects TwtrSrch() object.
    
    log = logging.getLogger(moduleName + '.twtr_statuses')
    
    log.debug("Parsing tweet statuses, keeping only fields of interest.")
    
    t_append = ext.rcds.append  # make it local
    for i in range(len(ext.payload['data'])):
        t_dict = {}
        t_src = ext.payload['data'][i]  # make easy to reference
        if t_src['coordinates'] is not None:
            t_dict['coordinates_long'] = t_src['coordinates']['coordinates'][0]  # yes, long is first
            t_dict['coordinates_lat'] = t_src['coordinates']['coordinates'][1]  # yes, lat is second
        t_dict['created_at'] = t_src['created_at']  # UTC
        t_dict['favorited'] = t_src['favorited']
        t_dict['id_str'] = t_src['id_str']
        t_dict['id'] = t_src['id']  # integer
        # below, keep just tags, as list
        t_dict['entities_hashtags'] = [ t_src['entities']['hashtags'][i]['text'] for i in range(len(t_src['entities']['hashtags'])) ]
        # below, keep just name and screen name, as list of dicts
        t_dict['entities_user_mentions'] = [ { 'name': t_src['entities']['user_mentions'][i]['name'] or 'p_None', 'screen_name': t_src['entities']['user_mentions'][i]['screen_name'] } for i in range(len(t_src['entities']['user_mentions'])) ]
        t_dict['text'] = t_src['text']
        t_dict['retweet_count'] = t_src['retweet_count']
        t_dict['retweeted'] = t_src['retweeted']
        t_dict['user_name'] = t_src['user']['name']
        t_dict['user_profile_image_url'] = t_src['user']['profile_image_url']
        t_dict['user_location'] = t_src['user']['location']
        t_dict['user_id_str'] = t_src['user']['id_str']
        t_dict['user_id'] = t_src['user']['id']
        t_dict['user_followers_count'] = t_src['user']['followers_count']
        t_dict['user_time_zone'] = t_src['user']['time_zone']
        t_dict['user_statuses_count'] = t_src['user']['statuses_count']
        t_dict['user_friends_count'] = t_src['user']['friends_count']
        t_dict['user_screen_name'] = t_src['user']['screen_name']
        t_dict['user_favourites_count'] = t_src['user']['favourites_count']
        # below, whatever we want from retweeted_status
        if 'retweeted_status' in t_src:  # see if it's a retweet
            t_dict['retweeted_status_text'] = t_src['retweeted_status']['text']  # original tweet content
            t_dict['retweeted_status_id'] = t_src['retweeted_status']['id']  # may use to check for dupes when displaying

        t_append(t_dict)
        
    log.debug("Twitter status parsing complete.")
        
    return ext

def inst_media(media):
    # expects a media object as returned from instagram api
    # returns dictionary of attributes
    
    media_dict = {}
    media_dict['id'] = media.id
    # below, created_time from Instagram is epoch ms, the API, though, converts
    # this to a datetime object...let's convert it back...
    media_dict['created_time'] = int(util.get_epch_ms(media.created_time) / 1000.0) # epoch secs
    media_dict['type'] = media.type
    try:  # evidently (with older media?), tags may not exist
        media_dict['tags'] = [tag.name for tag in media.tags]  # list of tags
    except AttributeError:
        pass
    try:  # evidently (with old media??), caption can be NoneType
        media_dict['caption_text'] = media.caption.text  # contents of caption
    except AttributeError:
        pass
    media_dict['likes_count'] = len(media.likes)  # media.likes being a list of User objects, just getting count...for now...
    media_dict['link'] = media.link  # URL
    media_dict['user_username'] = media.user.username
    media_dict['user_profile_picture'] = media.user.profile_picture  # URL
    media_dict['user_id'] = media.user.id
    media_dict['user_full_name'] = media.user.full_name
    media_dict['images_low_resolution_url'] = media.images['low_resolution'].url
    media_dict['images_low_resolution_width'] = media.images['low_resolution'].width
    media_dict['images_low_resolution_height'] = media.images['low_resolution'].height
    media_dict['images_thumbnail_url'] = media.images['thumbnail'].url
    media_dict['images_thumbnail_width'] = media.images['thumbnail'].width
    media_dict['images_thumbnail_height'] = media.images['thumbnail'].height
    media_dict['images_standard_resolution_url'] = media.images['standard_resolution'].url
    media_dict['images_standard_resolution_width'] = media.images['standard_resolution'].width
    media_dict['images_standard_resolution_height'] = media.images['standard_resolution'].height
#    t_dict['location'] = media.location  # not seeing this in object

    # will become entities in child adapters
#    print media.comments
#    print media.likes
#    raise
#    media_dict['comments'] = media.comments
#    media_dict['likes'] = media.likes

    return media_dict

def inst_usr(usr):
    # expects a user object as returned from instagram api
    # returns dictionary of attributes
    
    usr_dict = {}
    # the following fields are in every inst_user object...(NOT QUITE)
    usr_dict['id'] = usr.id
    usr_dict['username'] = usr.username
    usr_dict['full_name'] = usr.full_name
    usr_dict['profile_picture'] = usr.profile_picture
    
    # the following fields are not in abbreviated inst_user objects..
    try:
        usr_dict['bio'] = usr.bio
        usr_dict['website'] = usr.website
        usr_dict['counts_media'] = usr.counts['media']
        usr_dict['counts_follows'] = usr.counts['follows']
        usr_dict['counts_followed_by'] = usr.counts['followed_by']
    except:
        pass
    
    return usr_dict

def inst_cmnt(cmnt, media_id):
    # expects a comment object as returned from instagram api and
    # media_id of parent
    # returns dictionary of attributes
    
    cmnt_dict = {}
    cmnt_dict['id'] = cmnt.id
    # below, accommodate fact this is a child adapter...need parent media ID...
    cmnt_dict['media_id'] = media_id  # bit of cheating here, as 'parse' usually all from source...
    # below, created_time from Instagram is epoch ms, the API, though, converts
    # this to a datetime object...let's convert it back...
    cmnt_dict['created_at'] = int(util.get_epch_ms(cmnt.created_at) / 1000.0)  # epoch secs
    cmnt_dict['text'] = cmnt.text
    cmnt_dict['user_username'] = cmnt.user.username
    cmnt_dict['user_profile_picture'] = cmnt.user.profile_picture
    cmnt_dict['user_id'] = cmnt.user.id
    cmnt_dict['user_full_name'] = cmnt.user.full_name

    return cmnt_dict

def inst_like(like, media_id):
    # expects a like object as returned from instagram api and
    # media ID of parent
    # returns dictionary of attributes
    
    # like object is actually an abbreviated user object
    like_dict = inst_usr(like)
    # below, accommodate fact this is a child adapter...need parent media ID...
    like_dict['media_id'] = media_id
    
    return like_dict

def fb_obj(obj, flds=[]):
    # expects FB object as returned from API
    # expects flds (list) returned by FB api
    # returns dictionary of attributes

    if flds:  # list passed
        return { fld : obj[fld] for fld in flds }
    else:  # just get 'em all
        return { key : obj[key] for key in obj.keys() }
        
#def fb_like(like, prnt_id):
#    # Expects a like object as returned from FB api and
#    # ID of parent object.
#    # returns dictionary of attributes
    
#    # like object is actually an abbreviated user object
#    like_dict = fb_obj(like)
#    # below, accommodate fact this is a child adapter...need parent media ID...
#    like_dict['prnt_id'] = prnt_id
    
#    return like_dict

#def fb_usr(usr):
#    # Expects a user object as returned from FB api
#    # returns dictionary of attributes
    
#    return usr  # no parsing needed, already dict of attributes

def fb_cmnt(cmnt):
    # expects FB comment object as returned from API
    # returns dictionary of attributes
    
    log = logging.getLogger(moduleName + '.fb_cmnt')
    
    cmnt_dict = {}
    
    # these fields are always there
    cmnt_dict['created_time'] = cmnt.pop('created_time')
    cmnt_dict['id'] = cmnt.pop('id')
    cmnt_dict['message'] = cmnt.pop('message')
    cmnt_dict['comment_count'] = cmnt.pop('comment_count')
    cmnt_dict['like_count'] = cmnt.pop('like_count')
    cmnt_dict['from_name'] = cmnt['from']['name']
    cmnt_dict['from_id'] = cmnt['from']['id']
    
    if 'parent' in cmnt:
        cmnt_dict['parent_created_time'] = cmnt['parent']['created_time']
        cmnt_dict['parent_message'] = cmnt['parent']['message']
        cmnt_dict['parent_from_name'] = cmnt['parent']['from']['name']
        cmnt_dict['parent_from_id'] = cmnt['parent']['from']['id']
        cmnt_dict['parent_id'] = cmnt['parent']['id']
        
    if 'attachment' in cmnt:
        print cmnt
        raise
    
    return cmnt_dict
        
        
def fb_post(post):
    # expects FB post object as returned from API
    # returns dictionary of attributes
    
    log = logging.getLogger(moduleName + '.fb_post')
    
    post_dict = {}
    # seems like these will always be there...??
    post_dict['created_time'] = post.pop('created_time')
    post_dict['id'] = post.pop('id')
    post_dict['type'] = post.pop('type')

    # these seem to be optional....
    # message_tags and with_tags not accounted for
    if 'status_type' in post:  # only absent in older posts???
        post_dict['status_type'] = post.pop('status_type')
    if 'message' in post:
        post_dict['message'] = post.pop('message')
    if 'from' in post:
        from_info = post.pop('from')
        post_dict['from_name'] = from_info['name']
        post_dict['from_id'] = from_info['id']
    if 'icon' in post:
        post_dict['icon'] = post.pop('icon')
    if 'caption' in post:
        post_dict['caption'] = post.pop('caption')
    if 'description' in post:
        post_dict['description'] = post.pop('description')
    if 'link' in post:
        post_dict['link'] = post.pop('link')
    if 'object_id' in post:
        post_dict['object_id'] = post.pop('object_id')
    if 'name' in post:
        post_dict['name'] = post.pop('name')
    if 'picture' in post:
        post_dict['picture'] = post.pop('picture')
    if 'place' in post:
        place = post.pop('place')
        post_dict['place_id'] = place['id']
        post_dict['place_location'] = place['location']
        post_dict['place_name'] = place['name']
    if 'shares' in post:
        shares = post.pop('shares')
        post_dict['shares_count'] = shares['count']
    if 'source' in post:
        post_dict['source'] = post.pop('source')
    if 'story' in post:
        post_dict['story'] = post.pop('story')
    if 'to' in post:
        to = post.pop('to')
        post_dict['to'] = to['data']  # list of dicts: { name, id }
    if 'updated_time' in post:
        post_dict['updated_time'] = post.pop('updated_time')
    if 'message_tags' in post:
        # e.g.: {u'message_tags': {u'44': [{u'length': 10, u'offset': 44, u'type': u'page', u'id': u'214152816044', u'name': u'Otter Pops'}]}}.
        message_tags = post.pop('message_tags')
        post_dict['message_tags'] = []
        # dropping sequence indicator (k)
        for k, v in message_tags.iteritems():
            # keep only type, id, name
            for entry in v:
                entry.pop(u'length')
                entry.pop(u'offset')
                if not entry['name']:
                    entry['name'] = 'e_None'
                post_dict['message_tags'].append(entry)  # a list of dicts: { type, id, name }
    if 'with_tags' in post:
        with_tags = post.pop('with_tags')
        for i in xrange(len(with_tags['data'])):
            if not with_tags['data'][i]['name']:
                with_tags['data'][i]['name'] = 'e_None'
        post_dict['with_tags'] = with_tags['data']  # a list of dicts: { name, id }
        
    if len(post) != 0:
        log.error("Something was missed while parsing. Object that is left: %s." % str(post))
        print "Something was missed while parsing. Object that is left: %s." % str(post)
        raise

    return post_dict

#def fb_usr(usr):
#    # expects FB user object as returned from API
#    # returns dictionary of attributes
    
#    USING fb_obj AT THE MOMENT
    
#    log = logging.getLogger(moduleName + '.fb_usr')
    
#    usr_dict = {}
    
#    return usr_dict

    
    
    
        

    
    
    

    if flds:  # list passed
        return { fld : obj[fld] for fld in flds }
    else:  # just get 'em all
        return { key : obj[key] for key in obj.keys() }
    

###
### ABOVE THIS LINE HAS BEEN VETTED ###
###

class chunk():

    def __init__(self):

        self.logger = logging.getLogger(moduleName + '.chunk')
        
class csvRow(chunk):
    
    def __init__(self):
        csvRow.params = ['label', 'type', 'subtype', 'maxLength', 'notNull', 'valRange']  # list of parameters available (valid) in field definitions, hard-coded for now (ever?)...this explicit list also exists in validate.py.check_schema...tut, tut...
        csvRow.label = []  # labels for each field, as pulled from schema file (required)
        csvRow.type = []  # Python type for each field, as pulled from schema file (optional)
        csvRow.subtype = []  # subtype classification for each field, as pulled from schema file (optional)
        csvRow.maxLength = []  # maximum length for each field, as pulled from schema file (optional)
        csvRow.notNull = []  # whether (0) or not (1) the field can be empty
        csvRow.fieldDef = []  # also attaching schema as list of tuples, each tuple holding params for a single field...not sure which will be handier...
        csvRow.delim = ','  # default to comma
        csvRow.functions = []  # list of validation functions associated with each parameter
        csvRow.kwargs = {}  # kwargs associated with each function
        csvRow.sub = ''  # sub-directory from whence data originated (guides processing)
        csvRow.parser = ''  # parser (needed?)
        csvRow.srcId = -1
        csvRow.strmId = -1
 
        # The below parameter was originally meant to hold values for a single row, where the parameters above
        # would effectively be 'metadata' for that row. The values would change for each row processed.
        # After some consideration (see 'parse' function comments, below), for now the paramter below will store
        # *all* rows in a given file, as a list of lists.
        
        csvRow.values = []  # values in the row, as pulled from data file, the only attribute that will change from row to row while processing a file
        
        self.logger = logging.getLogger(moduleName + '.csvRow')
        
#    def validate_row(self, fSchema, rowString=''):  # old csv parse
    def validate_row(self, fSchema, rowList):
        
        self.logger = logging.getLogger(moduleName + '.csvRow.validate')
        
#        rowString = rowString.strip()  # I wonder the potential repercussions from this (blank spaces?)...want EOL chars gone, though... # old csv parse
#        rowList = rowString.split(self.delim)  # old csv parse
        lenRowList = len(rowList)
        if lenRowList != len(self.label):
            raise ValueError("Incorrect number (%i) of fields detected (should be %i). Considering this a bad record, not processing." % (lenRowList, len(self.label)))
        
        for i in range(lenRowList):
            try:
                rowList[i] = self.functions[i](rowList[i])  # validate field vlaue
            except:
                raise ValueError("Value %s is invalid for field %s, this record will not be processed." % (rowList[i], self.label[i]))
        
        return rowList
        
    def parse(self, fSchema, fMetrics, configParams):
        
        self.logger = logging.getLogger(moduleName + '.csvRow.parse')
        
        log = self.logger
        
        # should have check here for order of parameters, probably, based on name for each param (e.g., 'label') that should probably be required in the schema file
        
        i = 0
        for fieldDef in fSchema.schema:
            if len(fieldDef) != len(csvRow.params):  # need to have correct number of parameters defined for each field, otherwise yell
                raise ValueError("Not all fields have correct number of field definition parameters defined in the schema file %s.  If an optional parameter is specified (even as empty) for one field, it must be specified for all fields." % fSchema.filename)
            self.label.append(fSchema.schema[i][0])
            self.type.append(fSchema.schema[i][1])
            self.subtype.append(fSchema.schema[i][2])
            self.maxLength.append(fSchema.schema[i][3])
            self.notNull.append(fSchema.schema[i][4])
            i += 1
       
       # Immediately above, I used schema information to populate lists self.label, self.type,
       # self.subtype, self.maxLength, and self.notNull.  I'm not sure which will be more convenient
       # in the future, so for now let's also populate field definitions as a lit of discrete tuples, one for each
       # field definition.
       
        for i in range(len(self.label)):  # for now, i also want params for a field's definition in a discrete tuple
           csvRow.fieldDef.append((self.label[i], self.type[i], self.subtype[i], self.maxLength[i], self.notNull[i]))
           
        log.debug("The following field definitions are loaded for file %s" % str(fMetrics.filename))
        for i in range(len(self.fieldDef)):
            log.debug("%s" % str(self.fieldDef[i]))
        
#        fullPath = os.path.join(fMetrics.path, fMetrics.sub, fMetrics.filename)
#        destPath = os.path.join(fMetrics.path, fMetrics.sub, 'processed', fMetrics.filename)  # move it when done

        # The last thing I want to do before processing the lines is define for each field which
        # validation method should be used. I'm not sure the best way to do this, but - and I may be overthinking this - I 
        # did not want to do any sort of testing or interrogation for *each* line processed, where the result of that
        # testing/interrogation would set the function name.  Since any given field is expected to have the same data
        # type throughout the file, just set these function names onces.
        
        for i in range(len(self.label)):
            try:
                self.functions.append(getattr(validate, 'check_' + self.type[i]))  # **ATTENTION, HARD-CODED FUNCTION NAMES** Find myself wondering how hackish this is...but this sets the function/method to use for each field...
            except Exception, e:
                log.error("Unable to define validation function for field %s. Aborting." % self.label[i])
                raise
        
        log.debug("List of validation functions to be used for CSV fields is %s." % str(self.functions))
        
        # I have an urge to return each CSV _row_ and process each individually. Perhaps
        # it's the Streamy McStreamerson in me. At any rate, for now, I'll keep the contents of the entire
        # file together, for a couple of reasons:
        #
        # - Function calls are going to introduce overhead, however minor. Passing an aggregated data structure would be preferable in Python.
        # - I keep the whole file together, so I know when each operation is completed on the entire file (when the entire contents of that file
        #   are returned).
        #
        # Looking ahead, if you were to deploy this on some type of streaming platform, "tuples" are going to be rows in a file, not the contents
        # of the entire file, so data would have to be broken down accordingly...and the 'csvRow' object perhaps propagated beyond its limited use
        # here.
        #
        # To that end, I think (for now?) we'll have csvRow.values store the contents of the post-validated file. Let's use Python's csv parser
        # to populate csvRow.values.
        
        with open(os.path.join(fMetrics.path, fMetrics.sub, fMetrics.filename)) as fp:
            fMetrics.initTime = time.time()
#            for line in fp:  # old csv parse
#                try:  # old csv parse
            try:
                reader = csv.reader(fp)
                for row in reader:
                    rowList = csvRow.validate_row(self, fSchema, row)  # returns row as list of values
                    seenPrev = fMetrics.chunks['cur']
                    fMetrics.chunks['cur'] += 1
                    rowSize = sys.getsizeof(row)
                    rowLength = len(row)
                    fMetrics.chunks['avgSize'] = ((fMetrics.chunks['avgSize'] * seenPrev) + rowSize) / (seenPrev + 1)
                    fMetrics.chunks['avgLen'] = ((fMetrics.chunks['avgLen'] * seenPrev) + rowLength) / (seenPrev + 1)
                    fMetrics.chunks['complt'] += 1
                    self.values.append(rowList)
            except Exception, e:
                log.info("%s" % str(e))
                log.info("Following record failed to process, being written to %s and continuing: %s" % (configParams.configDict['failedTxFilename'], str(row)))
#                fBadRecords = external.RotatingFile()
#                fBadRecords = external.RotatingFile(configParams.configDict['failedTxPath'], '', configParams.configDict['failedTxFilename'])
                fBadRecords.open()
                fBadRecords.write(str(row))
                fBadRecords.close()
                fMetrics.chunks['failed'] += 1
            finally:
                seenPrev = fMetrics.chunks['cur']
                fMetrics.chunks['cur'] += 1
        
        fMetrics.endTime = time.time()                
        fMetrics.chunks['total'] = fMetrics.chunks['cur']
        
        return self, fMetrics
        
def find_parser(fSchema, fMetrics, configParams):
    
    log = logging.getLogger(moduleName + '.find_parser')
    
    log.debug("Find a parser for this data file...")

    #if fMetrics.parser in ['csv', 'sort']:  # once switched to db-driven config
    if fMetrics.datatype in ['csv', 'sort']:  # strictly using csv parser for sort ops right now
        rowData = csvRow()
        rowData.sub = fMetrics.sub  # originating sub-directory
        rowData.parser = fMetrics.parser  # needed? prolly not...i.e., does info specific to source need to go forward with parsed data...?
        rowData.srcId = fMetrics.srcId  # needed? prolly not
        rowData.strmId = fMetrics.strmId  # needed? prolly not unless fMetrics does not get propagated forward...might  make sense to stay with data (rowData)...
        rowData, fMetrics = csvRow.parse(rowData, fSchema, fMetrics, configParams)
    #elif:  # future/additional parsers
    else:
        log.warning("No parser found for datatype %s. Since I don't know how to handle any data files in that sub-directory, I've no choice but to ignore them." % fSchema.datatype)
        rowData = csvRow()  # no data
        return None, fMetrics  # or return 'empty'/other csvRow object?
        
    fMetrics.parser = True
        
    return rowData, fMetrics
    