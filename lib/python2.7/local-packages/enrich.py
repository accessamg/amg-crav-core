# Classes and functions facilitating analysis (vague enough?)

import logging
import dateutil
from geopy.geocoders import Nominatim

import util

module_nm = 'main.enrich'
module_logger = logging.getLogger(module_nm)

def common(ext):
    # Expects an Ext() object with rcds and certain metadata.
    
    log = logging.getLogger(module_nm + '.' + 'common')
    batch_id = util.get_batch_id(ext.cfg['id'])  # same batch ID for all self.rcds

    new_flds = {}
    for i in xrange(len(ext.rcds)):
        new_flds['ext_id'] = int(ext.cfg['id'])  # type identity crisis
        new_flds['e_batch_id'] = batch_id
        new_flds['e_proc_epch_ms'] = util.get_epch_ms()
        new_flds['e_rcd_seq'] = i + 1
        ext.rcds[i].update(new_flds)

    log.debug("Common enrichment attributes added to records.")
    
    return ext

def chld_rcd(ext, prnt_id_nm, chld):
    # Expects an Ext() object with rcds
    # rcds should each have parent id attribute
    # chld is name of child adapter (e.g., 'likes')
    
    log = logging.getLogger(module_nm + '.chld_rcd')
    batch_id = util.get_batch_id(ext.cfg['children'][chld])

    new_flds = {}
    for i in xrange(len(ext.rcds)):
        try:
            new_flds['e_prnt_rcd_id'] = ext.rcds[i][prnt_id_nm]
        except Exception as e:
            print str(ext.rcds[i])
            print e
            print str(ext.rcds)
            raise
        new_flds['ext_id'] = int(ext.cfg['children'][chld])
        new_flds['e_batch_id'] = batch_id  # overwrite that done by common (parent)
        ext.rcds[i].update(new_flds)

    log.debug("Child record enrichment completed.")
    
    return ext
    
def enrich_twtr_statuses(twtr_ext):
    # Expects a Ext.Twtr() object with populated records.
    # This could definitely be made more efficient.
    
    log = logging.getLogger(module_nm + '.' + 'enrich_twtr_statuses')
    
    log.debug("Beginning enrichment of Twitter records.")
    
    twtr_ext = common(twtr_ext)
    
    new_flds = {}
    for i in xrange(len(twtr_ext.rcds)):
        dttm = dateutil.parser.parse(twtr_ext.rcds[i]['created_at'])
        new_flds['e_eff_dttm_ts'] = util.get_sql_ts(dttm)
        new_flds['e_eff_epch_ms'] = util.get_epch_ms(dttm)
        new_flds['e_rcd_id'] = twtr_ext.rcds[i]['id_str']
        twtr_ext.rcds[i].update(new_flds)

    log.debug("Performing sentiment evaluation.")
    for i in xrange(len(twtr_ext.rcds)):
        new_flds = get_e_sntmnt(twtr_ext.rcds[i]['text'])  # get "e-sentiment" fields
        twtr_ext.rcds[i].update(new_flds)
    log.debug("Completed, the following fields appended to each record: %s" % str(new_flds.keys()))
    
    log.debug("Performing amplitude evaluation.")
    for i in xrange(len(twtr_ext.rcds)):
        new_flds = get_twtr_e_amp(twtr_ext.rcds[i])  # get "e-amplitude" fields
        twtr_ext.rcds[i].update(new_flds)
    log.debug("Completed, the following fields appended to each record: %s" % str(new_flds.keys()))

    log.debug("Performing geographic evaluation.")
    for i in xrange(len(twtr_ext.rcds)):
        if 'coordinates_long' and 'coordinates_lat' in twtr_ext.rcds[i]:  # already has long/lat
            new_flds = { 'e_geo_long' : twtr_ext.rcds[i]['coordinates_long'], 'e_geo_lat' : twtr_ext.rcds[i]['coordinates_lat'] }
            # don't retrieve anything else (for now?)
        else:
            new_flds = get_e_geo(twtr_ext.rcds[i]['user_location'])  # submit location info
        if i % 10 == 0:  # log every 10
            log.debug("10 tweets processed for geo.")
        twtr_ext.rcds[i].update(new_flds)
    log.debug("Completed, the following fields appended to each record: %s" % str(new_flds.keys()))
    
    log.debug("Performing retweet evaluation.")
    for i in xrange(len(twtr_ext.rcds)):
        new_flds = get_retweet_info(twtr_ext.rcds[i])
        twtr_ext.rcds[i].update(new_flds)
    log.debug("Completed, the following fields appended to each record: %s" % str(new_flds.keys()))
    
    log.debug("Twitter record enrichment complete.")
        
    return twtr_ext

def enrich_twtr_statuses_srch(twtr_ext):
    # Expects a Ext.Twtr() object with populated records.
    # This could definitely be made more efficient.
    
    log = logging.getLogger(module_nm + '.' + 'enrich_twtr_statuses_srch')
    
    log.debug("Performing hashtag/mention/text evaluation.")
    
    # below, for each, checking intersection of content with config (true/false)
    for i in xrange(len(twtr_ext.rcds)):
        new_flds = { 'e_hashtag': 0, 'e_mention': 0, 'e_text': 0, 'e_other': 1 }  # default to false for both
        # below, force to lowercase, check for intersection
        # this is really just supporting 'OR' right now...'AND' might have to compare entire list of tags/mentions...
        for conj in util.cfg_app['ext']['id'][twtr_ext.rcds[i]['ext_id']]['q']['hashtag'].keys():  # 'OR', 'AND', etc.
            if set([ tag.lower() for tag in twtr_ext.rcds[i]['entities_hashtags'] ]) & set( [ tag.lower() for tag in util.cfg_app['ext']['id'][twtr_ext.rcds[i]['ext_id']]['q']['hashtag'][conj] ]):
                new_flds['e_hashtag'] = 1  # update to true
                new_flds['e_other'] = 0
        for conj in util.cfg_app['ext']['id'][twtr_ext.rcds[i]['ext_id']]['q']['mention'].keys():  # 'OR', 'AND', etc.
            if set( [ twtr_ext.rcds[i]['entities_user_mentions'][j]['screen_name'].lower() for j in range(len(twtr_ext.rcds[i]['entities_user_mentions'])) ] ) & set( [ mntn.lower() for mntn in util.cfg_app['ext']['id'][twtr_ext.rcds[i]['ext_id']]['q']['mention'][conj] ] ):
                new_flds['e_mention'] = 1  # update to true
                new_flds['e_other'] = 0
        for conj in util.cfg_app['ext']['id'][twtr_ext.rcds[i]['ext_id']]['q']['text'].keys():  # 'OR', 'AND', etc.
            if any(x.lower() in twtr_ext.rcds[i]['text'].lower() for x in util.cfg_app['ext']['id'][twtr_ext.rcds[i]['ext_id']]['q']['hashtag'][conj]):
                new_flds['e_text'] = 1  # update to true
                new_flds['e_other'] = 0
        twtr_ext.rcds[i].update(new_flds)

    log.debug("Twitter search record enrichment complete.")
    
    return twtr_ext

def inst_media(inst_ext):
    
    log = logging.getLogger(module_nm + '.' + 'inst_media')
    
    log.debug("Beginning enrichment of Instagram media records.")
    
    inst_ext = common(inst_ext)
    
    new_flds = {}
    for i in xrange(len(inst_ext.rcds)):
        dttm = util.dttm_from_epch(inst_ext.rcds[i]['created_time'])
        new_flds['e_eff_dttm_ts'] = util.get_sql_ts(dttm)
        new_flds['e_eff_epch_ms'] = util.get_epch_ms(dttm)
#        new_flds['e_eff_epch_ms'] = int(inst_ext.rcds[i]['created_time']) * 1000  # epoch seconds
#        new_flds['e_eff_dttm_ts'] = util.ts_from_epch_ms(new_flds['e_eff_epch_ms'])
        new_flds['e_rcd_id'] = inst_ext.rcds[i]['id']
        inst_ext.rcds[i].update(new_flds)
    
    log.debug("Instagram record enrichment complete.")
    
    return inst_ext

def inst_usr(inst_ext):
    
    log = logging.getLogger(module_nm + '.inst_usr')
    
    log.debug("Beginning enrichment of Instagram user records.")
    
    inst_ext = common(inst_ext)
    inst_ext = add_time_cntxt(inst_ext)
    
    new_flds = {}
    for i in xrange(len(inst_ext.rcds)):
        new_flds['e_rcd_id'] = bld_rcd_id_time(inst_ext.rcds[i], 'id')
        inst_ext.rcds[i].update(new_flds)
    
    log.debug("Instagram user record enrichment complete.")
    
    return inst_ext

def inst_cmnt(inst_ext):
    # child adapter (parent: Instagram media adapter)
    
    log = logging.getLogger(module_nm + '.' + 'inst_cmnt')
    
    log.debug("Beginning enrichment of Instagram comment records.")
    
    inst_ext = common(inst_ext)
    inst_ext = chld_rcd(inst_ext, 'media_id', 'comments')  # with field that has parent rcd ID
    new_flds = {}
    for i in xrange(len(inst_ext.rcds)):
        # below, comments always child adapter...so get child external ID...
        # what is the meaning of the comment above?
        dttm = util.dttm_from_epch(inst_ext.rcds[i]['created_at'])
        new_flds['e_eff_dttm_ts'] = util.get_sql_ts(dttm)
        new_flds['e_eff_epch_ms'] = util.get_epch_ms(dttm)
        new_flds['e_rcd_id'] = inst_ext.rcds[i]['id']  # unique comment ID
        inst_ext.rcds[i].update(new_flds)
        
    log.debug("Instagram media comments enrichment complete.")
    
    return inst_ext

def inst_like(inst_ext):
    # child adapter (parent: Instagram media adapter)
    
    log = logging.getLogger(module_nm + '.' + 'inst_like')
    
    log.debug("Beginning enrichment of Instagram like records.")
    
    inst_ext = common(inst_ext)
    inst_ext = chld_rcd(inst_ext, 'media_id', 'likes')  # with field that has parent rcd ID
    inst_ext = add_time_cntxt(inst_ext)  # no time associated with like/user objects
    new_flds = {}
    for i in xrange(len(inst_ext.rcds)):
        # below, uniqueness might generally be established with media ID + like/user ID,
        # but this model would break down in the case of, e.g., a like/unlike/like scenario,
        # so using time component as part of e_rcd_id, as well...
        rcd = inst_ext.rcds[i]
        rcd['t_rcd_id'] = bld_rcd_id_chld(rcd, 'id')  # temp field
        new_flds['e_rcd_id'] = bld_rcd_id_time(rcd, 't_rcd_id')  # use temp field
        inst_ext.rcds[i].update(new_flds)
        
    log.debug("Instagram media likes enrichment complete.")
    
    return inst_ext

def fb_pg(fb_ext):
    
    log = logging.getLogger(module_nm + '.' + 'fb_pg')

    log.debug("Beginning enrichment of Facebook page info.")
    
    fb_ext = common(fb_ext)
    fb_ext = add_time_cntxt(fb_ext)
    
    new_flds = {}
    for i in xrange(len(fb_ext.rcds)):
        new_flds['e_rcd_id'] = str(fb_ext.rcds[i]['e_eff_epch_ms'])  # e_rcd_id of type str
        fb_ext.rcds[i].update(new_flds)
    
    log.debug("Facebook page info enrichment complete.")
    
    return fb_ext

def fb_post(fb_ext):
    
    log = logging.getLogger(module_nm + '.' + 'fb_post')

    log.debug("Beginning enrichment of Facebook post.")
    
    fb_ext = common(fb_ext)
    
    new_flds = {}
    for i in xrange(len(fb_ext.rcds)):
        new_flds['e_rcd_id'] = fb_ext.rcds[i]['id']
        dttm = dateutil.parser.parse(fb_ext.rcds[i]['created_time'])
        new_flds['e_eff_dttm_ts'] = util.get_sql_ts(dttm)
        new_flds['e_eff_epch_ms'] = util.get_epch_ms(dttm)
        fb_ext.rcds[i].update(new_flds)
    
    log.debug("Facebook post enrichment complete.")
    
    return fb_ext

def fb_usr(fb_ext):
    
    log = logging.getLogger(module_nm + '.fb_usr')

    log.debug("Beginning enrichment of Facebook user.")
    
    fb_ext = common(fb_ext)
    fb_ext = add_time_cntxt(fb_ext)
    
    new_flds = {}
    for i in xrange(len(fb_ext.rcds)):
        new_flds['e_rcd_id'] = bld_rcd_id_time(fb_ext.rcds[i], 'id')
        inst_ext.rcds[i].update(new_flds)
    
    log.debug("Facebook user record enrichment complete.")
    
    return fb_ext

def fb_like(fb_ext):
    
    log = logging.getLogger(module_nm + '.fb_like')

    log.debug("Beginning enrichment of Facebook like records.")
    
    fb_ext = common(fb_ext)
    fb_ext = chld_rcd(fb_ext, 'prnt_id', 'likes')  # with field that has parent rcd ID
    
    fb_ext = add_time_cntxt(fb_ext)
    
    new_flds = {}
    for i in xrange(len(fb_ext.rcds)):
        fb_ext.rcds[i]['e_rcd_id'] = bld_rcd_id_chld(fb_ext.rcds[i], 'id')  # this value changes in next line
        new_flds['e_rcd_id'] = bld_rcd_id_time(fb_ext.rcds[i], 'e_rcd_id')  # build on attr added in previous line
        fb_ext.rcds[i].update(new_flds)
    
    log.debug("Facebook likes record enrichment complete.")

    return fb_ext

def fb_cmnt(fb_ext):
    
    log = logging.getLogger(module_nm + '.fb_cmnt')

    log.debug("Beginning enrichment of Facebook comment records.")
    
    fb_ext = common(fb_ext)
    fb_ext = chld_rcd(fb_ext, 'prnt_id', 'comments')  # with field that has parent rcd ID
    
    new_flds = {}
    for i in xrange(len(fb_ext.rcds)):
        # below, comments always child adapter...so get child external ID...
        # what is the meaning of the comment above?
        dttm = util.dttm_from_str(fb_ext.rcds[i]['created_time'])
        new_flds['e_eff_dttm_ts'] = util.get_sql_ts(dttm)
        new_flds['e_eff_epch_ms'] = util.get_epch_ms(dttm)
        new_flds['e_rcd_id'] = fb_ext.rcds[i]['id']  # unique comment ID
        fb_ext.rcds[i].update(new_flds)
    
    log.debug("Facebook comments record enrichment complete.")

    return fb_ext

def add_time_cntxt(ext):
    # Meant to add time information to records which do not natively contain
    # it; in effect, proc time becomes the time at which the application first
    # detected the record, and serves as that record's effective time
    # Depends on common enrichment first being performed to the record(s)...
    
    log = logging.getLogger(module_nm + '.' + 'add_time_cntxt')
    
    new_flds = {}
    for i in xrange(len(ext.rcds)):
        new_flds['e_eff_dttm_ts'] = util.ts_from_epch_ms(ext.rcds[i]['e_proc_epch_ms'])
        new_flds['e_eff_epch_ms'] = ext.rcds[i]['e_proc_epch_ms']
        ext.rcds[i].update(new_flds)
        
    log.debug("Time contextualization complete.")
    
    return ext
        

    
def get_e_sntmnt(txt_str):
    # Return sentiment (positive/negative) value for given text.
    # Obviously very rudimentary. See http://www.nltk.org/ as possible addition...
    # We can generally expect sentiment to be a float/decimal value in the future.
    
    log = logging.getLogger(module_nm + '.get_e_sntmnt')
    
    new_flds = {'e_sntmnt' : 0.0}
    
    pos = [':)', ':]', ';)', ';]']
    neg = [':(', ':[', ';(', ';[']
    
    pos_fnd = False
    neg_fnd = False
    for tkn in pos:
        if tkn in txt_str:
            pos_fnd = True
    for tkn in neg:
        if tkn in txt_str:
            neg_fnd = True
    
    # decimals chosen for storage compatibility (boto/dynamodb)
    if pos_fnd == True and neg_fnd == True:
        new_flds['e_sntmnt'] = 0.0
        # flags to support aggregation
        new_flds['e_neg_sntmnt'] = 0
        new_flds['e_pos_sntmnt'] = 0
        new_flds['e_neu_sntmnt'] = 1
    elif neg_fnd == False and pos_fnd == True:
        new_flds['e_sntmnt'] = 1.0
        # flags to support aggregation
        new_flds['e_neg_sntmnt'] = 0
        new_flds['e_pos_sntmnt'] = 1
        new_flds['e_neu_sntmnt'] = 0
    elif neg_fnd == True and pos_fnd == False:
        new_flds['e_sntmnt'] = -1.0
        # flags to support aggregation
        new_flds['e_neg_sntmnt'] = 1
        new_flds['e_pos_sntmnt'] = 0
        new_flds['e_neu_sntmnt'] = 0
    else:
        new_flds['e_sntmnt'] = 0.0
        # flags to support aggregation
        new_flds['e_neg_sntmnt'] = 0
        new_flds['e_pos_sntmnt'] = 0
        new_flds['e_neu_sntmnt'] = 1
        
    return new_flds
        
def get_twtr_e_amp(twtr_rcd):
    # Thinking of this as a proprietary measure of the 'amplitude' of the tweet
    # based on fields such as number of retweets, number of user followers, etc.
    # Lots of options here...may at some point want multiple types of measures
    # such as this.
    
    # Going to need a function similar to this, possibly, to breakdown whether
    # tweet returned in search due to text, hashtag, @, etc.
    
    log = logging.getLogger(module_nm + '.get_e_amp')
    
    new_flds = {'e_amp' : 0.0}
    
    # right now, just a couple fields used as example
    new_flds['e_amp'] = twtr_rcd['retweet_count'] * 1 + twtr_rcd['user_followers_count'] * .5
    
    return new_flds
    
def get_e_geo(txt_str):
    # what kind of provisions to put in here for throughput detection?
    # this function is really going to have to be built out, probably, to accommodate
    # things like distributing across different services, exceptions, retries, etc...
    log = logging.getLogger(module_nm + '.get_e_geo')
    
    new_flds = {'e_geo_lat' : 0.0, 'e_geo_long' : 0.0}
    
    return new_flds  # uncomment to bypass geocoding
    if txt_str.strip() == '':  # could probably apply more rigorous checking to reduce frivolous calls
        return new_flds  # nothing to be done
    
    geolocator = Nominatim()
    location = geolocator.geocode(txt_str)
    #Example response:
    #{u'display_name': u'1290, Hathaway Avenue, Lakewood, Cuyahoga County, Ohio, 44107, United States of America',
    #u'importance': 0.521, u'place_id': u'1804579387', u'lon': u'-81.7886933636364', u'lat': u'41.4895401818182',
    #u'licence': u'Data \xa9 OpenStreetMap contributors, ODbL 1.0. http://www.openstreetmap.org/copyright',
    #u'boundingbox': [u'41.489490181818', u'41.489590181818', u'-81.788743363636', u'-81.788643363636'], u'type': u'house', u'class': u'place'}
    
    if location is not None:  # None returned if not found
        new_flds['e_geo_long'] = location.longitude
        new_flds['e_geo_lat'] = location.latitude
        # location.address returns fully qualified address (zip code might be of interest)
        
    return new_flds
    
### Exceptions
#class geopy.exc.GeocoderQuotaExceeded
#The remote geocoding service refused to fulfill the request because the client has used its quota.

#class geopy.exc.GeocoderAuthenticationFailure
#The remote geocoding service rejects the API key or account credentials this geocoder was instantiated with.

#class geopy.exc.GeocoderInsufficientPrivileges
#The remote geocoding service refused to fulfill a request using the account credentials given.

#class geopy.exc.GeocoderTimedOut
#The call to the geocoding service was aborted because no response was receiving within the timeout argument of either the geocoding class or, if specified, the method call. Some services are just consistently slow, and a higher timeout may be needed to use them.

#class geopy.exc.GeocoderUnavailable
#Either it was not possible to establish a connection to the remote geocoding service, or the service responded with a code indicating it was unavailable.

    # get address from coordinates:
    #geolocator = Nominatim()
    #location = geolocator.reverse("52.509669, 13.376294")
    #print (location.address)
    #print ((location.latitude, location.longitude))  # returns actual coords found
    #print (location.raw)
    
    if 'coordinates_long' and 'coordinates_lat' not in twtr_rcd:  # invalid unless both are present
        # here you would put logic that might take other fields (e.g., location)
        # and perform geocoding...for now, though...
        return new_flds  # stay with default/nulls
    else:
        new_flds['e_geo_lat'] = twtr_rcd['coordinates_lat']
        new_flds['e_geo_long'] = twtr_rcd['coordinates_long']
        
    return new_flds

def get_retweet_info(twtr_rcd):
    # Enrichment associated with retweets
    
    log = logging.getLogger(module_nm + '.' + 'enrich.get_retweet_info')
    
    new_flds = {'e_retweet': 0}
    
    if 'retweeted_status_text' in twtr_rcd.keys():  # retrieving this field in parser anytime retweet is seen
        new_flds['e_retweet'] = 1  # this is a retweet
        # below, not a new field, but using original tweet content...will overwrite existing(?)
        new_flds['text'] = twtr_rcd['retweeted_status_text']
        
    return new_flds

def bld_rcd_id_time(rcd, attr_nm):
    # expects entire rcd
    # as well as field to use as key/basis for first part of record ID
    # value for attr_nm must not contain delimiter! (or else convert it here...?)
    # | is forbidden char in content of rcd[attr_nm]
    return str(rcd[attr_nm]) + '|' + str(rcd['e_eff_epch_ms'])
    
def bld_rcd_id_chld(rcd, attr_nm):
    # build ID for record associated with child adapter *where record ID is not unique
    # across adapter*, thus parent ID is prepended to provide uniqueness
    # expects rcd (child adapter, enriched with e_prnt_rcd_id)
    #         attr_nm (key/basis for record ID)
    # value for attr_nm/prnt_rcd_id must not contain delimiter! (or else convert it here...?)
    # ; is forbidden char in content of rcd[attr_nm] or rcd['e_prnt_rcd_id']
    
    return str(rcd['e_prnt_rcd_id']) + ';' + str(rcd[attr_nm])
    