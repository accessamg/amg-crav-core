import logging
import requests
from time import sleep

import util
from external import Web
from internal import NoSql

app_name = 'campaign_monitor'
module_nm = 'externals.' + app_name
module_logger = logging.getLogger(module_nm)

spec = util.get_ext_spec(app_name)

def get_campaign_summary(client_id, campaign_id):
    
    log = logging.getLogger(module_nm + '.get_campaign_summary')
    
    print "Getting summary for campaign ID %s." % campaign_id
    req = Web()
    req.params['url'] = util.insert_values( spec['endpoints']['campaign_summary'], { 'campaign_id', campaign_id } )
    
    client_cfg = util.get_cfg_app(client_id)  # not ideal way of handling this ???
    req.auth = (client_cfg['auth'][app_name]['api_key'], '')
    
    req.do_json_request()
    
    return req.payload['data']

def get_campaigns(client_id):
    
    log = logging.getLogger(module_nm + '.get_campaigns')

    req = Web()
    req.params['url'] = util.insert_values( spec['endpoints']['campaigns'], { 'client_id' : client_id} )
    
    client_cfg = util.get_cfg_app(client_id)  # not ideal way of handling this ???
    req.auth = (client_cfg['auth'][app_name]['api_key'], '')
    
    req.do_json_request()
    
    return req.payload['data']
        
def get_campaign_summaries(client_id):
    
    log = logging.getLogger(module_nm + '.get_campaign_summaries')
    
    ext_id = util.build_ext_id(client_id, app_name)
    
    campaigns = get_campaigns(client_id)
    nosql = NoSql()
    rcds = []  # our collection of summaries
    for i in xrange(len(campaigns)):
        campaign_id = campaigns[i]['CampaignID']
        rcds.append( get_campaign_summary(client_id, campaign_id) )
        sleep(1)
        
    print "%i campaign summaries retrieved for %s." % (len(rcds), ext_id)
    
    nosql.put_rcds('ext', rcds)
