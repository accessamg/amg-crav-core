# Classes and functions facilitating access to applications and systems
# within the application.

from boto.dynamodb2.table import Table
from boto.dynamodb2.items import Item
import sqlite3 as SqlDB
import logging
import os
import shutil
import numpy as np
import json
import platform
import time

import util

module_nm = 'main.internal'
module_logger = logging.getLogger(module_nm)

class NoSql():

    def __init__(self):
        self.conn = util.aws_get_nosql_conn()
        self.tables = self.conn.list_tables()
        self.logger = logging.getLogger(module_nm + '.' + 'NoSql')
        
    def parse_resp(self, rslt_set):
        # Brings NoSQL response into the pardigm of the application, parsing
        # and assigning appropriate data types. Some assumptions are inherent
        # to this function, see in-line comments.
        #
        # Right now, returns result set using *both* attr_nms/attr_vals and
        # attr_arr. This is redundant but in place for now until one is chosen over
        # the other. Array form accommodates numpy computations (faster).
        #
        
        log = logging.getLogger(module_nm + '.NoSql.parse_resp')
        
        log.debug("Parsing NoSql response.")
    
        self.rcds = []  # the new way, list of dicts, one dict per record (flexible structure)

        # At one point, had parsing logic here to use on data that came back...due to varying data
        # structures, am (for now?) passing result objects to downstream functionality as typed by boto API,
        # leaving any further parsing/casting to that downstream functionality.that performed the query.
        # In the future, depending on where we land with data structures in NoSql, could
        # consider handling parsing in one or more functions here...
        
        # For now, though, parse only enough to get and return results from query.
        log.debug("Process results.")
        t_append_rcd = self.rcds.append  # make local
        while True:
            try:
                self.rcds = [ { t_key: rslt[t_key] for t_key in rslt.keys() } for rslt in rslt_set ]
            except Exception as e:  # CORE-78
                log.warning("Exception thrown, will pause and retry: %s." % e)
                time.sleep(3)
                continue
            break
    
        ####  BELOW IS SOME PARSING, ETC., LOGIC THAT USED TO BE PERFORMED HERE...GOING TO LEAVE
        #### FOR NOW IN CASE IT IS RESURRECTED IN KIND OR CONCEPT
        
        # This whole notion of attribute names kind of stinks, as-is. It needs to be passed to the
        # query function as a parameter and enforced there. Then this function here just parses
        # all attributes returned, which can vary from record to record.
        # get attribute names as list of strings
#        log.debug("Process attribute names.")
#        try:  # if attributes were passed
#            list(attrs)  # exception if none passed; precarious? assumes same keys for every record
#        except:  # no specific attributes passed, return all of them for each record
#            for rslt in rslt_set:
#                attrs = rslt.keys()  # all available keys; precarious? assumes same keys for every record
#                break  # just getting attribute names as represented by/in first result
            
#        self.attr_type_ids = util.get_datatypes_app(self.attr_nms)  # stop-gap? better way to handle types b/w python/NoSql
        
        # get attribute values as list of lists
#        log.debug("Process attributes.")      
#        t_append_rcd = self.rcds.append  # make local
#        t = 1
#        uni_attrs = []  # track unicode fields
#        for rslt in rslt_set:
#            # casting below to bring data type back into domain of application...there is expense to this casting...
#            try:  # a one-liner if our data is compliant
##                t_append_rcd(dict((key, util.cast_type(rslt[key], util.get_datatypes_app([key])[0])) for key in rslt.keys()))  # set key/value, cast value based on defined app data type, if any (defaults to string)
#                t_append_rcd(dict(key, rslt[key]) for key in rslt.keys())
#            # otherwise, will need to be a bit more gentle
#            except UnicodeEncodeError:  # not supporting unicode, at the moment...should we move to?
#                this_rcd = {}
#                for key in rslt.keys():  # will need to be gentler with this one
#                    try:
#                        this_rcd[key] = util.cast_type(rslt[key], util.get_datatypes_app([key])[0])
#                    except UnicodeEncodeError:  # we sure will hit this again, once we hit the offending key
#                        if key not in uni_attrs:
#                            log.debug("%s in this record contains unicode, forcing to ascii. Will not report this again for this key for this get." % key)
#                            uni_attrs.append(key)
#                        this_rcd[key] = util.cast_type(rslt[key].encode('ascii','ignore'), util.get_datatypes_app([key])[0])
#                        t_append_rcd(this_rcd)  # add to list of rcds/dicts
            
#            t += 1
#            if t % 10 == 0:
#                log.debug("%i results returned." % t)

        #### ABOVE IS SOME PARSING, ETC., LOGIC THAT USED TO BE PERFORMED HERE...GOING TO LEAVE
        #### FOR NOW IN CASE IT IS RESURRECTED IN KIND OR CONCEPT

        log.debug("Parsing completed. %i results returned." % len(self.rcds))
        
        return self
        
    def get_rcds_btw(self, ext_id, attrs=None, snc_epch_ms=0, unt_epch_ms=None):
        # Expects:
        # ext_id as int, external ID for which records will be retrieved
        # [attrs] as list, attributes to retrieve for each record (by default, will retrieve all)
        # [snc_epch_ms] as int, minimum *e_proc_epch_ms* to retrieve, i.e., get all newer (by default will retrieve all since last retrieval)
        # [unt_epch_ms] as int, maximum *e_proc_epch_ms* to retrieve, i.e., get all newer (by default will retrieve all since last retrieval)
        
        log = logging.getLogger(module_nm + '.NoSql.get_rcds_btw')
        
        log.debug("Performing get to NoSql data store 'ext'")
        
        try:
            attrs = tuple(attrs)  # passed to this function as list, exception if nothing was passed
        except:
            pass  # keep attrs as None by default, returns all attributes from NoSql store
        
        try:  # exception if None passed
            unt_epch_ms = int(unt_epch_ms)
        except:
            log.debug("No valid unt_epch_ms passed, using *now.")
            unt_epch_ms = util.get_epch_ms()  # use *now*
            
        if snc_epch_ms >= unt_epch_ms:  # check for valid b/w times
            log.debug("Attempting to query for records between epoch ms %i and %i, which is invalid/meaningless. Foregoing said query." % (snc_epch_ms, unt_epch_ms))
            self.attr_vals = []
            self.attr_arr = ()
            return self
        
        log.debug("Querying for records with e_proc_epch_ms between %i and %i." % (snc_epch_ms, unt_epch_ms))
        log.debug("In other words, records which arrived between %s and %s." % (util.ts_from_epch_ms(snc_epch_ms), util.ts_from_epch_ms(unt_epch_ms)))

        tbl = Table(util.get_tbl_nm('ext'), connection = self.conn)
        rslt_set = tbl.query_2(
            ext_id__eq = ext_id,
            e_proc_epch_ms__between = (snc_epch_ms, unt_epch_ms - 1),  # -1 because BETWEEN does >=<value>, <=<other_value> (inclusive)
            index = 'ProcEpchMs',
            attributes = attrs
        )
        
        self = NoSql.parse_resp(self, rslt_set)
        
# >>> items = myTable.query_2(
#...     forum_name__eq='Amazon DynamoDB',
#...     subject__beginswith='DynamoDB'
#... )  eq, le, lt, ge, gt, beginswith, between
#        self.results = tbl.query_2(**param_dict)

        log.debug("NoSql get returned %i records." % (len(self.rcds)))
        
        return self
    
    def get_aggs_snc(self, agg_id, attrs=None, snc_epch_ms=0):
        # Expects agg_id as int
        #         [attrs] as list of attributes to pull
        #         [snc_epch_ms] as int
        #
        # This function is intended specifically to query the data store for existing aggregates.
        
        log = logging.getLogger(module_nm + '.NoSql.get_aggs_snc')

        log.debug("Performing get to NoSql data store 'agg'.")
        log.debug("Retrieving aggregates for ID %i with v_floor_epch_ms occuring since %i (%s)." % (agg_id, snc_epch_ms, util.ts_from_epch_ms(snc_epch_ms)))

        tbl = Table(util.get_tbl_nm('agg'), connection = self.conn)
        rslt_set = tbl.query_2(
            agg_id__eq = agg_id,
            v_floor_epch_ms__gte = snc_epch_ms,
            attributes = attrs
        )
        
        self = NoSql.parse_resp(self, rslt_set)  # None because all attributes will be returned

        log.debug("NoSql get returned %i aggregation sets." % len(self.rcds))
        
        return self

    def get_items(self, app_tbl, params):
        # Expects app_tbl as string
        #         key as key conditions map/dict
        #         params as compliant NoSql parameter dict
        
        log = logging.getLogger(module_nm + '.NoSql.get_items')

        log.debug("Performing get to NoSql table %s with parameters: %s." % (app_tbl, str(params)))

        tbl = Table(util.get_tbl_nm(app_tbl), connection = self.conn)
        rslt_set = tbl.query_2(**params)
        
        self = NoSql.parse_resp(self, rslt_set)

        log.debug("NoSql get returned %i items." % len(self.rcds))
        
        return self
        
    def batch_get_rcds(self, app_tbl, keys):  # done lazily, requests only executed if records are accessed
        # keys are list/array of dictionaries of form [{col_nm:col_val}],
        # so [{ 'username' : 'jeff' }, { 'username' : 'kip' }] would perform a query
        # of column 'username', looking for values 'jeff' and 'kip'
        
        log = self.logger
        
        log.debug("Performing batch get to NoSql data store %s." % app_tbl)
        
        tbl = Table(util.get_tbl_nm(app_tbl))
        self.rcds = tbl.batch_get(keys = keys)
            
        log.debug("%i records returned." % len(self.rcds))
            
        return self
    
    def updt_item(self, app_tbl, key, params):
        # Update items already in the data store.
        # lower-level API requires that item/key fields include data types, e.g.:
        # item_data = {'id': {'S': value1}, 'created': {'S': value2}}
        
        log = logging.getLogger(module_nm + '.NoSql.updt_item')
        
        log.debug("TRACE: Updating table %s based on key %s using parameters %s." % (app_tbl, str(key), str(params)))
        
        try:
            self.conn.update_item(  util.get_tbl_nm(app_tbl),
                                key,
                                **params
                             )
        except:
            log.error("Error writing to data store, offending key and params: %s, %s, %s." % (app_tbl, str(key), str(params)))
            raise
        
        log.debug("No errors encountered during update.")
        
        return

    def put_rcds(self, app_tbl, rcds, t_overwrite=True):
        # rcds is list of dictionaries to be inserted OR
        # rcds is numpy array with column labels, all of which will be inserted
        # Defaults to overwriting existing records
        # Note: as of 2015-07-13, dynamo does not support overwrite attribute in BatchTable
        # object, despite a parameter being accepted by boto (boto does nothing with the value
        # passed).
        # This logic can be made more efficient (e.g., batch writes), see boto documentation...
        
        log = logging.getLogger(module_nm + '.NoSql.put_rcds')
        
        if len(rcds) == 0:
            log.debug("Nothing to do, no records passed.")
            return
        
        # below, probably want to dispense with array handling here...instead require the
        # conversion in upstream functionality...
        try:  # rcds passed as np.array with valid 'column' labels
            rcds = [dict(zip(rcds.dtype.names, rcd)) for rcd in rcds.tolist()]  # convert numpy array to dict with NoSql-compatible data types (Python)
            log.debug("Records translated from numpy array to list.")
        except:  # assume is already list of records
            pass
        
        log.debug("Performing put to NoSql data store %s." % app_tbl)
    
        # Right now, relying on upstream components to deliver valid data...
        # Wonder if any checks should be instituted here? If so, might want to think about multiple
        # functions, e.g., 'put_agg_rcds', that can contain parsing logic/validation specific to the
        # data structure being handled. Whether such functions would live in the 'internal' module or
        # other functional modules....(?)...
        dupe_cnt = 0  # how many dupes are detected
        tbl = Table(util.get_tbl_nm(app_tbl), connection = self.conn)  # table object
        for rcd in rcds:
#            log.debug("Writing the following record to NoSql table %s: %s" % (app_tbl, str(rcd)))  # quite verbose
#            print "Writing the following record to NoSql table %s: %s" % (app_tbl, str(rcd))  # quite verbose
            # below should be broken out into put_items() function, also referenced by write_cache
            try:
                tbl.put_item(data=rcd, overwrite=t_overwrite)
            # below, am assuming condition check is based on primary key....
#            except ConditionalCheckFailedException:  # already exists, and we don't want to overwrite...
#                dupe_cnt += 1
            except Exception as e:
                log.error("Error writing item to NoSqL store, raising error. Offending item: %s" % str(rcd))
                print str(rcd)
                print e
                raise
#            except ValidationException:  # handle wonkiness...may have hit upon an empty string value... # also finding NaNs from aggs...bah
#                log.debug(rcd)
#                print rcd
#                raise

        # below, might have to go this route to control overwrites...
        # also, 'expected' is deprecated...see ConditionExpression instead....
#        else:
#            for rcd in rcds:
#                try:
#                    rsp = self.conn.put_item(
#                                       util.get_tbl_nm(app_tbl),
#                                       item = rcd,
#                                       expected = { 'ext_id' : { 'Exists': False },  # hard-coded 'ext' primary key
#                                                    'e_rcd_id' : { 'Exists': False }
#                                       },
#                                       return_values = 'ALL_OLD'
#                                       )
#                except:
#                    log.error("Error writing item to NoSqL store, raising error. Offending item: %s" % str(rcd))
#                    print str(rcd)
#                    raise
                
#                if 'Attributes' in rsp:  # check if item already existed
#                    dupe_cnt += 1
            
        log.debug("%i records submitted resulted in %i items written to data store." % (len(rcds), len(rcds) - dupe_cnt))
        log.debug("Here's an example of the records written; the first one, actually: %s." % str(rcds[0]))
            
        return self
    
    def rfrsh_rcds(self, app_tbl, rcds, chk_del_rcds=False):
        # Expects:      app_tbl (str), env-agnostic name of table
        #               rcds (list), list of dicts serving as basis of refresh
        #               chk_del_rcds (bool), detect/mark remotely deleted records (e_deleted)
        # This function only adds those records whose keys (rcd_id) do not already exist
        # in data store; i.e., it will not overwrite existing records. Optionally, it also
        # sequentially (**CONSISTENT ORDERING OF RECORDS IS CRUCIAL**) compares existing
        # versus new records in an attempt to identify those that have been deleted in the
        # remote application (e_deleted).
        
        log = logging.getLogger(module_nm + '.NoSql.rfrsh_rcds')
        
        if len(rcds) == 0:  # check
            log.debug("No records submitted, nothing to do.")
            return

        # below, probably need to get away from hashkey of <proc>_id and just go with 'id'
        # or 'proc_id' in all proc tables
        h_key = app_tbl + '_id'  # hashkey (might want to just declare this function 'ext' only)
        h_key_cond = h_key + '__eq'
        proc_id = rcds[0][h_key]  # every record has proc ID in it, so pull from first
        
        # flag whether this is a child adapter invoking this function
        # Could have secondary index on e_prnt_rcd_id, but then would need dummy value in that attributes for non-child adapters...
        # also wondering about multi-part range key...
        if 'e_prnt_rcd_id' in rcds[0]:  # this is a child adapter, records for which are submitted one parent at a time (inefficient)
            chld = True
            log.debug("Performing refresh of %s, child adapter ID %i, parent record %s." % (app_tbl, proc_id, rcds[0]['e_prnt_rcd_id']))
        else:
            chld = False
            log.debug("Performing refresh of %s, ID %i." % (app_tbl, proc_id))
        
        # Remote records in this batch that are not found at all in data store constitute new records.
        # We will collect these and treat them later as simple appends.
        new_rcds = []

        # sort remote records into our expected/default order 
        # does imply any records w/ same e_proc_epch_ms are in same batch (prolly safe)
        rcds.sort(key=lambda x: (x['e_proc_epch_ms'], x['e_rcd_seq']))
        log.debug("%i records from remote sorted in preparation for data store search." % len(rcds))
        
        # First record in remote set constitutes beginning of interesting set in data store - i.e., a point
        # of refererence as to where to start our refresh - so let's find it. The following two parameters
        # will remain the same during our search.
        params =        {
                        h_key_cond : proc_id,  # this proc ID
                        'attributes' : ('e_rcd_id', 'e_proc_epch_ms', 'e_rcd_seq')
                        }
        # Below, accommodating scenario where first record in remote set is not found; assume it is an
        # entirely new record and check next remote record. In practice, unless there is a breakdown
        # in assumptions in this function, I do not believe a subsequent record should be found in the
        # data store if the first one is not.
        log.debug("Checking existing child records for refresh point of reference for this parent.")
        # done one at a time below; the first time a child adapter runs, it means checking each
        # remote record one at a time, a call to dynamo for each; may want to update this to pull
        # a single query to pull all children associated with parent...
        while rcds:  # as long as we have remote records to check
            # account for time context
            if '|' in rcds[0]['e_rcd_id']:  # hard-coded rcd_id delim (time context)
                log.debug("rcd_id contains time context, using __beginswith to query data store for %s." % (rcds[0]['e_rcd_id'].split('|')[0] + '|'))
                params['e_rcd_id__beginswith'] = rcds[0]['e_rcd_id'].split('|')[0] + '|'  # check for first one in data store
            # let's use equal-to, otherwise (not sure how much it really matters)
            else:
                log.debug("No time context to rcd_id, using __eq to query data store for %s." % rcds[0]['e_rcd_id'])
                params['e_rcd_id__eq'] = rcds[0]['e_rcd_id']  # check for first one in data store
            self.get_items(app_tbl, params)
            if self.rcds:  # found it
                log.debug("Found record id %s in data store to use as refresh point of reference, was at index %i in list of remote records." % (self.rcds[0]['e_rcd_id'], len(new_rcds)))
                break  # done, found our refresh point of reference
            else:  # not found this time, treat as new record
                new_rcds.append(rcds.pop(0))
        log.debug("%i new records detected during search for refresh point of reference." % len(new_rcds))
        
        if len(self.rcds) > 1:
            log.warning('''Multiple points of reference found for refresh; i.e., records of same entity ID portion of record ID but with different time context.
                            This implies: a) refresh is not appropriate for this adapter (i.e., multiple Type II data); b) there is a data problem that caused two
                            or more of the same entity ID (record ID minus time context) to exist in a data set where they should not (perhaps an e_delete was missed?).
                            Remediation logic could be applied here, but for now am exiting this function without performing any updates to the data store.''')
            return
        
        # If no rcd_ids from remote list are found, all records are new.
        if not self.rcds:  # no refresh point of reference found, no refresh to do
            log.debug("No refresh point of reference found in data store, submitting all %i remote records as new." % len(new_rcds))
            self.put_rcds(app_tbl, new_rcds)
            return
        
        ref_rcd_id = self.rcds[0]['e_rcd_id']  # save it off
        
        # Use record found as point of beginning for query to data store, get all records since
        params =        {
                        h_key_cond : proc_id,  # all items associated with this proc ID
                        'index' : 'ProcEpchMs',  # get via proc time
                        'e_proc_epch_ms__gte' : self.rcds[0]['e_proc_epch_ms'],  # records processed since (and including) our refresh point of reference
                        # below, 'query_filter' is being deprecated in lieu of 'filter_expression'...?
                        # condition expressions: http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.SpecifyingConditions.html
                        'query_filter' :        {
                                                'e_deleted__null' : True  # hard-coded 'e_deleted', only items that are NOT virtually deleted
                                                },
                        # below, 'attributes' tuple deprecated in lieu of 'projection_expression' string...?
                        'attributes' : ('e_rcd_id', 'e_proc_epch_ms', 'e_rcd_seq')  # record ID to check existence, epch_ms and seq for ordering (chk_del_rcds)
                        }
        # The below provision is included to handle the refresh of child adapters. Each invocation of this function
        # is limited to child records in the context of a single parent. The below is potentially very expensive,
        # though, from a provisioned throughput standpoint. Though the filter expression returns only child records
        # associated with the particular parent, the query for *all* child records (for *all* parents) still goes
        # against provisioned throughput. This should probably be remedied, or at the least refresh should be considered
        # a relatively expensive operation. To remedy, something must be available in the way of an index/key that can
        # be queried directly against in order to limit results...
        # Could have secondary index on e_prnt_rcd_id, but then would need dummy value in that attributes for non-child adapters...
        if chld:  # this is a child adapter, which is submitted in the context of one parent at a time
            log.debug("This query is for a child adapter, filtering to include only children records to parent ID %s." % rcds[0]['e_prnt_rcd_id'])
            params['query_filter']['e_prnt_rcd_id__eq'] = rcds[0]['e_prnt_rcd_id']  # add filter param, child records for this parent only
        self.get_items(app_tbl, params)
        self.rcds.sort(key=lambda x: (x['e_proc_epch_ms'], x['e_rcd_seq']))
        while self.rcds:
            if self.rcds[0]['e_rcd_id'] != ref_rcd_id:  # e_proc_epch_ms (precision) search could have picked up undesirables
                self.rcds.pop(0)  # prune until ref_rcd_id is first one
            else:
                break
            
        log.debug("%i eligible records retrieved from data store to compare to remote set." % len(self.rcds))

        # below, hard-coded rcd_id delimiter '|' (enriched time context) assumes first
        # component as key (for children, *could be* parent rcd_id + chld_attr_val);
        # those records with no delimiter in key will be unaffected
        ds_keys = [ rcd['e_rcd_id'].split('|')[0] for rcd in self.rcds ]  # keys from data store
        # below, pop off any truly *new* records into list of *new* records
        t_append = new_rcds.append  # make local
        for i in reversed(xrange(len(rcds))):  # through all our records returned from remote
            if rcds[i]['e_rcd_id'].split('|')[0] not in ds_keys:
                t_append(rcds.pop(i))

        log.debug("%i new records detected in remote set out of a total of %i." % (len(new_rcds), len(rcds) + len(new_rcds)))

        # *** The chk_del_rcds portion of this function relies HEAVILY on the assumption that records
        # are returned in a consistent order from the remote system, ordering that can be consistently
        # (across calls) achieved by sorting primary:e_proc_epch_ms, secondary:e_rcd_seq...***

        # checking for records which may have been deleted in remote application
        # perhaps break this out into discrete function
        if chk_del_rcds:
            log.debug("Performing additional check for records potentially deleted in remote application.")
            self.rcds.sort(key=lambda x: (x['e_proc_epch_ms'], x['e_rcd_seq']))  # sort records pulled from data store
            rmt_keys = [ rcd['e_rcd_id'].split('|')[0] for rcd in rcds ]  # keys from remote set
            log.debug("List of remote keys (~=rcd_id) to compare: %s" % str(rmt_keys))
            log.debug("List of data store keys (~=rcd_id) to compare: %s" % (str( [ rcd['e_rcd_id'] for rcd in self.rcds ] )))
            # now step through set of records from data store and check rcd_id against list of remote
            # keys; those that are missing imply they have been deleted in remote app...
            e_del_rcds = []  # hold records to be e_deleted
            t_append = e_del_rcds.append  # make local
            for rcd in self.rcds:
                if not rmt_keys:  # if we ran out of remote keys
                    break
                if rcd['e_rcd_id'].split('|')[0] != rmt_keys.pop(0):  # hard-coded time context delim
                    t_append(rcd)  # not found where expected in remote set: e_deleted
            # below line is simplified but not as thorough...may serve as adequate fall back??
#            updt_rcds = [ rcd for rcd in self.rcds if rcd['e_rcd_id'].split('|')[0] not in rmt_rcd_ids ]  # rcds needing updating as 'e_deleted'
            log.debug("List of records to be marked e_deleted: %s" % str(e_del_rcds))
            for rcd in e_del_rcds:
                # below, update_attributes deprecated in lieu of update_expression
                params = {}
                params['update_expression'] = 'SET e_deleted = :epch_ms'
                params['expression_attribute_values'] = {':epch_ms': { 'N': str(util.get_epch_ms()) } }
                # lower-level API requires that item/key fields include data types, e.g.:
                # item_data = { 'id': {'S': value1}, 'created': {'S': value2} }
                # also, even 'N' values must be submitted as strings...(proc_id)...
                self.updt_item(app_tbl,
                                {       'e_rcd_id' : { 'S' : rcd['e_rcd_id'] },  # keeping same rcd_id
                                        h_key : { 'N' : str(proc_id) }
                                },
                                params)
            log.debug("No problems encountered while submitted records as updates.")
            
        self.put_rcds(app_tbl, new_rcds)  # don't forget to write our new records
        
        log.debug("Refresh work completed.")
    
    # def parallel_get_records(self, query):
        # provides opportunity for faster table scans, if needed...
        # bit more code required beyond 'standard' reads
        
    def purge_old_rcds(self, id, age):
        # Purges items from external table according to parameters passed
        # See doc for supporting function purge_proc_items()
        
        log = logging.getLogger(module_nm + '.NoSql.purge_old_rcds')
        
        return NoSql.purge_proc_items(self, 'ext', id, age)

    def purge_old_aggs(self, id, age):
        # Purges items from aggregation table according to parameters passed
        # See doc for supporting function purge_proc_items()
        
        log = logging.getLogger(module_nm + '.NoSql.purge_old_rcds')

        return NoSql.purge_proc_items(self, 'agg', id, age)

    def purge_proc_items(self, app_tbl, id, age):
        # Purges items from process tables according to parameters passed,
        # supports proc-specific purge functions
        # app_tbl (str) - application table to purge (e.g., 'ext', 'agg')
        # id (int) - process ID (ext/agg)
        # [age] (int) - threshold, in milliseconds, beyond which items will be purged (default: 6 months)
        
        # Some redundancy between this function and get_rcds_btw(), but wanted to
        # minimize parsing for purposes of deletion.
        
        log = logging.getLogger(module_nm + '.NoSql.purge_old_proc_items')
        
        if app_tbl not in util.vld_procs:
            log.warning("%s not a valid process table, ignoring request to purge." % str(app_tbl))
            return "%s is not a valid process table, ignoring request to purge." % str(app_tbl)
            
        try:
            id = int(id)
        except:
            return "ID %s is not an integer, and must be." % str(id)

        try:  # exception if None passed
            age = int(age)
        except:
            log.debug("No valid age passed, defaulting to age of 6 months (15552000000).")
            age = 15552000000  # hard-coded, arbitrary default of 6 months
            
        min_epoch_ms = util.get_epch_ms() - age
        
        log.debug("Purging items for %s %i, anything older than age of %i, occuring prior to %i (%s)." % (app_tbl, id, age, min_epoch_ms, util.ts_from_epch_ms(min_epoch_ms)))

        tbl = Table(util.get_tbl_nm(app_tbl), connection = self.conn)

        log.debug("Performing get to NoSql data store %s." % app_tbl)
        
        # yeah, a little weird below as this would optimally reside in the proc-specific
        # functions, but I like the idea of having this supporting function...will see where
        # this goes in the future; probably good to make as common as possible across tables
        # the hash/range keys...
        r_params = {}  # query (read) parameters
        if app_tbl == 'ext':
            r_params['attributes'] = ['e_rcd_id']  # hard-coded, only return attribute we will use to perform actual delete
            r_params['ext_id__eq'] = id
            r_params['e_proc_epch_ms__between'] = (0, min_epoch_ms - 1)  # -1 because BETWEEN does >=<value>, <=<other_value> (inclusive)
            r_params['index'] = 'ProcEpchMs'  # using secondary global index for this table
        if app_tbl == 'agg':
            r_params['attributes'] = ['v_floor_epch_ms']  # hard-coded, only return attribute we will use to perform actual delete
            r_params['agg_id__eq'] = id
            r_params['v_floor_epch_ms__between'] = (0, min_epoch_ms - 1)  # -1 because BETWEEN does >=<value>, <=<other_value> (inclusive)
        
        # below, not using utility get function in order to eliminate overhead of parsing
        # result set into Python object (list)...invoking directly...
        rslt_set = tbl.query_2(**r_params)

        log.debug("Deleting all items returned in result set.")
        
        n = 0
        d_params = {}  # query (delete) parameters
        d_params[app_tbl + '_id'] = id  # hard-coded _id suffix...should probably just go with 'id' as key name in proc tables...
        for rslt in rslt_set:
            n += 1
            # below, use attribute name/value from read params
            # am wondering if below could be optimized...
            d_params[r_params['attributes'][0]] = rslt[r_params['attributes'][0]]
#            print r_params['attributes'][0]
#            print rslt[r_params['attributes'][0]]
            tbl.delete_item(**d_params)
        
        log.debug("Completed. %i records deleted." % n)
        
        return 'Success. %i records deleted.' % n


#In Layer2, it's a bit simpler.  If t is a Table object:
#>>> from boto.dynamodb.condition import GT
#>>> t.query('mitch', GT(100))
#A Condition object. Condition object can be one of the following types:
#EQ - equal (1)
#NE - not equal (1)
#LE - less than or equal (1)
#LT - less than (1)
#GE - greater than or equal (1)
#GT - greater than (1)
#NOT_NULL - attribute exists (0, use None)
#NULL - attribute does not exist (0, use None)
#CONTAINS - substring or value in list (1)
#NOT_CONTAINS - absence of substring or value in list (1)
#BEGINS_WITH - substring prefix (1)
#IN - exact match in list (N)
#BETWEEN - >= first value, <= second value (2)
    
# SQL not used, at the moment.
class Sql():
    
    def __init__(self):
        app_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
        sql_path = os.path.join(os.path.join(app_path,'data'),'sqlite-store')
        self.conn = SqlDB.connect(sql_path)  # hard-coded
        self.conn.row_factory = SqlDB.Row  # return rows as dicts instead of tuples
        self.c = self.conn.cursor()
        self.c.execute("SELECT name FROM sqlite_master WHERE type='table';")
        self.tables = self.c.fetchall()
        self.commit = True  # flag indicating whether transaction should be commited
        self.close = True  # flag indicating whether connection should be closed
        self.logger = logging.getLogger(module_nm + '.' + 'Sql')
    
    def get_rows(self, query, tbl_nm=''):
        
        log = self.logger
        
        log.debug("Performing get to Sql data table %s." % tbl_nm)

        self.c.execute(query)
        self.results = self.c.fetchall()
        self.col_nms = []  # provide column names too
        append_col = self.col_nms.append  # make local
        for i in range(len(self.c.description)):
            append_col(self.c.description[i][0])  # first element is column name
            
        rslt_cnt = len(self.results)  # save off result count for use in log statement below
        
        if self.close == True:
            self.conn.close()
        
        log.debug("%i rows returned." % rslt_cnt)
        
        return self
    
    def put_rows(self, updts, tbl_nm=''):
        # updts is list of valid sql update strings
        # two other optional params to add(?): commit_tx / close_conn (default True)
        
        log = self.logger
        
        log.debug("Performing put to Sql data table %s." % tbl_nm)
        
        num_rows_strt = self.conn.total_changes  # number of rows modified by this connection object since being opened
        
        for updt in updts:
            self.c.execute(updt)
            
        if self.commit == True:
            num_rows_updtd = self.conn.total_changes - num_rows_strt
            self.conn.commit()
        
        if self.close == True:
            if self.commit != True:
                log.warning("Attempting to close database connection prior to transaction commit to table %s, going to commit transaction to database first." % tbl_nm)
                self.commit = True
                self.conn.commit()
            self.conn.close()
            
        if self.commit == True:
            log.debug("%i rows updated." % (num_rows_updtd))
        else:
            log.debug("Row updates pending transaction commit.")
        
        return self
    
    #def batch_get_rows(self):
        # something

# Part of me likes the idea of this discrete module for classes/functions dealing with
# internal application access. Relying on it for base operations has proved a bit of
# a challenge, though, around circular module import. One way to do this might be to
# have a discrete module (e.g., 'conn') for handling the establishment of base
# connections with internal application stores/services. For now, though those
# base functions reside in 'util', which should be considered a fundamental module,
# imported by other modules, but generally not vise-versa.
# So right now, the below are really just pass-through functions....not sure if 
# we will want to adust this.
        
def write_cache(cache_pairs={}):
    
    util.write_cache(cache_pairs)
    
def read_cache(keys=[]):
    
    return util.read_cache(keys)


# Below, considered rewriting cache logic into new class...
        
#class Cache(cache_nm='main'):
#    # For accessing memory cache for ad hoc storage/retrieval of key/value pairs.
    
#    def __init__(self, cache_nm):
#        self.fn = cache_nm + '.tmp'
#        self.path = '../cache'  # hard-coded path
#        self.f_cache = shelve.open(os.path.join(self.path, self.fn))
#        self.logger = logging.getLogger(module_nm + '.Cache')
    
#    def get(self, keys=[]):
#        # Accepts list of keys and performs lookup to cache, returning a dictionary
#        # containing key names as well as cached values associated with those keys. If
#        # no entry is found for a key, None is returned.
        
#        log = logging.getLogger(module_nm + '.Cache.get')
        
#        self.results = {}  # key/value pairs retrieved during read
        
#        for key in keys:
#            try:
#                self.results[key] = self.f_cache[key]
#            except KeyError:
#                log.warning("Cached value not available for %s in %s, populating with default (None) and continuing." % (key, str(os.path.join(self.path, self.fn))))
#                self.results[key] = None
#        self.f_cache.close()
        
#        return self

#    def put(self, pairs={}):
#        # Accepts dictionary of key:value pairs. An entry is written (or existing
#        # entry updated, if the key already exists) to the cache for each key using its
#        # associated value, for future retrieval.
        
#        log = logging.getLogger(module_nm + '.Cache.put')  # use sparingly, as this is intended for now to serve as quasi-fast cache

#        for key in pairs:
#            try:
#                self.f_cache[key] = pairs[key]
#                log.debug("Wrote to cache attribute %s with value of %s." % (key, str(self.f_cache[key])))  # ***SHOULD BE COMMENTED EXCEPT FOR SPECIFIC DEBUGGING PURPOSES***
#            except:
#                raise IOError("Cache %s could not be written to while attempting to update attribute %s. Is write access allowed?" % (str(os.path.join(self.path, self.fn)), key))
        
#        self.f_cache.close()
        
#        return 0