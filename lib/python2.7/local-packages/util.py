import logging
import os
from pprint import pprint
import time
import random
import string
import errno
import sqlite3
import datetime
import pytz
import urllib
import decimal
import collections
from decimal import Decimal
from ConfigParser import SafeConfigParser
import platform
from sys import platform as _platform
import shelve
import calendar
import json
import dateutil.parser
from urlparse import urlparse

import boto.dynamodb2
from boto.dynamodb2.table import Table
from boto.dynamodb2.items import Item
from boto.dynamodb2.layer1 import DynamoDBConnection as NoSqlDB
from boto import utils as butils
from boto import ec2

module_nm = 'main.util'
moduleLogger = logging.getLogger(module_nm)

# Two main sections to this module:
# 1. Utility functions
# 2. Set application globals and establish application configuration

this_tz = 'US/Eastern'  # Enigma standard, hard-coded timezone for now
tz_offset = datetime.datetime.now(pytz.timezone(this_tz)).strftime('%z')  # string
# Below, let's get our timezone offset in milliseconds
tz_offset_dir = tz_offset[0]  # positive/negative
tz_offset_hours = int(tz_offset[1:3])
tz_offset_mins = int(tz_offset[3:])
tz_offset_ms = ((tz_offset_hours * 60) + tz_offset_mins) * 60 * 1000
if tz_offset_dir == '-':
    tz_offset_ms = -tz_offset_ms
    
dttm_fmt = '%Y-%m-%d %H:%M:%S.%L'
vld_cfg_aspects = ['main', 'auth', 'ext', 'agg']
vld_procs = ['ext', 'agg']  # hard-coded valid process types (this is prolly fine)
nosql_tbls = vld_procs + ['metrics_main', 'metrics_ext', 'metrics_agg', 'metrics_web', 'ops', 'cfg', 'ref']
counter = 0  # will increment in main...

##### Utility functions #####

def get_cfg_sctn_map(cfg_obj, sctn):
    # Expects:
    #   cfg_obj (Python configuration object)
    #   sctn (string) - section to be parsed in Config object
    
    log = logging.getLogger(module_nm + '.get_cfg_sctn_map')
    
    cfg = {}
    opts = cfg_obj.options(sctn)
    for opt in opts:
        try:
            cfg[opt] = cfg_obj.get(sctn, opt)
            if cfg[opt] == -1:
                DebugPrint("skip: %s" % opt)
        except:
            print("exception on %s!" % opt)
            cfg[opt] = None
            
    return cfg

def get_tbl_nm(app_tbl):
    # Converts table name string (e.g., 'ext') to operational name of table, which
    # varies between - and based on - environment
    cfg = get_cfg_app()
    cfg_env = cfg['env']
    return cfg_env['id'] + '-' + app_tbl  # hyphen delim

def aws_get_ec2_id():
    # Retrieve ID for instance from which this function is called
    
    meta = butils.get_instance_metadata()
    return meta['instance-id']
    
def aws_get_ec2_env(cfg_env=None):
    # Retrieve environment string for EC2 instance from which this function is called
    # e.g., 'sb', 'dev', 'prod'

    log = logging.getLogger(module_nm + '.aws_get_ec2_env')

    # Below, there's a chance we can't get environment config from the data store; e.g., 
    # if this is during environment config load, in which case a cfg_env object
    # (presumably based on env.cfg) can be passed.
    if cfg_env is None:  # override module global
        cfg = get_cfg_app()
        cfg_env = cfg['env']
    
    ec2_id = aws_get_ec2_id()
    conn = ec2.connection.EC2Connection(cfg_env['aws_access_key_id'], cfg_env['aws_secret_access_key'])  # hard-coded params
    reservations = conn.get_all_instances()
    try:
        for rez in reservations:
            for inst in rez.instances:
                if inst.id == ec2_id:
                    return inst.tags['env']  # env string is the value to this key
    except:
        log.error("Unable to retrieve ec2 instance environment, aborting.")
        print "Unable to retrieve ec2 instance environment, aborting."
        raise

def load_cfg_env():
    # Retrieves configuration for *this* environment from file
    log = logging.getLogger(module_nm + '.load_cfg_env')
    
    log.info("Loading environment configuration from file.")
    # bring in env config (distinction between this and 'main' config could be fleshed out, perhaps)
    cfg_env = {}
    env_cfg = SafeConfigParser()
    env_cfg.read('../../../config/env.cfg')  # hard-coded relative path
    glbl_opts = get_cfg_sctn_map(env_cfg, 'global')  # hard-coded section name for globals
    for opt in glbl_opts.keys():  # load our globals
        cfg_env[opt.lower()] = glbl_opts[opt]  # all lowercase

    if not is_aws:  # are we NOT in the AWS cloud; i.e., are we local?
        cfg_env['id'] = 'local'
    else:  # bit more work to do to determine environment...
        cfg_env['id'] = aws_get_ec2_env(cfg_env)
    
    env_opts = get_cfg_sctn_map(env_cfg, cfg_env['id'])
    for opt in env_opts.keys():  # load env options: will overwrite globals in cases of intersection
        cfg_env[opt.lower()] = env_opts[opt]
        
    return cfg_env

def load_cfg_app(client_id, from_file=False):
    # Retrieves configuration for application from:
    #   - data store, if available
    #   - seed file, if from_file == true (default: False)
    
    log = logging.getLogger(module_nm + '.load_cfg_app')
    global cfg_app  # our cfg object
    cfg_app = {}  # our parent
    cfg_app['tz_offset'] = tz_offset
    cfg_app['tz_offset_ms'] = tz_offset_ms
    cfg_app['this_tz'] = this_tz
    cfg_app['dttm_fmt'] = dttm_fmt
    cfg_app['env'] = load_cfg_env()  # environment config (from file)
    cfg_app['client_id'] = client_id
    
    log.info("Loading application configuration for client %s." % client_id)
    
    if not from_file:  # normal load from data store
        # First, let's see if we can get config from data store (nominal)
        params =    {   'limit': 1,  # just want most recent (i.e., active)
                        'client_id__eq': client_id,
                        'e_eff_epch_ms__lte': get_epch_ms(),  # get now (epoch ms)
                        'reverse': True
                    }

        conn = aws_get_nosql_conn()
        try:  # from data store
            tbl = Table(get_tbl_nm('cfg'), connection = conn)
            print "Querying data store with params: %s" % str(params)
            rslt_set = tbl.query_2(**params)
            # update config object with dicts in single item retrieved
            cfg = cfg_app.copy()
            cnt = 0
            for rslt in rslt_set:
                cnt += 1
                cfg.update({str(key): repl_deci(rslt[key]) for key in rslt.keys()})
                break  # for now, will be only one
            if cnt == 0:
                print "No configuration available for client ID %i, stopping processing." % client_id
                raise
        except:  # possibly from file
            if client_id == 0:  # if this is our dev-only client ID
                print "Problem loading from data store, reverting to file."
                log.warning("Problem retrieving configuration for client ID %i, reverting to file." % client_id)
                from_file = True  # revert to file
            else:
                log.error("Problem retrieving configuration for client ID %i, aborting." % client_id)
                raise

    # either by param or due to failure in if clause above
    # (overwrites whatever is already in memory)
    if from_file:  
        print "Loading configuration from file for client %s." % client_id
        log.warning("Loading configuration from file.")
#        from cfg_seed import cfg
#        cfg = dict(cfg)
        filename = '../../../config/clients/' + client_id + '.json'
        with open(filename, 'r') as file:
            file_data = file.read()
        cfg = json.loads(file_data)
        t_cfg = cfg_app.copy()
        t_cfg.update(cfg)

        cfg = t_cfg

    cfg_app = cfg  # back where we expect it (tempted to go with just 'cfg')

    # Some ugly casting below, need to handle at the Dynamo interface, possibly?
    # There are some broader questions around data types...
    for key in cfg_app.keys():  # find ID keys, cast to int
        try:  # for main/ext/agg/etc...may not be 'id' key...
            for id in cfg_app[key]['id'].keys():
                cfg_app[key]['id'][int(id)] = cfg_app[key]['id'][id]  # save off with int ID
                if not isinstance(id, int):  # means id was cast from something into int
                    cfg_app[key]['id'].pop(id, None)  # get rid of non-int key that we cast...
                for attr_nm, attr_val in cfg_app[key]['id'][int(id)].iteritems():
                    if isinstance(attr_val, Decimal):
                        if float(attr_val).is_integer():
                            cfg_app[key]['id'][int(id)][attr_nm] = int(cfg_app[key]['id'][int(id)][attr_nm])
        except:
            continue
        try:  # for main/ext/agg/etc...may not be 'type' key...
            for type in cfg_app[key]['type']:
                cfg_app[key]['type'][type] = [int(id) for id in cfg_app[key]['type'][type]]
        except:
            continue
    
    print "Loaded the following configuration:"
    pprint(cfg_app)
    log.debug("Loaded the following configuration for client ID %s: %s." % (client_id, str(cfg_app)))

def get_metrics(max_num=1):
    # will return max_num historical instances of metrics for each aspect main and procs
    
    log = logging.getLogger(module_nm + '.get_metrics')

    cfg = get_cfg_app()
    
    try:
        max_num = int(max_num)
    except:
        log.warning( "max_num of %s specified for client ID %i, which is invalid...must be an integer. Defaulting to 1." % (str(max_num), cfg['client_id']) )
        max_num = 1
    
    metrics = {}
    for proc in vld_procs:
        metrics[proc] = get_metrics_proc(proc, max_num)
    metrics['main'] = get_metrics_main(max_num)
    
    log.debug("Returning following metrics: %s" % str(metrics))

    return metrics  # right now, converting away from decimals (for one, can't cast them to JSON)

def get_metrics_main(max_num=1):
    # get most recent main app metrics, last 'num entries for each process
    
    log = logging.getLogger(module_nm + '.get_metrics_main')
    
    cfg = get_cfg_app()
    client_id = cfg['client_id']

    try:
        max_num = int(max_num)
    except:
        log.warning( "max_num of %s specified for client ID %i, which is invalid...must be an integer. Defaulting to 1." % (str(max_num), client_id) )
        max_num = 1
        
        log.info("Querying metrics_main for last %i entries." % (proc, max_num))

    params =    {   'limit': max_num,  # just want most recent (i.e., active)
                    'client_id__eq': client_id,
                    'proc_end_epch_ms__lte': get_epch_ms(),  # get now (epoch ms)
                    'reverse': True
                }
    
    metrics_main = aws_get_nosql_items(get_tbl_nm('metrics_main'), params)
    
    log.debug("Returning following metrics: %s" % str(metrics_main))

    return { 'main': metrics_main }

def get_metrics_ext(max_num=1, ids=[]):
    
    return get_metrics_proc('ext', ids, max_num)

def get_metrics_agg(max_num=1, ids=[]):
    
    return get_metrics_proc('agg', ids, max_num)

def get_metrics_proc(proc, max_num=1, ids=[]):
    # get metrics for processing aspect of application
    
    log = logging.getLogger(module_nm + '.get_metrics_proc')
    
    if proc not in vld_procs:  # referencing a global
        log.warning("%s is not a valid process type right now, only %s are. Skipping request." % (str(proc), str(vld_procs)))
        return "Invalid process type " + proc + " specified, must be one of " + str(vld_procs)
        
    # tap our global var
    if len(ids) == 0:  # return all
        ids = [ id for id in cfg_app[proc]['id'].keys() ]
        
    metrics_proc = {}
    for id in ids:
        try:
            id = int(id)
        except:
            log.warning("ID with a value of %s was submitted, which is invalid. ID must be an integer. Skipping this ID." % str(id))
            continue
        if id not in cfg_app[proc]['id'].keys():
            log.warning("Requesting configuration for %s id %i, which does not exist. Skipping this ID." % (proc, id))
            continue
        try:
            max_num = int(max_num)
        except:
            log.warning("max_num of %s specified for %s ID %i, which is invalid...must be an integer. Defaulting to 1." % (str(max_num), proc, id))
            max_num = 1
        
        log.info("Querying %s for last %i entries for ID %i." % (proc, max_num, id))
    
        if proc == 'agg':
            app_tbl = 'metrics_agg'
            key = 'agg_id__eq'
        if proc == 'ext':
            app_tbl = 'metrics_ext'
            key = 'ext_id__eq'

        params =    {   'limit': max_num,  # just want most recent (i.e., active)
                        key : id,
                        'proc_end_epch_ms__lte' : get_epch_ms(),  # get now (epoch ms)
                        'reverse' : True
                    }
        
        metrics = aws_get_nosql_items(get_tbl_nm(app_tbl), params)
        
        metrics_proc[id] = metrics
        
    log.debug("Returning following metrics: %s" % str(metrics_proc))

    return metrics_proc

def get_metrics_web(max_num=1):
    # get metrics for web interface
    
    log = logging.getLogger(module_nm + '.get_metrics_web')
    
    cfg = get_cfg_app()
    client_id = cfg['client_id']

    try:
        max_num = int(max_num)
    except:
        log.warning("max_num of %s specified for %s ID %i, which is invalid...must be an integer. Defaulting to 1." % (str(max_num), proc, id))
        max_num = 1
        
    log.info("Querying %s for last %i entries for client ID %i." % ('metrics_web', max_num, client_id))

    params =    {   'limit': max_num,  # just want most recent (i.e., active)
                    'client_id__eq' : client_id,
                    'proc_end_epch_ms__lte' : get_epch_ms(),  # get now (epoch ms)
                    'reverse' : True
                }
    
    metrics_web = aws_get_nosql_items(get_tbl_nm('metrics_web'), params)
        
    log.debug("Returning following metrics: %s" % str(metrics_web))

    return { 'web': metrics_web }
    
def get_cfg():
    
    log = logging.getLogger(module_nm + '.get_cfg')

    return cfg_app  # right now, converting away from decimals (for one, can't cast them to JSON)
    
def get_cfg_main():
    
    log = logging.getLogger(module_nm + '.get_cfg_main')

    cfg = get_cfg_app()

    cfg_main = {}
    # tap our global var
    cfg_main['client_id'] = int(cfg['client_id'])
    cfg_main['e_eff_epch_ms'] = int(cfg['e_eff_epch_ms'])
    cfg_main['main'] = cfg['main']

    log.debug("Main configuration returned for client ID %i: %s" % (cfg_main['client_id'], str(pprint(cfg_main))))
    
    return cfg_main

def get_cfg_auth():
    
    log = logging.getLogger(module_nm + '.get_cfg_auth')

    cfg = get_cfg_app()

    cfg_auth = {}
    # tap our global var
    cfg_auth['client_id'] = int(cfg['client_id'])
    cfg_auth['e_eff_epch_ms'] = int(cfg['e_eff_epch_ms'])
    cfg_auth['auth'] = cfg['auth']

    log.debug("Auth configuration returned for client ID %i: %s" % (cfg_auth['client_id'], str(cfg_auth)))
    
    return cfg_auth
  
def get_cfg_ext(ids=[]):
    
    log = logging.getLogger(module_nm + '.get_cfg_ext')
    
    return get_cfg_proc('ext', ids)

def get_cfg_agg(ids=[]):
    
    log = logging.getLogger(module_nm + '.get_cfg_agg')
    
    return get_cfg_proc('agg', ids)

def get_cfg_proc(proc, ids=[]):
    # supporting function to retrieve configuration for requested process
    
    log = logging.getLogger(module_nm + '.get_cfg_proc')
    
    if proc not in vld_procs:  # referencing a global
        log.warning("%s is not a valid process type right now, only %s are. Skipping request." % (str(proc), str(vld_procs)))
        return "Invalid process type " + proc + " specified, must be one of " + str(vld_procs)

    cfg = get_cfg_app()  # relies on right client config already being loaded (yikes)

    if len(ids) == 0:  # return all
        return { id: cfg[proc][id] for id in cfg[proc].keys() }
    
    cfg_proc = {}
    for id in ids:
        if id not in cfg[proc].keys():
            log.warning("Requesting configuration for %s id %s, which does not exist. Skipping this ID." % (proc, id))
            continue
        cfg_proc.update( { id: cfg[proc][id] } )  # if we made it here, append to result
        
    log.debug("Returning following config info: %s" % str(cfg_proc))
    
    return cfg_proc

def push_cfg_to_ds():
    # Writes current (updated?) configuration to data store
    
    log = logging.getLogger(module_nm + '.push_cfg_to_ds')
    
    # may want/need some (integrity) checks in here
    cfg = get_cfg_app()

    ds_cfg = {}
    for aspect in vld_cfg_aspects:
        ds_cfg[aspect] = cfg[aspect]

    ds_cfg['client_id'] = cfg['client_id']
    ds_cfg['e_eff_epch_ms'] = get_epch_ms()  # right now
    aws_put_nosql_items('cfg', [ds_cfg])
    
    log.info("Current configuration successfully pushed to data store for client ID %i." % int(cfg['client_id']))
    
    return    

def updt_cfg_aspect(cfg_aspect):
    # Expects cfg_aspect as format, e.g.:
    # { 'main': { attrs } }
    # where dict key will be used to determine what aspect of
    # configuration to update.
    
    log = logging.getLogger(module_nm + '.updt_cfg_aspect')

    # may want to adjust how this is handled
    # these attributes get added back in later
    cfg_aspect.pop('e_eff_epch_ms', None)
    cfg_aspect.pop('client_id', None)

    if len(cfg_aspect.keys()) != 1:
        log.warning("Currently supports the update of exactly one aspect at a time, you tried to submit %i of them." % int(len(cfg_aspect.keys())))
        log.debug("Invalid dictionary submitted to this function was: %s." % str(cfg_aspect))
        return
    
    aspect_nm = cfg_aspect.keys()[0]
    if aspect_nm not in vld_cfg_aspects:
        log.warning("%s is not currently a valid configuration aspect." % str(aspect_nm))
        return

    cfg_app[aspect_nm] = cfg_aspect[aspect_nm]  # update in-memory config
    log.info("Configuration aspect %s for client ID %i successfully updated to: %s." % (aspect_nm, int(cfg_app['client_id']), str(cfg_app[aspect_nm])))
    
    push_cfg_to_ds()  # update stored config

    return
    
def update_logger_config(self, params, log_key=''):
    
    # Concept of 'log_key' is to accommodate multiple log configurations. If no log_key is provided to this function,
    # the log configuration is assumed to apply to main application logging.
    
    log = logging.getLogger(module_nm + '.update_logger_config')
    
    self.setLevel(params[log_key + '_lvl'])
    full_path = os.path.join(params[log_key + '_path'], params[log_key + '_fn'])

    log.info("%s logging changed by parameters, see %s for subsequent log messages." % (log_key, full_path))
    
    # Might want to do this on a selective basis where some of the default handlers remain.
    # I'm probably overthinking this, but this is pursuant to my goal to have all config in main config (instead of default Python logging.config).
    # An option to use separate logging conf for more advanced/flexible logging might be something to think about.

    handlers = list(self.handlers)
    for h in handlers:  # blow away any current handlers...brute force, may want to add to this in the future...
        self.removeHandler(h)
        h.flush()
        h.close()
            
    num_backup_files = params[log_key + '_max_files'] - 1

    handler = logging.handlers.RotatingFileHandler(full_path, 'a', maxBytes=params[log_key + '_file_size'], backupCount=num_backup_files)

    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    
    self.addHandler(handler)
    
    self.disabled = False  # while jacking around with handlers, this attribute would get set to True...not sure why...
    log.debug("Log handlers are now %s" % str(self.handlers))
    log.debug("Attributes for this logger are %s" % str(pprint(vars(handler))))
    
    return self

def get_pretty_ts(oTime=time.time()):  # not sure if this one is/should be used...

    return time.ctime(int(oTime))
    
def get_ext_spec(ext_name):
    # retrieve and load commong/global spec for an external adapter 
    
    filename = '../../../config/externals/' + ext_name + '.json'
    with open(filename, 'r') as file:
        file_data = file.read()

    return json.loads(file_data)
    
def get_fs_safe_ts(oTime=time.time()):

    return time.strftime("%Y%m%d-%H%M%S", time.localtime(oTime))
    
def get_rand_int(min=-2147483648, max=2147483648):

    return random.randint(min, max)
    
def get_rand_long(min=-9223372036854775808, max=9223372036854775808):

    return random.randint(min, max)
    
def get_rand_float(min=1, max=10):

    return random.uniform(min, max)

def get_rand_string(min=0, max=25, chars=string.ascii_letters + string.digits, strRange=[]):

    if not strRange:
        strLen = random.randint(min, max)
        return ''.join(random.choice(chars) for _ in range(strLen))
    else:
        return strRange[random.randint(0, len(strRange)-1)]  # randomly return entry in range list

def get_now_dttm():
    # Returns aware (timezone info) Python object
    return datetime.datetime.now(pytz.timezone(this_tz))

def get_sql_ts(dttm=datetime.datetime.now(pytz.timezone(this_tz))):  # hard-coded to Eastern
    # Returns sql-compatible timestamp string
    # If no arguments, uses *now* as time basis.
    
#    timezone = pytz.timezone(this_tz)
#    return timezone.localize(dttm).strftime(dttm_fmt)[:-3]  # removing last three digits in sub-second
    return dttm.strftime(dttm_fmt)[:-3]
    
def get_epch_ms(dttm=None):
    # Returns milliseconds since epoch for datetime object passed.
    # If no argument is passed, uses *now* as time basis.
    
    log = logging.getLogger(module_nm + '.get_epch_ms')
    
    if dttm is None:
        dttm = get_now_dttm()

    try:
        # return int(calendar.timegm(dttm.astimezone(pytz.timezone('US/Eastern')).utctimetuple()) * 1000.0 + round(dttm.microsecond / 1000.0))
        return int((dttm - datetime.datetime(1970, 1, 1, tzinfo=pytz.utc)).total_seconds() * 1000.0)
    except TypeError:  # dttm must be naive, assume UTC...
        return int(((dttm.replace(tzinfo=pytz.utc)) - datetime.datetime(1970, 1, 1, tzinfo=pytz.utc)).total_seconds() * 1000.0)
    
def ts_from_epch_ms(epch_ms):
    # Returns pretty timestamp when passed epoch milliseconds
    # in default/standard timezone.
    
    timezone = pytz.timezone(this_tz)

    return timezone.localize(datetime.datetime.fromtimestamp(epch_ms/1000)).strftime(dttm_fmt)
    
def dttm_from_epch(epch):
    # Returns datetime object based on epoch seconds passed
    # Right now, assumes everything to be UTC
    
    return datetime.datetime.fromtimestamp(epch)
    
def dttm_from_str(dttm_str):
    # Returns datetime object based on (compatible) string passed
    
    return dateutil.parser.parse(dttm_str)

def get_full_filename(self):
    return self.path + self.sub + self.filename

def confirm_dir_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise
        
def db_ins_rplc(updt_str):  # allows overwriting of entries, could be db-specific
    
    conn = sqlite3.connect('../cache/db')  # hard-coded db
    c = conn.cursor()
    c.execute(updt_str)
    new_row_id = c.lastrowid
    conn.commit()
    conn.close()
    
    return new_row_id

def cast_type(obj, type_id):
    
    # Takes object and casts it to appropriate data type based on type_id.
    # Hard-coded data types versus IDs for now...could be db-driven, perhaps including
    # type relationships within main configuration object that would be used by this function.

    if type_id == 0:  # int
        return int(obj)
    elif type_id == 1:  # long int
        return long(obj)
    elif type_id == 2:  # string; not supporting unicode right now...should we move to?
        try:
            return str(obj)
        except UnicodeEncodeError:  # not supporting unicode, at the moment...should we move to?
            return str(obj.encode('ascii', 'ignore'))
    elif type_id == 3:  # float
        return float(obj)
    elif type_id == 4:  # bool
        return bool(obj)
    else:  # default to string
        try:
            return str(obj)
        except UnicodeEncodeError:  # not supporting unicode, at the moment...should we move to?
            return str(obj.encode('ascii', 'ignore'))
        
def get_batch_id(ext_id):
    # Returns batch ID. Right now, simple logic, subject to enhancement.
    # This function used to accept Ext object, but now ext_id...

    cache_key = str(ext_id) + '_ext_batch_id_int'
    try:
        batch_id_int = read_cache([cache_key])[cache_key]
        if batch_id_int == None:  # no value found
            batch_id_int = 1
    except Exception as e:
        print "exception, setting batch_id_int to 1" + str(e)
        batch_id_int = 1
    
    write_cache({cache_key : batch_id_int + 1})  # increment and store
    
    return str(batch_id_int)

def get_rcd_ids(num, ext={}):
    # Returns record IDs. Right now, simple logic, subject to enhancement.
    # May want to increment record ID by one for each record (global?), in
    # which case will have to hit data store for "most recent" batch ID, etc.
    # This function is setup to accommodate the passing of Ext() object,
    # which may also be used in building the record ID. In other words: "multi-part" key.
    
    # Right now, just (possibly) returning external ID + batch ID + random int as record ID.
    
    try:
        if ext.meta['batch_id'] == -1:
            batch_id = 'N'  # no batch_id provided/available
        else:
            batch_id = str(ext.meta['batch_id'])
    except:
        batch_id = 'N'
    try:
        ext_id = str(ext.cfg['id'])
    except:
        ext_id = 'E'  # no external ID provided/available
    
    rcd_ids = []  # stores list of all record IDs for this batch
    t_append = rcd_ids.append  # make local
    for i in xrange(num):
        t_append(ext_id + batch_id + str(i))
        
    return rcd_ids

def enc_url_str(str):
    # URL-encode string
    return urllib.quote(str)
    
def enc_url_dict(dict):
    # URL-encode dict to string
    # {'one':'abc', 'two':'def'} becomes 'one=abc%two=def'
    return urllib.urlencode(dict)

def get_datatypes_app(attr_nms=[]):  # hard-coded data types here
    
    # Takes list of strings, each of which represents field/attribute name.
    # Returns list of integers representing corresponding Python data type for each.
    
    log = logging.getLogger(module_nm + '.get_datatypes_app')
    
    attr_types = []
    types = {   'e_eff_epch_ms' : 0,
                'e_proc_epch_ms' : 0,
                'e_sntmnt': 0,
                'e_rcd_id': 0,
                'e_pos_sntmnt': 0,
                'e_neg_sntmnt': 0,
                'e_neu_sntmnt': 0,
                'e_hashtag': 0,
                'e_mention': 0,
                'e_text': 0,
                'e_other': 0,
                'e_updtd_epch_ms': 0,
                'v_hashtag': 0,
                'v_mention': 0,
                'v_text': 0,
                'v_other': 0,
                'v_floor_epch_ms': 0,
                'v_pos_sntmnt_cum': 0,
                'v_neg_sntmnt_cum': 0,
                'v_neu_sntmnt_cum': 0,
                'e_ext_id' : 0,
                'e_amp' : 3,
                'e_retweet' : 0,  # boolean 0 or 1
                'v_retweet_cum' : 0,
                'agg_id' : 0
                }
    
    for i in range(len(attr_nms)):
        app_type = attr_types.append
        try:
            app_type(types[attr_nms[i]])
        except:
#            log.info("No application data type defined for attribute %s, defaulting to string." % attr_nms[i])  # extremely verbose
            app_type(2)  # default to string
            
    return attr_types
    
def get_datatypes_np(attr_nms=[]):  # hard-coded data types here
    
    # Takes list of strings, each of which represents field/attribute name.
    # Returns list of strings representing corresponding numpy array data type for each.
    
    attr_types = []
    types = { 'e_eff_epch_ms' : 'u8',
                'e_proc_epch_ms' : 'u8',
                'e_sntmnt' : 'i1',
                'e_rcd_id' : 'u4',
                'e_pos_sntmnt' : 'u2',
                'e_neg_sntmnt' : 'u2',
                'e_neu_sntmnt' : 'u2',
                'v_floor_epch_ms' : 'u8',
                'v_pos_sntmnt_cum' : 'u2',
                'v_neg_sntmnt_cum' : 'u2',
                'v_neu_sntmnt_cum' : 'u2',
                'e_ext_id' : 'u2',
#                u'v_floor_epch_ms' : 'u8',
#                u'v_pos_sntmnt_cum' : 'u2',
#                u'v_neg_sntmnt_cum' : 'u2',
#                u'v_neu_sntmnt_cum' : 'u2',
#                u'e_ext_id' : 'u2',
                'v_5min_epch_ms' : 'u8',
                'e_amp' : 'f16',
                'e_retweet' : 'u1',  # boolean 0 or 1
                'v_retweet_cum' : 'u2'
                 }
    
    return [types[x] for x in attr_nms]

# Below are utility functions facilitating interaction with AWS services.

def aws_get_nosql_conn():

    log = logging.getLogger(module_nm + '.aws_get_nosql_conn')

    cfg = get_cfg_app()
    cfg_env = cfg['env']
    if cfg_env['id'] == 'local':  # hard-coded check for local env (dynamoDBLocal)
        conn = NoSqlDB(
                    host = 'localhost',  # hard-coded
                    port = cfg_env['dynamo_port'],
                    aws_access_key_id = cfg_env['aws_access_key_id'],
                    aws_secret_access_key = cfg_env['aws_secret_access_key'],
                    is_secure=False)
    else:  # assume we're in the cloud...
        conn = boto.dynamodb2.connect_to_region(
                    cfg_env['region'],
                    aws_access_key_id = cfg_env['aws_access_key_id'],
                    aws_secret_access_key = cfg_env['aws_secret_access_key'])

    return conn

def aws_del_nosql_tbl(app_tbl):
    # where 'tbl' is table name (string)
    try:
        tbl = Table(get_tbl_nm(app_tbl))
        tbl.delete()
        log.info("Table %s has been deleted from the NoSql data store." % app_tbl)
        return True
    except:
        print "Problem deleting table %s from NoSql data store. Does it not exist?" % app_tbl
        log.warning("Problem deleting table %s from NoSql data store. Does it not exist?" % app_tbl)
        return False

def aws_desc_nosql_tbl(app_tbl):
    # where 'tbl' is table name (string)
    try:
        conn = aws_get_nosql_conn()
        return conn.describe_table(get_tbl_nm(app_tbl))
    except:
        log.warning("Problem when trying to describe NoSql table %s. Does it exist?" % app_tbl)
        return False

def aws_get_nosql_items(tbl_nm, params, conn=None):
    # Expects   tbl_nm as string
    #           params as compliant NoSql parameter dict
    #           [conn] as valid nosql connection
    
    log = logging.getLogger(module_nm + '.aws_get_nosql_items')

    if conn is None:
        conn = aws_get_nosql_conn()

    log.debug("Performing get to NoSql table %s with parameters: %s." % (tbl_nm, str(params)))

    tbl = Table(tbl_nm, connection = conn)
    rslt_set = tbl.query_2(**params)
    
    items = []
    log.debug("Process results.")
    t_append_item = items.append  # make local
    items = [{key: rslt[key] for key in rslt.keys()} for rslt in rslt_set]

    log.debug("NoSql get returned %i items." % len(items))
    
    return items

def aws_put_nosql_items(app_tbl, entries, conn=None):
    # Expects   app_tbl as string
    #           entries/items as list of dictionaries
    #           [conn] as valid nosql connection
                        
    log.debug("Performing put to NoSql data store %s." % app_tbl)

    if conn is None:
        conn = aws_get_nosql_conn()
        
    tbl = Table(get_tbl_nm(app_tbl), connection = conn)
    
    for entry in entries:
        item = Item(tbl, data=entry)
        try:
            item.save(overwrite=True)  # replace any overlapping entries entirely
        except:
            log.error("Error writing item to NoSqL store, raising error. Offending item: %s" % str(entry))
            print str(entry)
            raise
        
def build_ext_id(client_id, app, sub=''):
    # right now, just a concatenation
    
    if sub:
        return client_id + '-' + 'app' + '-' + sub
    else:
        return client_id + '-' + app

def get_total_nosql_thrput():
    # checks app configuration to determine total (provisioned) throughput configured for app

    total_thrput = 0
    cfg = get_cfg_app()
    cfg_env = cfg['env']
    for key in cfg_env.keys():
        if 'throughput' in key:
            total_thrput += int(cfg_env[key])

    return total_thrput

def repl_deci(obj):
    # replace decimals in object with integer or float

    if isinstance(obj, list):
        for i in xrange(len(obj)):
            obj[i] = repl_deci(obj[i])
        return obj
    elif isinstance(obj, dict):
        for k in obj.iterkeys():
            obj[k] = repl_deci(obj[k])
        return obj
    elif isinstance(obj, decimal.Decimal):
        if obj % 1 == 0:
            return int(obj)
        else:
            return float(obj)
    else:
        return obj
    
def split_url(url):
    # split URL into domain and path

    url = urlparse(url)
    
    return url.hostname, url.path

def insert_values(my_str, vars={}):

    # function looks for "double handlebars" and performs variable substitution based
    # on names and values submitted as vars argument

    for var_name in vars.keys():
        my_str.replace('{{' + var_name + '}}', vars[var_name])
    
    return my_str    
    
    
def read_cache(keys=[]):
    
    log = logging.getLogger(module_nm + '.read_cache')

    cache_pairs = {}  # will hold returned keys/values
    params = {}  # for query
    params['eff_epch_ms__eq'] = 0  # assumes no versioning maintained; this could easily be changed
    for key in keys:  # can pass multiple keys at once (can we deprecate this, pass just one at a time?)
        params['key__eq'] = key
        res = aws_get_nosql_items(get_tbl_nm('ops'), params)  # 'ops' table hard-coded, need to move get_tbl_nm to aws_get_nosql_items
        try:
            cache_pairs[key] = res[0]['value']  # if it exists
            log.debug("Read from cache attribute %s with value of %s." % (key, str(cache_pairs[key])))
        except:
            log.warning("Cached value not found for %s, populating response with default (None) and continuing." % key)
            cache_pairs[key] = None
    
    return cache_pairs
        
def write_cache(cache_pairs={}):
    
    log = logging.getLogger(module_nm + '.write_cache')
    
    entry = {}  # our dict with key/value
    entry['eff_epch_ms'] = 0  # same for each entry (non-versioned, Type 1 data)    
    for key in cache_pairs.keys():  # can pass multiple key/value pairs (can we deprecate this, pass just one at a time?)
        entry['key'] = key
        entry['value'] = cache_pairs[key]
        aws_put_nosql_items('ops', [entry])  # hard-coded 'ops' table

##### Set globals and situate configuration #####

if 'amzn' in platform.platform():  # hard-coded (and primitive) check to see if we're in AWS cloud
    is_aws = True
else:  # assume loco
    is_aws = False

if _platform == "linux" or _platform == "linux2":
    this_os = 'linux'
elif _platform == "darwin":
    this_os = 'osx'
elif _platform == "win32":
    this_os = 'win32'
else:
    this_os = 'other'

# pull app config in, global
# not sure how PC the below is or if it even does what i thought it might do....
# ... but i only want to run this once

log = logging.getLogger(module_nm)
#client_id = 0  # hard-coded, will be pulled from somewhere...

def get_cfg_app(client_id=''):

# for now, let's just always load it
#    try:
#        print("Checking whether cfg_app is already loaded into memory.")
#        cfg_app
#        print("Configuration already in memory for client ID %i." % cfg_app['client_id'])

#    except:
        # Configuration has multiple components to it, each
        # stored in a child dictionary. Environment config comes from file. Once we have that,
        # we can retrieve the rest of the configuration from our data store.
#    print("Configuration not already loaded into memory, so loading it BASED ON CLIENT ID %s." % client_id)
    print("Loading configuration.")
    load_cfg_app(client_id, from_file=True)  # establish/complete our cfg_app object
        
    return cfg_app

