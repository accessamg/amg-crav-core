import time
import os
import socket
from pprint import pprint
import logging
import sys

from boto.dynamodb2.layer1 import DynamoDBConnection as NoSqlDB  # support DynamoDBLocal
from boto.dynamodb2.fields import HashKey, RangeKey, KeysOnlyIndex, AllIndex, GlobalKeysOnlyIndex
from boto.dynamodb2.table import Table
from boto.dynamodb2.types import NUMBER
from boto.dynamodb2.types import STRING
from boto.dynamodb2.items import Item
import json

import util
import internal


module_nm = 'init_nosql'

# The base folder this whole business is installed in:
basedir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

# The init folder everything is installed in
buildinitdir = os.path.join(os.path.join(basedir,'tools'),'dbinit')

# The data dir for our folders.
datadir = os.path.join(os.path.join(basedir,'data'))
nosqldir = os.path.join(datadir,'dynamodb-store')


#Access Key ID:
#AKIAJ4DB6W447UOLVGVA
#Secret Access Key:
#siot+KOGYyYKJ0kf38VOhW2oMFaKvvaRLIShG5Wh

def _initApp():

    no_sql = internal.NoSql()
    chk_srvcs()
    chk_ddl(no_sql)
#    chk_data(no_sql)  # moved to file-based config

def chk_srvcs():

    log = logging.getLogger(module_nm + '.chk_srvcs')

    # for local, make sure dynamo service is up
    cfg_env = util.load_cfg_env()
    print "cfg_env:"
    print cfg_env
    if cfg_env['id'] == 'local':
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex(('127.0.0.1', int(cfg_env['dynamo_port'])))
        if result != 0:
            raise EnvironmentError("NoSQL data store is not detected, stopping application. Is NoSql data store running?")
        else:
            print "Data store detected on appropriate port."

def chk_ddl(no_sql):

    log = logging.getLogger(module_nm + '.chk_ddl')

    # let's track/check total provisioned throughput...
    # right now, read and write throughput are the same...
    total_thrput = util.get_total_nosql_thrput()
    cfg_env = util.load_cfg_env()
    if cfg_env['id'] == 'sb':  # sandbox within the cloud...
        if total_thrput > 25:  # stay in our free tier...dedicated to the sacrifice made by baal...
            print "Total provsioned throughput greater than 25, aborting."
            raise
    print "Total configured throughput: %i." % total_thrput

    for app_tbl in util.nosql_tbls:
        tbl_nm = util.get_tbl_nm(app_tbl)
        if tbl_nm not in no_sql.tables['TableNames']:
            print "Table %s not found in data store, creating it now." % tbl_nm
            log.warning("Table %s not found in data store, creating it now." % tbl_nm)

            if app_tbl == 'ext':  # external adapters
                ext = Table.create(util.get_tbl_nm('ext'), schema = [
                    HashKey('ext_id', data_type=STRING),
                    RangeKey('e_rcd_id', data_type=STRING)
                    ], throughput = {
                        'read' : cfg_env['ext_throughput'],
                        'write' : cfg_env['ext_throughput']
                    } , # indexes = [  #, global_indexes = [  # projection choices of GlobalKeysOnlyIndex, GlobalAllIndex, or GlobalIncludeIndex
#                        AllIndex('ProcEpchMs', parts = [
#                            HashKey('ext_id', data_type=NUMBER),
#                            RangeKey('e_proc_epch_ms', data_type=NUMBER)  # processed time facilitates incremental retrieval
#                            # interesting to see if DynamoStreams may alleviate need for this index....
#                            ] #, throughput = {  # pretty sure i should be able to do this, but getting unexpected keyword error
#                            #'read' : 15,
#                            #'write' : 15
#                            #}
#                        )
#                    ],
                    connection = no_sql.conn
                    )

#            if app_tbl == 'agg':  # aggregations
#                agg = Table.create(util.get_tbl_nm('agg'), schema = [
#                    HashKey('agg_id', data_type=NUMBER),
#                    RangeKey('v_floor_epch_ms', data_type=NUMBER)
#                    ], throughput = {
#                        'read' : cfg_env['agg_throughput'],
#                        'write' : cfg_env['agg_throughput']
#                    },
#                    connection = no_sql.conn
#                    )
                
#            if app_tbl == 'cfg':  # configuration
#                cfg = Table.create(util.get_tbl_nm('cfg'), schema = [
#                    HashKey('client_id', data_type=NUMBER),
#                    RangeKey('e_eff_epch_ms', data_type=NUMBER)
#                    ], throughput = {
#                        'read' : cfg_env['cfg_throughput'],
#                        'write' : cfg_env['cfg_throughput']
#                    },
#                    connection = no_sql.conn
#                    )

            if app_tbl == 'metrics_ext':
                metrics_ext = Table.create(util.get_tbl_nm('metrics_ext'), schema = [
                    HashKey('ext_id', data_type=NUMBER),
                    RangeKey('proc_end_epch_ms', data_type=NUMBER)  # defaults to type STRING
                    ], throughput = {
                        'read' : cfg_env['metrics_throughput'],
                        'write' : cfg_env['metrics_throughput']
                    },
                    connection = no_sql.conn
                    )
        
            if app_tbl == 'metrics_agg':
                metrics_agg = Table.create(util.get_tbl_nm('metrics_agg'), schema = [
                    HashKey('agg_id', data_type=NUMBER),
                    RangeKey('proc_end_epch_ms', data_type=NUMBER)  # defaults to type STRING
                    ], throughput = {
                        'read' : cfg_env['metrics_throughput'],
                        'write' : cfg_env['metrics_throughput']
                    },
                    connection = no_sql.conn
                    )
        
            if app_tbl == 'metrics_main':
                metrics_main = Table.create(util.get_tbl_nm('metrics_main'), schema = [
                    HashKey('client_id', data_type=NUMBER),
                    RangeKey('proc_end_epch_ms', data_type=NUMBER)  # defaults to type STRING
                    ], throughput = {
                        'read' : cfg_env['metrics_throughput'],
                        'write' : cfg_env['metrics_throughput']
                    }, 
                    connection = no_sql.conn
                    )
                
            if app_tbl == 'metrics_web':
                metrics_web = Table.create(util.get_tbl_nm('metrics_web'), schema = [
                    HashKey('client_id', data_type=NUMBER),
                    RangeKey('proc_end_epch_ms', data_type=NUMBER)  # defaults to type STRING
                    ], throughput = {
                        'read' : cfg_env['metrics_throughput'],
                        'write' : cfg_env['metrics_throughput']
                    },
                    connection = no_sql.conn
                    )
        
            if app_tbl == 'ops':  # operational
                ops = Table.create(util.get_tbl_nm('ops'), schema = [
                    HashKey('key', data_type=STRING),
                    RangeKey('eff_epch_ms', data_type=NUMBER)
                    ], throughput = {
                        'read' : cfg_env['ops_throughput'],
                        'write' : cfg_env['ops_throughput']
                    },
                    connection = no_sql.conn
                    )

#            if app_tbl == 'ref':  # reference
#                ops = Table.create(util.get_tbl_nm('ref'), schema = [
#                    HashKey('key', data_type=STRING),
#                    RangeKey('eff_epch_ms', data_type=NUMBER)  # something else??
#                    ], throughput = {
#                        'read' : cfg_env['ref_throughput'],
#                        'write' : cfg_env['ref_throughput']
#                    },
#                    connection = no_sql.conn
#                    )

def chk_data(no_sql):

    log = logging.getLogger(module_nm + '.chk_data')

    # Add some (environment-specific?) checks for table sizes? Conditionally prune?

    # For now, just always overwrite meta/cfg (will pickup any changes to meta/cfg seed files)
    import meta_seed
    import cfg_seed

    try:
        internal.NoSql.put_rcds(no_sql, 'ops', [meta_seed.ext_meta, meta_seed.agg_meta, meta_seed.main_meta])
    except:
        print "Waiting 10 seconds, then going to try once more to load data...table may not be ready."
        log.warning("Waiting 15 seconds, then going to try once more to load data to 'ops'...table may not be ready.")
        time.sleep(15)
        internal.NoSql.put_rcds(no_sql, 'ops', [meta_seed.ext_meta, meta_seed.agg_meta, meta_seed.main_meta])

    now = util.get_epch_ms()
    print "Updating e_eff_epch_ms for each configuration to *now*: %i." % now
    for i in range(len(cfg_seed.cfg)):  # all config dicts in this list
        cfg_seed.cfg[i]['e_eff_epch_ms'] = now

    try:
        internal.NoSql.put_rcds(no_sql, 'cfg', cfg_seed.cfg)
        print "Configuration written to data store."
    except:
        print "Waiting 10 seconds, then going to try once more to load data...table may not be ready."
        log.warning("Waiting 15 seconds, then going to try once more to load data to 'ops'...table may not be ready.")
        time.sleep(15)
        internal.NoSql.put_rcds(no_sql, 'cfg', cfg_seed.cfg)
        print "Configuration written to data store."

_initApp()
